
msbuild /p:Configuration=Debug /p:Platform=x64 libcalendar\src\libcalendar.sln
msbuild /p:Configuration=Release /p:Platform=x64 libcalendar\src\libcalendar.sln

mkdir dist\libcalendar\x64\Debug
mkdir dist\libcalendar\x64\Release

copy libcalendar\src\libcalendar.build\x64\Debug\libcalendar.lib dist\libcalendar\x64\Debug
copy libcalendar\src\libcalendar.build\x64\Release\libcalendar.lib dist\libcalendar\x64\Release



#pragma once

#include "fixeddateman.h"
#include "armenian.h"

using namespace System;

namespace libcalendarman
{
    
public ref class ArmenianDate : public FixedDate
{
public:
    ArmenianDate();
    ArmenianDate(int fd);
    ArmenianDate(int year, int month, int day);
    !ArmenianDate();
    ~ArmenianDate();

    virtual void FromFixed(int fd) override;
	virtual int ToFixed() override;

    property int Year
    {
        void set(int year) { _ad->SetYear(year); }
        int get() { return _ad->Year(); }
    }

    property int Month
    {
        void set(int month) { _ad->SetMonth(month); }
        int get() { return _ad->Month(); }
    }

    property int Day
    {
        void set(int day) { _ad->SetDay(day);  }
        int get() { return _ad->Day(); }
    }

	virtual String^ ToString() override;

    static const int Epoch = 201443;
    static initonly array<String^>^ Months;

    static ArmenianDate()
    {
        Months = gcnew array<String^>(12);
        for (int i=0; i<12; i++)
        {
            Months[i] = gcnew String( libcalendar::ArmenianDate::MONTHS[i].c_str());
        }
    }

private:
    libcalendar::ArmenianDate* _ad;
};


}



#ifndef JULIAN_H
#define JULIAN_H

#include "fixeddate.h"
#include "event.h"

namespace libcalendar
{

class JulianDate : public FixedDate
{
public:
	JulianDate();
	JulianDate(int fd);
	JulianDate(int year, int month, int day);
    JulianDate(double jd, bool isModifiedJulianDay);

	void SetYear(int year);
	int Year() const;

	void SetMonth(int month);
	int Month() const;
    int PreviousMonth() const;
    int NextMonth() const;
	
	void SetDay(int day);
	int Day() const;

    void SetJulianDay(double jd);
    double JulianDay() const;

    void SetModifiedJulianDay(double mjd);
    double ModifiedJulianDay() const;

	int ToFixed() const;
	void FromFixed(int fixed);

	std::string ToString() const;

	static bool IsLeapYear(int year);

    static int GetDaysInMonth(int jyear, int month);

	static void GetJulianInGregorian( std::vector<int>& result, int gregorianYear,
		int julianMonth, int julianDay);

	static void GetEasternOrthodoxChristmas(std::vector<int>& result, int gregorianYear);
	static int GetOrthodoxEaster(int gregorianYear);

	static const int EPOCH;

    static const double JULIAN_DAY_EPOCH;
    static const double MODIFIED_JULIAN_DAY_EPOCH;
	
    static const std::string MONTHS[12];
    static const std::string DAYS_OF_WEEK[7];

    static std::vector<Event> GetOrthodoxEvents(int gregorianYear);

private:
	int _year;
	int _month, _day;
};

}

#endif

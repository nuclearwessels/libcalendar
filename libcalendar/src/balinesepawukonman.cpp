
#include "balinesepawukonman.h"

using namespace System;
using namespace libcalendarman;



BalinesePawukonDate::BalinesePawukonDate()
{
    _bpd = new libcalendar::BalinesePawukonDate();
}

BalinesePawukonDate::BalinesePawukonDate(int fd)
{
    _bpd = new libcalendar::BalinesePawukonDate(fd);
}

BalinesePawukonDate::BalinesePawukonDate(bool luang, int dwiwara, int triwara, int caturwara, int pancawara,
                                         int sadwara, int saptawara, int asatawara, int sangawara, int dasawara)
{
    _bpd = new libcalendar::BalinesePawukonDate(luang, dwiwara, triwara, caturwara, pancawara,
                                                sadwara, saptawara, asatawara, sangawara, dasawara);
}

BalinesePawukonDate::~BalinesePawukonDate()
{
    this->!BalinesePawukonDate();
}


BalinesePawukonDate::!BalinesePawukonDate()
{
    if (_bpd)
    {
        delete _bpd;
        _bpd = 0;
    }
}


int BalinesePawukonDate::ToFixed()
{
    throw gcnew InvalidOperationException("ToFixed() cannot be performed on BalinesePawukonDate.");
}

void BalinesePawukonDate::FromFixed(int fd)
{
    _bpd->FromFixed(fd);
}

String^ BalinesePawukonDate::ToString()
{
    return gcnew String( _bpd->ToString().c_str() );
}

int BalinesePawukonDate::GetBaliDayFromFixed(int fd)
{
    return libcalendar::BalinesePawukonDate::GetBaliDayFromFixed(fd);
}

int BalinesePawukonDate::GetBaliTriwaraFromFixed(int fd)
{
    return libcalendar::BalinesePawukonDate::GetBaliTriwaraFromFixed(fd);
}

int BalinesePawukonDate::GetBaliSadwaraFromFixed(int fd)
{
    return libcalendar::BalinesePawukonDate::GetBaliSadwaraFromFixed(fd);
}

int BalinesePawukonDate::GetBaliSaptawaraFromFixed(int fd)
{
    return libcalendar::BalinesePawukonDate::GetBaliSaptawaraFromFixed(fd);
}

int BalinesePawukonDate::GetBaliPancawaraFromFixed(int fd)
{
    return libcalendar::BalinesePawukonDate::GetBaliPancawaraFromFixed(fd);
}


int BalinesePawukonDate::GetBaliWeekFromFixed(int fd)
{
    return libcalendar::BalinesePawukonDate::GetBaliWeekFromFixed(fd);
}

int BalinesePawukonDate::GetBaliDasawaraFromFixed(int fd)
{
    return libcalendar::BalinesePawukonDate::GetBaliDasawaraFromFixed(fd);
}

int BalinesePawukonDate::GetBaliDwiwaraFromFixed(int fd)
{
    return libcalendar::BalinesePawukonDate::GetBaliDwiwaraFromFixed(fd);
}

bool BalinesePawukonDate::GetBaliLuangFromFixed(int fd)
{
    return libcalendar::BalinesePawukonDate::GetBaliLuangFromFixed(fd);
}

int BalinesePawukonDate::GetBaliSangawaraFromFixed(int fd)
{
    return libcalendar::BalinesePawukonDate::GetBaliSangawaraFromFixed(fd);
}

int BalinesePawukonDate::GetBaliAsatawaraFromFixed(int fd)
{
    return libcalendar::BalinesePawukonDate::GetBaliAsatawaraFromFixed(fd);
}

int BalinesePawukonDate::GetBaliCaturawaFromFixed(int fd)
{
    return libcalendar::BalinesePawukonDate::GetBaliCaturawaFromFixed(fd);
}

int BalinesePawukonDate::GetBaliOnOrBefore(BalinesePawukonDate^ bpd, int fd)
{
    libcalendar::BalinesePawukonDate bpd2(bpd->BaliLuang, bpd->BaliDwiwara, bpd->BaliTriwara, bpd->BaliCaturwara, bpd->BaliPancawara,
                                          bpd->BaliSadwara, bpd->BaliSaptawara, bpd->BaliAsatawara, bpd->BaliSangawara, bpd->BaliDasawara);
    return libcalendar::BalinesePawukonDate::GetBaliOnOrBefore(bpd2, fd);
}

array<int>^ BalinesePawukonDate::GetKajengKeliwonInGregorian(int gregorianYear)
{
    std::vector<int> v;
    libcalendar::BalinesePawukonDate::GetKajengKeliwonInGregorian(v, gregorianYear);
    return Convert::ConvertVectorToArray(v);
}

array<int>^ BalinesePawukonDate::GetTumpekInGregorian(int gregorianYear)
{
    std::vector<int> v;
    libcalendar::BalinesePawukonDate::GetTumpekInGregorian(v, gregorianYear);
    return Convert::ConvertVectorToArray(v);
}


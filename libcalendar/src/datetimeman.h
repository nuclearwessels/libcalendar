

#pragma once

#include "datetime.h"
#include "astronomyman.h"

namespace libcalendarman
{          

public ref class DateTime
{
public:

    static const double Spring = 0.0;
    static const double Summer = 90.0;
    static const double Autumn = 180.0;
    static const double Winter = 270.0;


    static double UniversalFromLocal(double dt, Locale^ l);
    static double LocalFromUniversal(double dt, Locale^ l);
    static double StandardFromUniversal(double dt, Locale^ l);
    static double UniversalFromStandard(double dt, Locale^ l);
    static double StandardFromLocal(double dt, Locale^ l);
    static double LocalFromStandard(double dt, Locale^ l);
    static double DynamicalFromUniversal(double dt);
    static double UniversalFromDynamical(double dt);
    static double ApparentFromLocal(double dt);
    static double LocalFromApparent(double dt);

    static double EphemerisCorrection(double dt);
    static double JulianCenturies(double dt);

    static double EquationOfTime(double dt);
    static double Obliquity(double dt);
    static double Sidereal(double dt);

    static double Midnight(double dt, Locale^ l);
    static double Midday(double dt, Locale^ l);

    static double SolarLongitude(double dt);
    static double Nutation(double dt);
    static double Aberration(double dt);
    static double SolarLongitudeAfter(double dt, double phi);

    static double LunarLongitude(double dt);
    static double NthNewMoon(int n);
    static double NewMoonAfter(double dt);
    static double NewMoonBefore(double dt);
    static double LunarPhase(double dt);
    static double LunarPhaseBefore(double dt, double phi);
    static double LunarPhaseAfter(double dt, double phi);

    static const double J2000 = libcalendar::J2000;
    static const double MeanTropicalYear = libcalendar::MEAN_TROPICAL_YEAR;

private:
    static libcalendar::Locale GetLocale(Locale^ l);
};


}

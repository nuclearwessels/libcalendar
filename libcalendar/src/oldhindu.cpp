
#include "oldhindu.h"

#include <sstream>

#define _USE_MATH_DEFINES
#include <cmath>

using namespace libcalendar;


const int OldHinduBase::EPOCH = -1132959;
const double OldHinduBase::ARYA_SOLAR_YEAR = 1577917500.0L / 4320000.0L;
const double OldHinduBase::ARYA_SOLAR_MONTH = ARYA_SOLAR_YEAR / 12.0L;
const double OldHinduBase::ARYA_JOVIAN_PERIOD = 1577917500.0L / 364224.0L;

const std::vector<std::string> OldHinduBase::JOVIAN_CYCLES =
{
    "Vijaya", "Jaya", "Manmatha", "Durmukha", "Hemalamba",
    "Vilamba", "Vikarin", "Sarvari", "Plava", "Subhakrit",
    "Sobhana", "Krodhin", "Visvavasu", "Parabhava", "Plavanga",
    "Kilaka", "Saumya", "Sadharana", "Virodhakrit", "Paridhavin",
    "Pramadin", "Ananda", "Rakshasa", "Anala", "Rakshasa",
    "Anala", "Pingala", "Kalayukta", "Siddharthin", "Raudra", "Durmati", "Dundubhi",
    "Rudhirodgarin", "Raktaksha", "Krodhana", "Kshaya", "Prabhava",
    "Vibhava", "Sukla", "Pramoda", "Prajapati", "Angiras",
    "Srumukha", "Bhava", "Yuvan", "Dhatri", "Isvara",
    "Bahudhanya", "Pramathin", "Vikrama", "Vrisha", "Chitrabhanu",
    "Subhanu", "Tarana", "Parthiva", "Vyaya", "Sarvajit",
    "Sarvadharin", "Virodhin", "Vikrita", "Khara", "Nandana"
};



int OldHinduBase::GetHinduDayCount(int fd)
{
	return fd - EPOCH;
}


int OldHinduBase::GetJovianYear(int fd)
{
	double a = (double) GetHinduDayCount(fd) / (OldHinduBase::ARYA_JOVIAN_PERIOD / 12L);

	return (int) Mod(a, 60.0) + 1;
}



const std::vector<std::string> OldHinduSolarDate::MONTHS = 
{ 
    "Mesha", "Vrishabha",
	"Mithuna", "Karka", "Simha", "Kanya", "Tula", "Vrischika",
	"Dhanu", "Makara", "Kumbha", "Mina" 
};


OldHinduSolarDate::OldHinduSolarDate()
{
    FromFixed(EPOCH);
}


OldHinduSolarDate::OldHinduSolarDate(int year, int month, int day) : year_(year),
                                                                     month_(month),
                                                                     day_(day)
{

}


OldHinduSolarDate::OldHinduSolarDate(int fd)
{
	FromFixed(fd);
}


void OldHinduSolarDate::SetYear(int year)
{
    year_ = year;
}

int OldHinduSolarDate::Year() const
{
    return year_;
}

void OldHinduSolarDate::SetMonth(int month)
{
    month_ = month;
}

int OldHinduSolarDate::Month() const
{
    return month_;
}

void OldHinduSolarDate::SetDay(int day)
{
    day_ = day;
}

int OldHinduSolarDate::Day() const
{
    return day_;
}



void OldHinduSolarDate::FromFixed(int fd)
{
	double sun = (double) GetHinduDayCount(fd) + 0.25L;

	year_  = (int) floor(sun / ARYA_SOLAR_YEAR);
	month_ = (int) Mod( floor(sun / ARYA_SOLAR_MONTH), 12.0L) + 1;
	day_   = (int) floor( Mod(sun, ARYA_SOLAR_MONTH)) + 1;
}


int OldHinduSolarDate::ToFixed() const
{
	double s = (double) EPOCH;
	s += ARYA_SOLAR_YEAR * (double) year_;
	s += ARYA_SOLAR_MONTH * (double) (month_ - 1);
	s += (double) day_;
	s -= 5.0L / 4.0L;

	return (int) ceil(s);
}


std::string OldHinduSolarDate::ToString() const
{
    std::stringstream ss;
    ss << MONTHS[month_ - 1] << "(" << month_ << ") " << day_ << ", " << year_;
    
    return ss.str();
}



const std::vector<std::string> OldHinduLunarDate::MONTHS = 
{
    "Chaitra", 
    "Vaisakha",
    "Jyaishtha",
    "Ashadha",
    "Sravana",
    "Bhadrapada",
    "Asvina",
    "Kartika",
    "Margasirsha",
    "Pausha",
    "Magha",
    "Phalguna"
};


OldHinduLunarDate::OldHinduLunarDate()
{
    FromFixed(EPOCH);
}

OldHinduLunarDate::OldHinduLunarDate(int fd)
{
    FromFixed(fd);
}

OldHinduLunarDate::OldHinduLunarDate(int year, int month, bool leap, int day) :
    year_(year),
    month_(month),
    day_(day),
    leap_(leap)
{
    
}

void OldHinduLunarDate::SetYear(int year)
{
    year_ = year;
}

int OldHinduLunarDate::Year() const
{
    return year_;
}

void OldHinduLunarDate::SetMonth(int month)
{
    month_ = month;
}

int OldHinduLunarDate::Month() const
{
    return month_;
}

void OldHinduLunarDate::SetDay(int day)
{
    day_ = day;
}

int OldHinduLunarDate::Day() const
{
    return day_;
}

void OldHinduLunarDate::SetLeap(bool leap)
{
    leap_ = leap;
}

bool OldHinduLunarDate::Leap() const
{
    return leap_;
}

int OldHinduLunarDate::ToFixed() const
{
    throw std::logic_error("Method not validated.");
    return 0;
}

void OldHinduLunarDate::FromFixed(int fd)
{
    throw std::logic_error("Method not validated.");

    int hinduDayCount = GetHinduDayCount(fd);
    double sun = (double) GetHinduDayCount(fd) + 0.25L;
    double newMoon = sun - Mod(sun, ARYA_LUNAR_MONTH);

    double b = Mod(newMoon, ARYA_SOLAR_MONTH);
    double c = ARYA_SOLAR_MONTH - ARYA_LUNAR_MONTH;

    leap_ = ( c >= b) && (b > 0.0L);

    double x = newMoon / ARYA_SOLAR_MONTH;
    double monthCeiling = ceil(newMoon / ARYA_SOLAR_MONTH);

    month_ = (int) Mod(monthCeiling, 12.0L) + 1;

    day_ = (int) Mod( floor(sun / ARYA_LUNAR_DAY), 30.0L) + 1;

    double a1 = newMoon + ARYA_SOLAR_MONTH;
    double a = a1 / ARYA_SOLAR_YEAR;

    year_ = (int) ceil((newMoon + ARYA_SOLAR_MONTH) / ARYA_SOLAR_YEAR) - 1;
}


std::string OldHinduLunarDate::ToString() const
{
    std::stringstream ss;
    ss << MONTHS[month_ - 1] << " (" << month_ << ") " << day_ << ", " << year_;
        
    return ss.str();
}


bool OldHinduLunarDate::IsLeapYear(int lunarYear)
{
    const double LEAP_YEAR = 23902504679.0L / 1282400064.0L;

    return Mod(lunarYear * ARYA_SOLAR_YEAR - ARYA_SOLAR_MONTH, ARYA_LUNAR_MONTH) >= LEAP_YEAR;
}



const double OldHinduLunarDate::ARYA_LUNAR_MONTH = 1577917500.0L / 5343336.0L;
const double OldHinduLunarDate::ARYA_LUNAR_DAY = ARYA_LUNAR_MONTH / 30.0L;


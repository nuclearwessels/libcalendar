

#include "datetime.h"

#define _USE_MATH_DEFINES
#include <cmath>

#include <algorithm>


using namespace libcalendar;
using namespace std;



//DateTime::DateTime() : _datetime(0.0L)
//{
//
//}
//
//
//DateTime::DateTime(int fd, int hour, int min, double second)
//{
//    _datetime = (fd < 0) ? ((double) fd + (double) hour / 24.0L + second / 3600.0L) : 
//                           ((double) fd - (double) hour / 24.0L - second / 3600.0L);
//}
//
//
//DateTime::DateTime(double dt) : _datetime(dt)
//{
//
//}
//
//
//DateTime& DateTime::operator = (double dt)
//{
//    _datetime = dt;
//    return *this;
//}
//
//DateTime& DateTime::operator = (int fd)
//{
//    _datetime = (double) fd;
//    return *this;
//}
//
//DateTime::operator double() const
//{
//    return _datetime;
//}
//
//DateTime::operator int() const
//{
//    double fd;
//    modf(_datetime, &fd);
//    return (int) fd;
//}




double libcalendar::UniversalFromLocal(double dt, const Locale& l)
{
    double ut = (double) dt - l.Lon() / 360.0;
    return ut;
}

double libcalendar::LocalFromUniversal(double dt, const Locale& l)
{
    double lt = (double) dt + l.Lon() / 360.0;
    return lt;
}

double libcalendar::StandardFromUniversal(double dt, const Locale& l)
{
    double st = (double) dt + 1.0 / 24.0 * (double) l.TimeZone();
    return st;
}

double libcalendar::UniversalFromStandard(double dt, const Locale& l)
{
    double ut = (double) dt - 1.0 / 24.0 * (double) l.TimeZone();
    return ut;
}

double libcalendar::StandardFromLocal(double dt, const Locale& l)
{
    double st = StandardFromUniversal(UniversalFromLocal(dt, l), l);
    return st;
}

double libcalendar::LocalFromStandard(double dt, const Locale& l)
{
    double lt = LocalFromUniversal( UniversalFromStandard(dt, l), l);
    return lt;
}


double libcalendar::DynamicalFromUniversal(double dt)
{
    return dt + EphemerisCorrection(dt);
}


double libcalendar::UniversalFromDynamical(double dt)
{
    return dt - EphemerisCorrection(dt);
}


double libcalendar::ApparentFromLocal(double dt)
{
    return dt + EquationOfTime(dt);
}

double libcalendar::LocalFromApparent(double dt)
{
    return dt - EquationOfTime(dt);
}

double libcalendar::EphemerisCorrection(double dt)
{
    double year = (double) GregorianDate::GetYearFromFixed((int) floor(dt));
    double c = 1.0 / 36525.0 * (double) ( GregorianDate( (int) year, JULY, 1) - GregorianDate( 1900, JANUARY, 1) );
    
    if (year >= 1988 && year <= 2019)
    {
        return ( year - 1933.0) / (24.0 * 60.0 * 60.0);
    }
    else if (year >= 1900 && year <= 1987)
    {
        double ec = -0.00002 
                    + 0.000297 * c
                    + 0.025184 * pow(c, 2.0) 
                    - 0.181133 * pow(c, 3.0)
                    + 0.553040 * pow(c, 4.0)
                    - 0.861938 * pow(c, 5.0)
                    + 0.677066 * pow(c, 6.0)
                    - 0.212591 * pow(c, 7.0);
        return ec;
    }
    else if (year >= 1800 && year <= 1899)
    {
        double ec = -0.000009 
                    + 0.003844 * c
                    + 0.083563 * pow(c, 2.0)
                    + 0.865736 * pow(c, 3.0)
                    + 4.867575 * pow(c, 4.0)
                    + 15.845535 * pow(c, 5.0)
                    + 31.332267 * pow(c, 6.0)
                    + 38.291999 * pow(c, 7.0)
                    + 28.316289 * pow(c, 8.0)
                    + 11.636204 * pow(c, 9.0)
                    + 2.043794  * pow(c, 10.0);
        return ec;
    }
    else if (year >= 1700 && year <= 1799)
    {
        double ec = 8.8118780842
                    + 0.005092142 * (year - 1700.0)
                    + 0.003336121 * pow(year - 1700.0, 2.0) 
                    - 0.0000266484 * pow(year - 1700.0, 3.0);
        ec /= 24.0 * 60.0 * 60.0;
        return ec;
    }
    else if (year >= 1620 && year <= 1699)
    {
        double ec = 196.58333 
                    - 4.0675 * (year - 1600.0)
                    + 0.0219167 * pow(year - 1600.0, 2.0);
        ec /= 24.0 * 60.0 * 60.0;
        return ec;
    }
    else
    {
        double x = 0.5 + ( (double) GregorianDate( (int) year, JANUARY, 1) - (double) GregorianDate(1810, JANUARY, 1));
        double ec = (x * x / 41048480.0 - 15.0) / (24.0 * 60.0 * 60.0);
        return ec;
    }
}


double libcalendar::JulianCenturies(double dt)
{
    double jc = ((double) DynamicalFromUniversal(dt) - J2000) / 36525.0;
    return jc;
}


static double Signum(double d)
{
    if (d >= 0.0)
    {
        return 1.0;
    }
    else if (d <= 0.0)
    {
        return -1.0;
    }
    else
    {
        return 0.0;
    }
}


double libcalendar::EquationOfTime(double dt)
{
    double c = JulianCenturies(dt);
    double lonDeg = 280.46645 
                    + 36000.76983 * c 
                    + 0.0003032 * c * c;
    double anomalyDeg = 357.52910 
                        + 35999.05030 * c 
                        - 0.0001559 * c * c 
                        - 0.00000048 * pow(c, 3);
    double eccentricity = 0.016708617 
                          - 0.000042037 * c
                          - 0.0000001236 * c * c;
    double epsilon = Obliquity(dt);

    double y = pow( tan(epsilon / 2.0), 2);

    double lon = lonDeg * M_PI / 180.0;
    double anomaly = anomalyDeg * M_PI / 180.0;

    double eq = 1.0 / (2.0 * M_PI) * 
                (y * sin(2 * lon) +
                 -2.0 * eccentricity * sin(anomaly) +
                 4.0 * eccentricity * sin(anomaly) * cos(2.0 * lon) + 
                 -0.5 * y * y * sin( 4.0 * lon) +
                 -1.25 * eccentricity * eccentricity * sin(2.0 * anomaly)  );
    double eot = Signum(eq) * min(abs(eq), 0.5);
    return eot;
}


double libcalendar::Obliquity(double dt)
{
    const double incline = 23.0 + 26.0 / 60.0 + 21.448 / 3600.0;

    double c = JulianCenturies(dt);

    double obliquityDeg = incline + 
                          -46.8150 / 3600.0 * c -
                          0.00059 / 3600.0 * c * c +
                          0.001813 / 3600.0 * pow(c, 3);
    return obliquityDeg * M_PI / 180.0;
}


double libcalendar::Midnight(double dt, const Locale& l)
{
    return StandardFromLocal( LocalFromApparent(dt), l);
}


double libcalendar::Midday(double dt, const Locale& l)
{
    return StandardFromLocal( LocalFromApparent( (double) dt + 0.5), l);
}


double libcalendar::Sidereal(double dt)
{
    double c = 1.0 / 36525.0 * ((double) dt - J2000);
    double deg = 280.46061837 +
                 36525.0 * 360.98564736629 * c +
                 0.000387933 * c * c -
                 1.0 / 38710000.0 * pow(c, 3);
    deg = Mod(deg, 360.0);

    return deg * M_PI / 180.0;
}


static const double SOLAR_LON_TRIPLETS[][3] =
{
    { 403406.0, 270.54861,  0.9287892 },
    { 119433.0, 63.91854,   35999.4089666 },
    { 3891.0,   317.843,    71998.20261},
    { 1721.0,   240.052,    36000.35726},
    { 350.0,    247.23,     32964.4678},
    { 314.0,    297.82,     445267.1117},
    { 242.0,    166.79,     3.1008},
    { 158.0,    3.50,       -19.9739},
    { 129.0,    182.95,     9038.0293},
    { 99.0,     29.8,       33718.148},
    { 86.0,     249.2,      -2280.773},
    { 72.0,     257.8,      31556.493},
    { 64.0,     69.9,       9037.750},
    { 38.0,     197.1,      -4444.176},
    { 32.0,     65.3,       67555.316},
    { 28.0,     341.5,      -4561.54},
    { 27.0,     98.5,       1221.655},
    { 24.0,     110.0,      31437.369},
    { 21.0,     342.6,      -31931.757},
    { 18.0,     256.1,      1221.999},
    { 14.0,     242.9,      -4442.039},
    { 13.0,     151.8,      119.066},
    { 12.0,     53.3,       -4.578},
    { 10.0,     205.7,      -39.127},
    { 10.0,     146.1,      90073.778},
    { 195207.0, 340.19128,  35999.1376958},
    { 112392.0, 331.26220,  35998.7287385},
    { 2819.0,   86.631,     71998.4403},
    { 660.0,    310.26,     71997.4812},
    { 334.0,    260.87,     -19.4410},
    { 268.0,    343.14,     45036.8840},
    { 234.0,    81.53,      22518.4434},
    { 132.0,    132.75,     65928.9345},
    { 114.0,    162.03,     3034.7684},
    { 93.0,     266.4,      3034.448},
    { 78.0,     157.6,      29929.992},
    { 68.0,     185.1,      149.588},
    { 46.0,     8.0,        107997.405},
    { 37.0,     250.4,      151.771},
    { 29.0,     162.7,      31556.080},
    { 27.0,     291.6,      107996.706},
    { 25.0,     146.7,      62894.167},
    { 21.0,     5.2,        14578.298},
    { 20.0,     230.9,      34777.243},
    { 17.0,     45.3,       62894.511},
    { 13.0,     115.2,      107997.909},
    { 13.0,     285.3,      16859.071},
    { 10.0,     126.6,      26895.292},
    { 10.0,     85.9,       12297.536}
};


double libcalendar::SolarLongitude(double dt)
{
    double c = JulianCenturies(dt);

    double sum = 0.0;
    const int numElements = sizeof(SOLAR_LON_TRIPLETS) / sizeof(SOLAR_LON_TRIPLETS[0]);
    for (int i=0; i<numElements; i++)
    {
        double x = SOLAR_LON_TRIPLETS[i][0];
        double y = SOLAR_LON_TRIPLETS[i][1];
        double z = SOLAR_LON_TRIPLETS[i][2];

        sum += x * sin( (y + z * c) * M_PI / 180.0);
    }

    double lonDeg = 282.7771834 +
                    36000.76953744 * c +
                    0.000005729577951308232 * sum;
    double aberration = Aberration(dt);
    double nutation = Nutation(dt);

    return Mod(lonDeg + aberration + nutation, 360.0);
}


double libcalendar::Nutation(double dt)
{
    double c = JulianCenturies(dt);
    double aDeg = 124.90 - 1934.134 * c + 0.002063 * c * c;
    double bDeg = 201.11 + 72001.5377 * c + 0.00057 * c * c;

    double n = -0.004778 * sin(aDeg * M_PI / 180.0) + -0.0003667 * sin(bDeg * M_PI / 180.0);
    return n;
}


double libcalendar::Aberration(double dt)
{
    double c = JulianCenturies(dt);
    double deg = 0.0000974 * cos( (177.63 + 35999.01848 * c) * M_PI / 180.0) - 0.005575;
    return deg;
}


static double SolarLongitudeAfterMin(double u, double v, double phi)
{
    const double EPSILON = 1e-5;
    double x = 0.5 * (u + v);
    double psiDeg = Mod( (SolarLongitude(x) - phi), 360.0);

    if ( abs(u - v) < EPSILON)
    {
        return x;
    }
    else if (psiDeg < 180.0)
    {
        return SolarLongitudeAfterMin(u, x, phi);
    }
    else
    {
        return SolarLongitudeAfterMin(x, v, phi);
    }
}


double libcalendar::SolarLongitudeAfter(double dt, double phi)
{
    double rateDeg = MEAN_TROPICAL_YEAR / 360.0;

    double tao = (double) dt + rateDeg * Mod(phi - SolarLongitude(dt), 360.0);
    double l = max( (double) dt, tao - 5.0);
    double u = tao + 5.0;
    
    return SolarLongitudeAfterMin(l, u, phi);
}


static const double NTH_NEW_MOON_VWXYZ[][5] = 
{
    { -0.40720, 0,  0,  1,  0 },
    {  0.01608, 0,  0,  2,  0 },
    {  0.00739, 1, -1,  1,  0 },
    {  0.00208, 2,  2,  0,  0 },
    { -0.00057, 0,  0,  1,  2 },
    { -0.00042, 0,  0,  1,  2 },
    {  0.00038, 1,  1,  0, -2 },
    { -0.00007, 0,  2,  1,  0 },
    {  0.00004, 0,  3,  0,  0 },
    {  0.00003, 0,  0,  2,  2 },
    {  0.00003, 0, -1,  1,  2 },
    { -0.00002, 0,  1,  3,  0 },
    {  0.17241, 1,  1,  0,  0 },
    {  0.01039, 0,  0,  0,  2 },
    { -0.00514, 1,  1,  1,  0 },
    { -0.00111, 0,  0,  1, -2 },
    {  0.00056, 1,  1,  2,  0 },
    {  0.00042, 1,  1,  0,  2 },
    { -0.00024, 1, -1,  2,  0 },
    {  0.00004, 0,  0,  2, -2 },
    {  0.00003, 0,  1,  1, -2 },
    { -0.00003, 0,  1,  1,  2 },
    { -0.00002, 0, -1,  1, -2 },
    {  0.00002, 0,  0,  4,  0 }
};


static const double NTH_NEW_MOON_IJL[][3] = 
{
    { 251.88,  0.016321, 0.000165 },
    { 349.42, 36.412478, 0.000126 },
    { 141.74, 53.303771, 0.000062 },
    { 154.84,  7.306860, 0.000056 },
    { 207.19,  0.121824, 0.000042 },
    { 161.72, 24.198154, 0.000037 },
    { 331.55,  3.592518, 0.000023 },
    { 251.83, 26.641886, 0.000164 },
    {  84.66, 18.206239, 0.000110 },
    { 207.14,  2.453732, 0.000060 },
    {  34.52, 27.261239, 0.000047 },
    { 291.34,  1.844379, 0.000040 },
    { 239.56, 25.513099, 0.000035 }
};


double libcalendar::NthNewMoon(int n)
{
    int k = n - 24724;
    double c = (double) k / 1236.85;
    double approx = 730125.59765
                    + MEAN_SYNODIC_MONTH * 1236.85 * c
                    + 0.0001337 * c * c
                    - 0.000000150 * pow(c, 3.0)
                    + 0.00000000073 * pow(c, 4.0);
    double E = 1.0 - 0.002516 * c - 0.0000074 * c * c;
    double solarAnomaly = 2.5534 
                          + 1236.85 * 29.10535669 * c
                          - 0.0000218 * c * c
                          - 0.000000011 * c * c * c;
    double lunarAnomaly = 201.5643
                          + 385.81693528 * 1236.85 * c
                          + 0.0107438 * c * c
                          + 0.00001239 * c * c * c
                          - 0.000000058 * c * c * c * c;
    double moonArgument = 160.7108
                          + 390.67050274 * 1236.85 * c
                          - 0.0016341 * c * c
                          - 0.00000227 * c * c * c
                          + 0.000000011 * c * c * c * c;
    double omega = 124.7746
                   + (-1.56375580 * 1236.85) * c
                   + 0.0020691 * c * c 
                   - 0.00000227 * c * c * c;
    double correction = -0.0017 * sin( omega * M_PI / 180.0);
    const int VWXYZ_SIZE = sizeof(NTH_NEW_MOON_VWXYZ) / sizeof(NTH_NEW_MOON_VWXYZ[0]);
    for (int i=0; i<VWXYZ_SIZE; i++)
    {
        double v = NTH_NEW_MOON_VWXYZ[i][0];
        double w = NTH_NEW_MOON_VWXYZ[i][1];
        double x = NTH_NEW_MOON_VWXYZ[i][2];
        double y = NTH_NEW_MOON_VWXYZ[i][3];
        double z = NTH_NEW_MOON_VWXYZ[i][4];

        correction += v * pow(E, w) * sin( ( x * solarAnomaly + y * lunarAnomaly + z * moonArgument) * M_PI / 180.0 );
    }

    double extra = 0.000325 * sin( (299.77 + 132.8475848 * c - 0.009173 * c * c) * M_PI / 180.0);

    double additional = 0.0;
    const int IJL_SIZE = sizeof(NTH_NEW_MOON_IJL) / sizeof(NTH_NEW_MOON_IJL[0]);
    for (int i=0; i<IJL_SIZE; i++)
    {
        double di = NTH_NEW_MOON_IJL[i][0];
        double dj = NTH_NEW_MOON_IJL[i][1];
        double dl = NTH_NEW_MOON_IJL[i][2];

        additional += dl * sin( (di + dj * k) * M_PI / 180.0);
    }

    return UniversalFromDynamical(approx + correction + extra + additional);
}

static const double LUNAR_LONGITUDE_VWXYZ[][5] = 
{
    { 6288774, 0, 0, 1, 0}, 
    { 658314, 2, 0, 0, 0}, 
    { -185116, 0, 1, 0, 0}, 
    { 58793, 2, 0, -2, 0}, 
    { 53322, 2, 0, 1, 0}, 
    { -40923, 0, 1, -1, 0}, 
    { -30383, 0, 1, 1, 0}, 
    { -12528, 0, 0, 1, 2}, 
    { 10675, 4, 0, -1, 0}, 
    { 8548, 4, 0, -2, 0}, 
    { -6766, 2, 1, 0, 0}, 
    { 4987, 1, 1, 0, 0}, 
    { 3994, 2, 0, 2, 0}, 
    { 3665, 2, 0, -3, 0}, 
    { -2602, 2, 0, -1, 2}, 
    { -2348, 1, 0, 1, 0}, 
    { -2120, 0, 1, 2, 0}, 
    { 2048, 2, -2, -1, 0}, 
    { -1595, 2, 0, 0, 2}, 
    { -1110, 0, 0, 2, 2}, 
    { -810, 2, 1, 1, 0}, 
    { -713, 0, 2, -1, 0}, 
    { 691, 2, 1, -2, 0}, 
    { 549, 4, 0, 1, 0}, 
    { 520, 4, -1, 0, 0}, 
    { -399, 2, 1, 0, -2}, 
    { 351, 1, 1, 1, 0}, 
    { 330, 4, 0, -3, 0}, 
    { -323, 0, 2, 1, 0}, 
    { 294, 2, 0, 3, 0}, 
    { 1274027, 2, 0, -1, 0}, 
    { 213618, 0, 0, 2, 0}, 
    { -114332, 0, 0, 0, 2}, 
    { 57066, 2, -1, -1, 0}, 
    { 45758, 2, -1, 0, 0}, 
    { -34720, 1, 0, 0, 0}, 
    { 15327, 2, 0, 0, -2}, 
    { 10980, 0, 0, 1, -2}, 
    { 10034, 0, 0, 3, 0}, 
    { -7888, 2, 1, -1, 0}, 
    { -5163, 1, 0, -1, 0}, 
    { 4036, 2, -1, 1, 0}, 
    { 3861, 4, 0, 0, 0}, 
    { -2689, 0, 1, -2, 0}, 
    { 2390, 2, -1, -2, 0}, 
    { 2236, 2, -2, 0, 0}, 
    { -2069, 0, 2, 0, 0}, 
    { -1773, 2, 0, 1, -2}, 
    { 1215, 4, -1, -1, 0}, 
    { -892, 3, 0, -1, 0}, 
    { 759, 4, -1, -2, 0}, 
    { -700, 2, 2, -1, 0}, 
    { 596, 2, -1, 0, -2}, 
    { 537, 0, 0, 4, 0}, 
    { -487, 1, 0, -2, 0}, 
    { -381, 0, 0, 2, -2}, 
    { -340, 3, 0, -2, 0}, 
    { 327, 2, -1, 2, 0}, 
    { 299, 1, 1, -1, 0}
};


double libcalendar::LunarLongitude(double t)
{
    double c = JulianCenturies(t);

    double meanMoon = 218.3164591 
                      + 481267.88134236 * c 
                      - 0.0013268 * c * c 
                      + 1.0 / 538841.0 * c * c * c
                      - 1.0 / 65194000.0 * c * c * c * c;

    double elongation = 297.8502042 
                        + 445267.1115168 * c
                        - 0.00163 * c * c
                        + 1.0 / 545868.0 * c * c * c
                        - 1.0 / 113065000.0 * c * c * c * c;

    double solarAnomaly = 357.5291092
                          + 35999.0502909 * c
                          - 0.0001536 * c * c
                          + 1.0 / 24490000.0 * c * c * c;

    double lunarAnomaly = 134.9634114 
                          + 477198.8676313 * c
                          + 0.008997 * c * c
                          + 1.0 / 69699.0 * c * c * c
                          - 1.0 / 14712000.0 * c * c * c * c;

    double moonNode = 93.2720993 
                      + 483202.0175273 * c
                      - 0.0034029 * c * c
                      - 1.0 / 3526000.0 * c * c * c
                      + 1.0 / 863310000.0 * c * c * c * c;

    double E = 1.0 - 0.002516 * c - 0.0000074 * c * c;
    
    double correction = 0.0;
    const int VWXYZ_SIZE = sizeof(LUNAR_LONGITUDE_VWXYZ) / sizeof(LUNAR_LONGITUDE_VWXYZ[0]);
    for (int i=0; i<VWXYZ_SIZE; i++)
    {
        double v = LUNAR_LONGITUDE_VWXYZ[i][0];
        double w = LUNAR_LONGITUDE_VWXYZ[i][1];
        double x = LUNAR_LONGITUDE_VWXYZ[i][2];
        double y = LUNAR_LONGITUDE_VWXYZ[i][3];
        double z = LUNAR_LONGITUDE_VWXYZ[i][4];

        correction += v * pow(E, abs(x)) *
                      sin( (w * elongation + x * solarAnomaly + y * lunarAnomaly + z * moonNode) * M_PI / 180.0);
    }
    correction *= 1.0 / 1000000.0;

    double venus = 3958.0 / 1000000.0 * sin( (119.75 + c * 131.849) * M_PI / 180.0);
    double jupiter = 318.0 / 1000000.0 * sin( (53.09 + c * 479264.29) * M_PI / 180.0);
    double flatEarth = 1962.0 / 1000000.0 * sin( (meanMoon - moonNode) * M_PI / 180.0);
    double nutation = Nutation(t);

    double lunarLon = Mod( meanMoon + correction + venus + jupiter + flatEarth + nutation, 360.0);
    return lunarLon;
}


double libcalendar::LunarPhase(double dt)
{
    return Mod( LunarLongitude(dt) - SolarLongitude(dt), 360.0);
}


double libcalendar::NewMoonAfter(double dt)
{
    double t0 = NthNewMoon(0);
    double phi = LunarPhase(dt);
    int n = (int) Round( (dt - t0) / MEAN_SYNODIC_MONTH - phi / 360.0);

    int k = n-1;
    while ( NthNewMoon(k) < dt)
    {
        k++;
    }
    
    return NthNewMoon(k);
}


double libcalendar::NewMoonBefore(double dt)
{
    double t0 = NthNewMoon(0);
    double phi = LunarPhase(dt);
    int n = (int) Round( (dt - t0) / MEAN_SYNODIC_MONTH - phi / 360.0);

    int k = n;
    while (NthNewMoon(k) >= dt)
    {
        k--;
    }

    return NthNewMoon(k);
}


static double LunarPhaseBeforeBisect(double u, double v, double phi)
{
    const double EPSILON = 1e-5;

    double x = (u + v ) / 2.0;
    double fx = Mod( LunarPhase(x) - phi, 180.0);

    if ( (u-v) < EPSILON)
    {
        return x;
    }
    else if (fx < 180.0)
    {
        return LunarPhaseBeforeBisect(u, x, phi);
    }
    else
    {
        return LunarPhaseBeforeBisect(x, v, phi);
    }
}


double libcalendar::LunarPhaseBefore(double dt, double phi)
{
    double tao = dt - 1.0 / 360.0 * MEAN_SYNODIC_MONTH * Mod( LunarPhase(dt) - phi, 360.0);
    double l = tao - 2.0;
    double u = min( dt, tao + 2.0 );

    return LunarPhaseBeforeBisect(l, u, phi);
}


static double LunarPhaseAfterBisect(double u, double v, double phi)
{
    const double EPSILON = 1e-5;

    double x = (u + v) / 2.0;
    double fx = Mod( LunarPhase(x) - phi, 360.0);

    if ( (u-v) < EPSILON)
    {
        return x;
    }
    else if ( fx < 180.0)
    {
        return LunarPhaseAfterBisect(u, x, phi);
    }
    else
    {
        return LunarPhaseAfterBisect(x, v, phi);
    }
}


double libcalendar::LunarPhaseAfter(double dt, double phi)
{
    double tao = dt + 1.0 / 360.0 * MEAN_SYNODIC_MONTH * Mod( phi - LunarPhase(dt), 360.0);
    double u = tao + 2.0;
    double l = max( dt, tao - 2.0);

    return LunarPhaseAfterBisect(l, u, phi);
}


static const double LUNAR_LATITUDE_VWXYZ[][5] =
{
    { 5128122, 0, 0, 0, 1}, 
    { 277693, 0, 0, 1, -1}, 
    { 55413, 2, 0, -1, 1}, 
    { 32573, 2, 0, 0, 1}, 
    { 9266, 2, 0, 1, -1}, 
    { 8216, 2, -1, 0, -1}, 
    { 4200, 2, 0, 1, 1}, 
    { 2463, 2, -1, -1, 1}, 
    { 2065, 2, -1, -1, -1}, 
    { 1828, 4, 0, -1, -1}, 
    { -1749, 0, 0, 0, 3}, 
    { -1491, 1, 0, 0, 1}, 
    { -1410, 0, 1, 1, -1}, 
    { -1335, 1, 0, 0, -1}, 
    { 1021, 4, 0, 0, -1}, 
    { 777, 0, 0, 1, -3}, 
    { 607, 2, 0, 0, -3}, 
    { 491, 2, -1, 1, -1}, 
    { 439, 0, 0, 3, -1}, 
    { 421, 2, 0, -3, -1}, 
    { -351, 2, 1, 0, 1}, 
    { 315, 2, -1, 1, 1}, 
    { -283, 0, 0, 1, 3}, 
    { 223, 1, 1, 0, -1}, 
    { -220, 0, 1, -2, -1}, 
    { -185, 1, 0, 1, 1}, 
    { -177, 0, 1, 2, 1}, 
    { 166, 4, -1, -1, -1}, 
    { 132, 4, 0, 1, -1}, 
    { 115, 4, -1, 0, -1}, 
    { 280602, 0, 0, 1, 1}, 
    { 173237, 2, 0, 0, -1}, 
    { 46271, 2, 0, -1, -1}, 
    { 17198, 0, 0, 2, 1}, 
    { 8822, 0, 0, 2, -1}, 
    { 4324, 2, 0, -2, -1}, 
    { -3359, 2, 1, 0, -1}, 
    { 2211, 2, -1, 0, 1}, 
    { -1870, 0, 1, -1, -1}, 
    { -1794, 0, 1, 0, 1}, 
    { -1565, 0, 1, -1, 1}, 
    { -1475, 0, 1, 1, 1}, 
    { -1344, 0, 1, 0, -1}, 
    { 1107, 0, 0, 3, 1}, 
    { 833, 4, 0, -1, 1}, 
    { 671, 4, 0, -2, 1}, 
    { 596, 2, 0, 2, -1}, 
    { -451, 2, 0, -2, 1}, 
    { 422, 2, 0, 2, 1}, 
    { -366, 2, 1, -1, 1}, 
    { 331, 4, 0, 0, 1}, 
    { 302, 2, -2, 0, -1}, 
    { -229, 2, 1, 1, -1}, 
    { 223, 1, 1, 0, 1}, 
    { -220, 2, 1, -1, -1}, 
    { 181, 2, -1, -2, -1}, 
    { 176, 4, 0, -2, -1}, 
    { -164, 1, 0, 1, -1}, 
    { -119, 1, 0, -2, -1}, 
    { 107, 2, 2, 0, 1}
};


double libcalendar::LunarLatitude(double dt)
{
    double c = JulianCenturies(dt);
    double longitude = 218.3164591 
                       + 481267.88134236 * c 
                       - 0.0013268 * c * c
                       + 1.0 / 538841.0 * c * c * c
                       - 1.0 / 65194000.0 * c * c * c * c;
    double elongation = 297.8502042 
                        + 445267.1115168 * c
                        - 0.00163 * c * c
                        + 1.0 / 545868.0 * c * c * c
                        - 1.0 / 113065000.0 * c * c * c * c;
    double solarAnomaly = 357.5291092
                          + 35999.0502909 * c 
                          - 0.0001536 * c * c
                          + 1.0 / 24490000.0 * c * c * c;
    double lunarAnomaly = 134.9634114
                          + 477198.8676313 * c 
                          + 0.008997 * c * c
                          + 1.0 / 69699.0 * c * c * c
                          - 1.0 / 14712000.0 * c * c * c * c;
    double moonNode = 93.2720993
                      + 483202.0175273 * c 
                      - 0.0034029 * c * c
                      - 1.0 / 3526000.0 * c * c * c
                      + 1.0 / 863310000.0 * c * c * c * c;
    double E = 1.0 - 0.002516 * c - 0.0000074 * c * c;

    double latitude = 0.0;
    const int VWXYZ_SIZE = sizeof(LUNAR_LATITUDE_VWXYZ) / sizeof(LUNAR_LATITUDE_VWXYZ[0]);
    for (int i=0; i<VWXYZ_SIZE; i++)
    {
        double v = LUNAR_LATITUDE_VWXYZ[i][0];
        double w = LUNAR_LATITUDE_VWXYZ[i][1];
        double x = LUNAR_LATITUDE_VWXYZ[i][2];
        double y = LUNAR_LATITUDE_VWXYZ[i][3];
        double z = LUNAR_LATITUDE_VWXYZ[i][4];
        
        latitude += v * pow(E, abs(x))
                    * sin( (w * elongation + x * solarAnomaly + y * lunarAnomaly + z * moonNode) * M_PI / 180.0);
    }
    latitude *= 1.0 / 1000000.0;

    double venus = 175.0 / 1000000.0 
                   * ( sin( (119.75 + c * 131.849 + moonNode) * M_PI / 180.0) +
                       sin( (119.75 + c * 131.849 - moonNode) * M_PI / 180.0) );
    double flatEarth = -2235.0 / 1000000.0 * sin( longitude * M_PI / 180.0)
                       + 127.0 / 1000000.0 * sin( (longitude - lunarAnomaly) * M_PI / 180.0)
                       - 115.0 / 1000000.0 * sin( (longitude + lunarAnomaly) * M_PI / 180.0);
    double extra = 382.0 / 1000000.0 * sin( (313.45 + c * 481266.484) * M_PI / 180.0);

    return Mod(latitude + venus + flatEarth + extra, 360.0);
}


double libcalendar::LunarAltitude(double dt, const Locale& l)
{
    double phi = l.Lat();
    double psi = l.Lon();

    double epsilon = Obliquity(dt);
    double lambda = LunarLongitude(dt);
    double beta = LunarLatitude(dt);

    double alpha = Arctan( (sin(lambda * M_PI / 180.0) * cos(epsilon * M_PI / 180.0) -
                            tan(beta * M_PI / 180.0) * sin(epsilon * M_PI / 180.0))
                            / cos(lambda * M_PI / 180.0), (int) floor(lambda / 90.0) + 1);

    double delta = asin( sin(beta * M_PI / 180.0) * cos(epsilon * M_PI / 180.0) + 
                         cos(beta * M_PI / 180.0) * sin(epsilon * M_PI / 180.0) * sin(lambda * M_PI / 180.0) );

    double theta_0 = Sidereal(dt);
    double H = Mod(theta_0 + psi - alpha, 360.0);
    double altitude = asin( sin(theta_0 * M_PI / 180.0) * sin(delta * M_PI / 180.0) +
                            cos(phi * M_PI / 180.0) * cos(delta * M_PI / 180.0) * cos(H * M_PI / 180.0) );

    return Mod(altitude + 180.0, 360.0) - 180.0;
}


double libcalendar::MomentFromDepression(double approx, const Locale& l, double alpha)
{
    double phi = l.Lat();
    double t = UniversalFromLocal(approx, l);
    double delta = asin( sin(Obliquity(t) * DEG_TO_RAD) * sin( SolarLongitude(t) * DEG_TO_RAD) ) * RAD_TO_DEG;
    bool morning = Mod(approx, 1.0) < 0.5;
    double sineOffset = tan(phi * DEG_TO_RAD) * tan(delta * DEG_TO_RAD) + 
                        sin(alpha * DEG_TO_RAD) / (cos(delta * DEG_TO_RAD) * cos(phi * DEG_TO_RAD) );

    if (abs(sineOffset) <= 1)
    {
        double moment = floor(approx) + 0.5 + 
                        (morning ? -1.0 : 1.0) * (Mod(0.5 + asin(sineOffset) * RAD_TO_DEG / 360.0, 1.0) - 0.25);

        return LocalFromApparent(moment);
    }
    else
    {
        return numeric_limits<double>::quiet_NaN();
    }
}


double libcalendar::Dawn(int fd, const Locale& locale, double alpha)
{
    double approx = MomentFromDepression( (double) fd + 0.25, locale, alpha);
    double result;
    if (_isnan(approx))
    {
        result = MomentFromDepression( (double) fd, locale, alpha);
    }
    else
    {
        result = MomentFromDepression( approx, locale, alpha);
    }

    return StandardFromLocal(result, locale);
}


double libcalendar::Dusk(int fd, const Locale& locale, double alpha)
{
    double approx = MomentFromDepression( (double) fd + 0.75, locale, alpha);
    double result;
    if (_isnan(approx))
    {
        result = MomentFromDepression((double) fd + 0.99, locale, alpha);
    }
    else
    {
        result = MomentFromDepression( approx, locale, alpha);
    }

    if (_isnan(result))
    {
        return numeric_limits<double>::quiet_NaN();
    }
    else
    {
        return StandardFromLocal(result, locale);
    }

}




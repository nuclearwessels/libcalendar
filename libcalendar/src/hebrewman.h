

#pragma once

#include "fixeddateman.h"
#include "eventman.h"
#include "hebrew.h"

namespace libcalendarman
{

public ref class HebrewDate : public FixedDate
{
public:
	HebrewDate();
	HebrewDate(int fixed);
	HebrewDate(int year, int month, int day);
    !HebrewDate();
    ~HebrewDate();

    property int Year
    {
        int get() { return _hd->Year(); }
        void set(int year) { _hd->SetYear(year); }
    }

    property int Month
    {
        int get() { return _hd->Month(); }
        void set(int month) { _hd->SetMonth(month); }
    }

    property int Day
    {
        int get() { return _hd->Day(); }
        void set(int day) { _hd->SetDay(day); }
    }

	virtual void FromFixed(int fd) override;
	virtual int ToFixed() override;

	virtual System::String^ ToString() override;
	
	static System::String^ GetMonthName(int hebrewMonth, bool leapYear);
	static bool IsLeapYear(int year);
	static int GetLastMonthOfYear(int year);
	static double GetMolad(int hebrewMonth, int hebrewYear);
	static int GetElapsedDays(int hebrewYear);
	static int GetNewYearDelay(int hebrewYear);
	static int GetNewYear(int hebrewYear);
	static int GetLastDayOfMonth(int hebrewMonth, int hebrewYear);
	static bool IsLongMarheshvan(int hebrewYear);
	static bool IsShortKislev(int hebrewYear);
	static int GetDaysInYear(int hebrewYear);		
	static int GetYomKippur(int gregorianYear);
	static int GetRoshHaShanah(int gregorianYear);
	static int GetSukkot(int gregorianYear);
	static int GetHoshanaRabba(int gregorianYear);
	static int GetSheminiAzeret(int gregorianYear);
	static int GetSimhatTorah(int gregorianYear);
	static int GetPassover(int gregorianYear);
	static bool GetOmer(int fd, int% completedWeeks, int% excessDays);
	static int GetShavuot(int gregorianYear);
	static int GetPurim(int gregorianYear);
	static int GetTaAnitEsther(int gregorianYear);
	static int GetTishahBeAv(int gregorianYear);
	static int GetYomHaZikkaron(int gregorianYear);
	static int GetShEla(int gregorianYear);
	static array<int>^ GetBirkathHaHama(int gregorianYear);
	
	static int GetHebrewBirthday(int birthdate, int hebrewYear);
	static array<int>^ GetHebrewBirthdayInGregorian(int birthdate, int gregorianYear);
	
	static int GetYahrzeit(int deathdate, int hebrewYear);
	static array<int>^ GetYahrzeitInGregorian(int deathdate, int gregorianYear);
	
	static const int Epoch = libcalendar::HebrewDate::EPOCH;
	
    static initonly array<System::String^>^ DaysOfWeek = gcnew array<String^>
    {
        gcnew String( libcalendar::HebrewDate::DAYS_OF_WEEK[0].c_str()),
        gcnew String( libcalendar::HebrewDate::DAYS_OF_WEEK[1].c_str()),
        gcnew String( libcalendar::HebrewDate::DAYS_OF_WEEK[2].c_str()),
        gcnew String( libcalendar::HebrewDate::DAYS_OF_WEEK[3].c_str()),
        gcnew String( libcalendar::HebrewDate::DAYS_OF_WEEK[4].c_str()),
        gcnew String( libcalendar::HebrewDate::DAYS_OF_WEEK[5].c_str()),
        gcnew String( libcalendar::HebrewDate::DAYS_OF_WEEK[6].c_str())
    };

    static initonly array<EventType^>^ HebrewEventTypes = gcnew array<EventType^>
    {
        gcnew EventType("Hebrew", "Hebrew New Year", gcnew SingleEventDelegate(HebrewDate::GetNewYear), gcnew DateCreateDelegate(HebrewDate::CreateHebrewDate) ),
        gcnew EventType("Hebrew", "Yom Kippur", gcnew SingleEventDelegate(HebrewDate::GetYomKippur), gcnew DateCreateDelegate(HebrewDate::CreateHebrewDate)),
        gcnew EventType("Hebrew", "Rosh Ha Shanah", gcnew SingleEventDelegate(HebrewDate::GetRoshHaShanah), gcnew DateCreateDelegate(HebrewDate::CreateHebrewDate)),
        gcnew EventType("Hebrew", "Sukkot", gcnew SingleEventDelegate(HebrewDate::GetSukkot), gcnew DateCreateDelegate(HebrewDate::CreateHebrewDate)),
        gcnew EventType("Hebrew", "Hoshana Rabba", gcnew SingleEventDelegate(HebrewDate::GetHoshanaRabba), gcnew DateCreateDelegate(HebrewDate::CreateHebrewDate)),
        gcnew EventType("Hebrew", "Shemini Azeret", gcnew SingleEventDelegate(HebrewDate::GetSheminiAzeret), gcnew DateCreateDelegate(HebrewDate::CreateHebrewDate)),
        gcnew EventType("Hebrew", "Simhat Torah", gcnew SingleEventDelegate(HebrewDate::GetSimhatTorah), gcnew DateCreateDelegate(HebrewDate::CreateHebrewDate)),
        gcnew EventType("Hebrew", "Passover", gcnew SingleEventDelegate(HebrewDate::GetPassover), gcnew DateCreateDelegate(HebrewDate::CreateHebrewDate)),
        gcnew EventType("Hebrew", "Shavuot", gcnew SingleEventDelegate(HebrewDate::GetShavuot), gcnew DateCreateDelegate(HebrewDate::CreateHebrewDate)),
        gcnew EventType("Hebrew", "Purim", gcnew SingleEventDelegate(HebrewDate::GetPurim), gcnew DateCreateDelegate(HebrewDate::CreateHebrewDate)),
        gcnew EventType("Hebrew", "Ta Anit Esther", gcnew SingleEventDelegate(HebrewDate::GetTaAnitEsther), gcnew DateCreateDelegate(HebrewDate::CreateHebrewDate)),
        gcnew EventType("Hebrew", "Yom Ha Zikkaron", gcnew SingleEventDelegate(HebrewDate::GetYomHaZikkaron), gcnew DateCreateDelegate(HebrewDate::CreateHebrewDate)),
        gcnew EventType("Hebrew", "Sh Ela", gcnew SingleEventDelegate(HebrewDate::GetShEla), gcnew DateCreateDelegate(HebrewDate::CreateHebrewDate)),
        gcnew EventType("Hebrew", "Birkath Ha Hama", gcnew MultipleEventDelegate(HebrewDate::GetBirkathHaHama), gcnew DateCreateDelegate(HebrewDate::CreateHebrewDate))
    };

    static FixedDate^ CreateHebrewDate(int fd);

    static array<Event^>^ GetHebrewEvents(int gregorianYear);
	
private:
	libcalendar::HebrewDate* _hd;
};


}

#include "coptic.h"
#include "gregorian.h"
#include "julian.h"

#include "validate.h"

#include <string>
#include <sstream>

using namespace libcalendar;


const int CopticDate::EPOCH = 103605;


const std::vector<std::string> MONTHS = 
{ 
    "Thoout", "Paope", "Athor", "Koiak",
	"Tobe", "Meshir", "Paremotep", "Parmoute", "Pashons", "Paone", "Epep",
	"Mesore", "Epagomene" 
};

const std::vector<std::string> CopticDate::DAYS_OF_WEEK = 
{ 
    "Tkyriake", "Pesnau", "Pshoment",
	"Peftoou", "Ptiou", "Psoou", "Psabbaton" 
};


CopticDate::CopticDate()
{
	FromFixed(EPOCH);
}


CopticDate::CopticDate(int fixed)
{
	FromFixed(fixed);
}


CopticDate::CopticDate(int year, int month, int day)
{
	year_ = year;
    month_ = month;
    day_ = day;

    ValidateDate();
}


void CopticDate::SetYear(int year)
{
	year_ = year;
    ValidateDate();
}


int CopticDate::Year() const
{
	return year_;
}


void CopticDate::SetMonth(int month)
{
	month_ = month;
    ValidateDate();
}


int CopticDate::Month() const
{
	return month_;
}


void CopticDate::SetDay(int day)
{	
	day_ = day;
    ValidateDate();
}


int CopticDate::Day() const
{
	return day_;
}


void CopticDate::ValidateDate() const
{
    ValidateBetween(month_, 1, 13);
    ValidateGreaterEqual(day_, 0);
    ValidateLessEqual(day_, (month_ < 13) ? 30 : (IsLeapYear(year_) ? 6 : 5));
}


void CopticDate::FromFixed(int fd)
{
	year_ = Floor( 4 * (fd - EPOCH) + 1463, 1461);	
    month_ = Floor( fd - CopticDate(year_, 1, 1).ToFixed(), 30) + 1;
	day_ = fd + 1 - CopticDate(year_, month_, 1).ToFixed();
	
    ValidateDate();
}


int CopticDate::ToFixed() const
{
	int returnme = EPOCH - 1;
	returnme += 365 * (year_-1);
	returnme += Floor( year_, 4);
	returnme += 30 * (month_ - 1);
	returnme += day_;
	return returnme;
}


std::string CopticDate::ToString() const
{
    std::stringstream ss;
    ss << MONTHS[month_] << "(" << month_ << ") " << day_ << ", " << year_ << " A.M.";

	return ss.str();
}


std::vector<int> CopticDate::GetCopticInGregorian(int gregorianYear, int copticMonth, int copticDay)
{
    std::vector<int> results;
	
	int jan1 = GregorianDate(gregorianYear, 1, 1).ToFixed();
	int dec31 = GregorianDate(gregorianYear, 12, 31).ToFixed();

	int y = CopticDate(jan1).Year();

	int date1 = CopticDate(y, copticMonth, copticDay).ToFixed();
	int date2 = CopticDate(y+1, copticMonth, copticDay).ToFixed();

	if (date1 >= jan1  &&  date1<=dec31)
	{
		results.push_back(date1);
	}

	if (date2>=jan1  &&  date2<=dec31)
	{
		results.push_back(date2);
	}

    return results;
}


std::vector<int> CopticDate::GetCopticChristmas(int gregorianYear)
{
	return GetCopticInGregorian(gregorianYear, 4, 29);
}


bool CopticDate::IsLeapYear(int copticYear)
{
	return (Mod(copticYear, 4) == 3) ? true : false;
}


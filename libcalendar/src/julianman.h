

#ifdef _MANAGED

#include "fixeddateman.h"
#include "julian.h"
#include "gregorian.h"
#include "eventman.h"

using namespace System;

namespace libcalendarman
{
    
public ref class JulianDate : public FixedDate
{
public:
	JulianDate();
	JulianDate(int fd);
	JulianDate(int year, int month, int day);
    JulianDate(double jd, bool isModifiedJulianDay);
    !JulianDate();
    ~JulianDate();

    property int Year
    {
        void set(int year) { _jd->SetYear(year); }
        int get() { return _jd->Year(); }
    }

    property int Month
    {
        void set(int month) { _jd->SetMonth(month); }
        int get() { return _jd->Month(); }
    }

    property int PreviousMonth
    {
        int get() { return _jd->PreviousMonth(); }
    }

    property int NextMonth
    {
        int get() { return _jd->NextMonth(); }
    }

    property int Day
    {
        void set(int day) { _jd->SetDay(day);  }
        int get() { return _jd->Day(); }
    }

    property double JulianDay
    {
        void set(double jd) { _jd->SetJulianDay(jd); }
        double get() { return _jd->JulianDay(); }
    }

    property double ModifiedJulianDay
    {
        void set(double mjd) { _jd->SetModifiedJulianDay(mjd); }
        double get() { return _jd->ModifiedJulianDay(); }
    }

	virtual int ToFixed() override;
	virtual void FromFixed(int fixed) override;

	virtual String^ ToString() override;

	static bool IsLeapYear(int year);

	static array<int>^ GetJulianInGregorian(int gregorianYear, int julianMonth, int julianDay);

	static array<int>^ GetEasternOrthodoxChristmas(int gregorianYear);
	static int GetOrthodoxEaster(int gregorianYear);

	static initonly int Epoch = libcalendar::GregorianDate(0, 12, 30).ToFixed();
    static initonly array<String^>^ Months = gcnew array<String^>
    {
        gcnew String(libcalendar::JulianDate::MONTHS[0].c_str()),
        gcnew String(libcalendar::JulianDate::MONTHS[1].c_str()),
        gcnew String(libcalendar::JulianDate::MONTHS[2].c_str()),
        gcnew String(libcalendar::JulianDate::MONTHS[3].c_str()),
        gcnew String(libcalendar::JulianDate::MONTHS[4].c_str()),
        gcnew String(libcalendar::JulianDate::MONTHS[5].c_str()),
        gcnew String(libcalendar::JulianDate::MONTHS[6].c_str()),
        gcnew String(libcalendar::JulianDate::MONTHS[7].c_str()),
        gcnew String(libcalendar::JulianDate::MONTHS[8].c_str()),
        gcnew String(libcalendar::JulianDate::MONTHS[9].c_str()),
        gcnew String(libcalendar::JulianDate::MONTHS[10].c_str()),
        gcnew String(libcalendar::JulianDate::MONTHS[11].c_str())
    };

    static initonly array<EventType^>^ OrthodoxEventTypes = gcnew array<EventType^>
    {
        gcnew EventType("Christian Orthodox", "Orthodox Christmas", gcnew MultipleEventDelegate(JulianDate::GetEasternOrthodoxChristmas), gcnew DateCreateDelegate(JulianDate::CreateJulianDate) ),
        gcnew EventType("Christian Orthodox", "Orthodox Easter",    gcnew SingleEventDelegate(JulianDate::GetOrthodoxEaster), gcnew DateCreateDelegate(JulianDate::CreateJulianDate) )
    };

    static FixedDate^ CreateJulianDate(int fd);

private:
    libcalendar::JulianDate* _jd;
};

}

#endif



#include "egyptian.h"

using namespace libcalendar;
using namespace std;


EgyptianDate::EgyptianDate()
{
    FromFixed(EPOCH);
}

EgyptianDate::EgyptianDate(int fd)
{
    FromFixed(fd);
}

EgyptianDate::EgyptianDate(int year, int month, int day) : _year(year), 
                                                           _month(month),
                                                           _day(day)
{

}


void EgyptianDate::SetYear(int year)
{
    _year = year;
}

int EgyptianDate::Year() const
{
    return _year;
}

void EgyptianDate::SetMonth(int month)
{
    _month = month;
}

int EgyptianDate::Month() const
{
    return _month;
}

void EgyptianDate::SetDay(int day)
{
    _day = day;
}

int EgyptianDate::Day() const
{
    return _day;
}


void EgyptianDate::FromFixed(int fd)
{
    int days = fd - EPOCH;
    _year = Floor(days, 365) + 1;
    _month = Floor( Mod(days, 365), 30) + 1;
    _day = days - 365 * (_year - 1) - 30 * (_month - 1) + 1;
}


int EgyptianDate::ToFixed() const
{
    return EPOCH + 365 * (_year - 1) + 30 * (_month - 1) + _day - 1;
}


string EgyptianDate::ToString() const
{
    char temp[100];
    snprintf(temp, 100, "year %d month %d day %d", _year, _month, _day);
    return string(temp);
}



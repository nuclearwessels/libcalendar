
#pragma once

#include "fixeddateman.h"
#include "islamic.h"

namespace libcalendarman
{

public ref class IslamicDate : public FixedDate
{
public:
	IslamicDate();
	IslamicDate(int fd);
	IslamicDate(int year, int month, int day);
    !IslamicDate();
    ~IslamicDate();

    property int Year
    {
        int get() { return id->Year(); }
        void set(int year) { id->SetYear(year); }
    }

    property int Month
    {
        int get() { return id->Month(); }
        void set(int month) { id->SetMonth(month); }
    }

    property int Day
    {
        int get() { return id->Day(); }
        void set(int day) { id->SetDay(day); }
    }

	virtual void FromFixed(int fixed) override;
	virtual int ToFixed() override;

	virtual System::String^ ToString() override;

	static bool IsLeapYear(int year);
	
	static array<int>^ GetIslamicInGregorian(int gregorianYear, int islamicMonth, int islamicDay);

	static const int Epoch = libcalendar::IslamicDate::EPOCH;
	static initonly array<System::String^>^ Months;
	static initonly array<System::String^>^ DaysOfWeek;

    static IslamicDate()
    {
        Months = gcnew array<System::String^>(12);
        for (int i=0; i<12; i++)
        {
            Months[i] = gcnew System::String(libcalendar::IslamicDate::MONTHS[i].c_str());
        }

        DaysOfWeek = gcnew array<System::String^>(7);
        for (int i=0; i<7; i++)
        {
            DaysOfWeek[i] = gcnew System::String(libcalendar::IslamicDate::DAYS_OF_WEEK[i].c_str());
        }
    }

private:
	libcalendar::IslamicDate* id;
};


}


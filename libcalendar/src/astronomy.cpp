

#include "astronomy.h"
#define _USE_MATH_DEFINES
#include <cmath>
#include <cassert>

using namespace libcalendar;


Locale::Locale() : _lat(0.0L), _lon(0.0L), _elev(0.0L), _timeZone(0)
{

}


Locale::Locale(double latDeg, double lonDeg, double elevMeters, int timeZone) :
    _lat(latDeg), _lon(lonDeg), _elev(elevMeters), _timeZone(timeZone)
{
    
}


void Locale::SetLat(double lat, bool degrees)
{
    _lat = degrees ? lat : lat * 180.0L / M_PI;
}


double Locale::Lat() const
{
    return _lat;
}


double Locale::LatRad() const
{
    return _lat * M_PI / 180.0L;
}


void Locale::SetLon(double lon, bool degrees)
{
    _lon = degrees ? lon : lon * 180.0L / M_PI;
}


double Locale::Lon() const
{
    return _lon;
}


double Locale::LonRad() const
{
    return _lon * M_PI / 180.0L;
}


void Locale::SetElev(double meters)
{
    _elev = meters;
}

double Locale::Elev() const
{
    return _elev;
}

void Locale::SetTimeZone(int zone)
{
    assert(zone <= 14  && zone >= -12);
    _timeZone = zone;
}

int Locale::TimeZone() const
{
    return _timeZone;
}



void Locale::ConvertDegToDMS(double degrees, double& deg, double& min, double& seconds)
{

    double fract = modf(degrees, &deg);
    
    //degrees -= deg;

    fract = abs(fract);

    fract *= 60.0L;
    fract = modf(fract, &min);

    fract *= 60.0L;
    seconds = fract;
}


double Locale::ConvertDMSToDeg(double deg, double min, double seconds)
{
    return (deg < 0.0L) ? (deg - min / 60.0L - seconds / 3600.0L) : (deg + min / 60.0L + seconds / 3600.0L);
}





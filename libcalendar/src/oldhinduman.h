

#pragma once

#include "fixeddateman.h"
#include "oldhindu.h"

using namespace System;

namespace libcalendarman
{

public ref class OldHindu abstract
{
public:
    static const int Epoch = -1132959;
    static const double AryaSolarYear = libcalendar::OldHindu::ARYA_SOLAR_YEAR;
    static const double AryaSolarMonth = libcalendar::OldHindu::ARYA_SOLAR_MONTH;
    static const double AryaJovianPeriod = libcalendar::OldHindu::ARYA_JOVIAN_PERIOD;

    static initonly array<String^>^ JovianCycles = gcnew array<String^>
    { 
	    "Vijaya", "Jaya", "Manmatha", "Durmukha", "Hemalamba",
	    "Vilamba", "Vikarin", "Sarvari", "Plava", "Subhakrit",
	    "Sobhana", "Krodhin", "Visvavasu", "Parabhava", "Plavanga",
	    "Kilaka", "Saumya", "Sadharana", "Virodhakrit", "Paridhavin", 
	    "Pramadin", "Ananda", "Rakshasa", "Anala", "Rakshasa",
	    "Anala", "Pingala", "Kalayukta", "Siddharthin", "Raudra", "Durmati", "Dundubhi",
	    "Rudhirodgarin", "Raktaksha", "Krodhana", "Kshaya", "Prabhava",
	    "Vibhava", "Sukla", "Pramoda", "Prajapati", "Angiras", 
	    "Srumukha", "Bhava", "Yuvan", "Dhatri", "Isvara",
	    "Bahudhanya", "Pramathin", "Vikrama", "Vrisha", "Chitrabhanu",
	    "Subhanu", "Tarana", "Parthiva", "Vyaya", "Sarvajit", 
	    "Sarvadharin", "Virodhin", "Vikrita", "Khara", "Nandana" 
    };

    static int GetHinduDayCount(int fd);
    static int GetJovianYear(int fd);
};


public ref class OldHinduSolarDate : FixedDate
{
public:
    OldHinduSolarDate();
	OldHinduSolarDate(int year, int month, int day);
	OldHinduSolarDate(int fd);
    ~OldHinduSolarDate();

    property int Year
    {
        void set(int year) { _ohsd->SetYear(year); }
        int get() { return _ohsd->Year(); }
    }


    property int Month
    {
        void set(int month) { _ohsd->SetMonth(month); }
        int get() { return _ohsd->Month(); }
    }

    property int Day
    {
        void set(int day) { _ohsd->SetDay(day); }
        int get() { return _ohsd->Day(); }
    }

	virtual void FromFixed(int fd) override;
	virtual int ToFixed() override;
	virtual String^ ToString() override;
        
    static initonly array<String^>^ Months = gcnew array<String^>
    {
        gcnew String( libcalendar::OldHinduSolarDate::MONTHS[0].c_str()),
        gcnew String( libcalendar::OldHinduSolarDate::MONTHS[1].c_str()),
        gcnew String( libcalendar::OldHinduSolarDate::MONTHS[2].c_str()),
        gcnew String( libcalendar::OldHinduSolarDate::MONTHS[3].c_str()),
        gcnew String( libcalendar::OldHinduSolarDate::MONTHS[4].c_str()),
        gcnew String( libcalendar::OldHinduSolarDate::MONTHS[5].c_str()),
        gcnew String( libcalendar::OldHinduSolarDate::MONTHS[6].c_str()),
        gcnew String( libcalendar::OldHinduSolarDate::MONTHS[7].c_str()),
        gcnew String( libcalendar::OldHinduSolarDate::MONTHS[8].c_str()),
        gcnew String( libcalendar::OldHinduSolarDate::MONTHS[9].c_str()),
        gcnew String( libcalendar::OldHinduSolarDate::MONTHS[10].c_str()),
        gcnew String( libcalendar::OldHinduSolarDate::MONTHS[11].c_str())
    };

protected:
    !OldHinduSolarDate();

private:
    libcalendar::OldHinduSolarDate* _ohsd;
};



public ref class OldHinduLunarDate : FixedDate
{
public:
    OldHinduLunarDate();
    OldHinduLunarDate(int fd);
    OldHinduLunarDate(int year, int month, bool leap, int day);
    ~OldHinduLunarDate();

    property int Year
    {
        void set(int year) { _ohld->SetYear(year); }
        int get() { return _ohld->Year(); }
    }


    property int Month
    {
        void set(int month) { _ohld->SetMonth(month); }
        int get() { return _ohld->Month(); }
    }

    property int Day
    {
        void set(int day) { _ohld->SetDay(day); }
        int get() { return _ohld->Day(); }
    }

    property bool Leap
    {
        void set(bool leap) { _ohld->SetLeap(leap); }
        bool get() { return _ohld->Leap(); }
    }

    virtual int ToFixed() override;
    virtual void FromFixed(int fd) override;

    static bool IsLeapYear(int lunarYear);

    static const double AryaLunarMonth = 1577917500.0L / 5343336.0L;
    static const double AryaLunarDay = AryaLunarMonth / 30.0L;

    static initonly array<String^>^ Months = gcnew array<String^>
    {
        gcnew String( libcalendar::OldHinduLunarDate::MONTHS[0].c_str() ),
        gcnew String( libcalendar::OldHinduLunarDate::MONTHS[1].c_str() ),
        gcnew String( libcalendar::OldHinduLunarDate::MONTHS[2].c_str() ),
        gcnew String( libcalendar::OldHinduLunarDate::MONTHS[3].c_str() ),
        gcnew String( libcalendar::OldHinduLunarDate::MONTHS[4].c_str() ),
        gcnew String( libcalendar::OldHinduLunarDate::MONTHS[5].c_str() ),
        gcnew String( libcalendar::OldHinduLunarDate::MONTHS[6].c_str() ),
        gcnew String( libcalendar::OldHinduLunarDate::MONTHS[7].c_str() ),
        gcnew String( libcalendar::OldHinduLunarDate::MONTHS[8].c_str() ),
        gcnew String( libcalendar::OldHinduLunarDate::MONTHS[9].c_str() ),
        gcnew String( libcalendar::OldHinduLunarDate::MONTHS[10].c_str() ),
        gcnew String( libcalendar::OldHinduLunarDate::MONTHS[11].c_str() )
    };

protected:
    !OldHinduLunarDate();

private:
    libcalendar::OldHinduLunarDate* _ohld;
};


}




#pragma once

#include "fixeddate.h"


namespace libcalendar
{


class MayanLongCountDate : FixedDate
{
public:
    MayanLongCountDate();
    MayanLongCountDate(int fd);
    MayanLongCountDate(int baktun, int katun, int tun, int uinal, int kin);

    void SetBaktun(int baktun);
    int Baktun() const;

    void SetKatun(int katun);
    int Katun() const;

    void SetTun(int tun);
    int Tun() const;

    void SetUinal(int uinal);
    int Uinal() const;

    void SetKin(int kin);
    int Kin() const;

    int ToFixed() const;
    void FromFixed(int fd);

    std::string ToString() const;

    static const int MAYAN_EPOCH = -1137142;

private:
    int _baktun, _katun, _tun, _uinal, _kin;
};


class MayanHaabDate : FixedDate
{
public:
    MayanHaabDate();
    MayanHaabDate(int fd);
    MayanHaabDate(int month, int day);

    void SetMonth(int month);
    int Month() const;

    void SetDay(int day);
    int Day() const;

    int ToFixed() const;
    void FromFixed(int fd);

    std::string ToString() const;

    static const std::string MONTHS[];
    static const int MAYAN_HAAB_EPOCH;

    static int GetMayanHaabOrdinal(int month, int day);
    static int GetMayanHaabOnOrBefore(const MayanHaabDate& mhd, int fd);

private:
    int _month, _day;
};



class MayanTzolkinDate
{
public:
    static const std::string MONTHS[];
    static const int MAYAN_TZOLKIN_EPOCH;

    int Number() const;
    int Name() const;

    MayanTzolkinDate();
    MayanTzolkinDate(int fd);
    MayanTzolkinDate(int number, int name);

    int ToFixed() const;
    void FromFixed(int fd);
    std::string ToString() const;

    static int GetMayanTzolkinOrdinal(int number, int name);
    static int GetMayanTzolkinOnOrBefore(const MayanTzolkinDate& mtd, int fd);

private:
    int _name, _number;
};


int GetMayanCalendarRoundOnOrBefore(const MayanHaabDate& mhd, const MayanTzolkinDate& mtd, int fd);

}


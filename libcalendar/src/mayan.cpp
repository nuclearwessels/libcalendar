

#include "mayan.h"

#include <stdexcept>

using namespace libcalendar;
using namespace std;


MayanLongCountDate::MayanLongCountDate()
{
    FromFixed(MAYAN_EPOCH);
}


MayanLongCountDate::MayanLongCountDate(int fd) 
{
    FromFixed(fd);
}


MayanLongCountDate::MayanLongCountDate(int baktun, int katun, int tun, int uinal, int kin) :
    _baktun(baktun),
    _katun(katun),
    _tun(tun),
    _uinal(uinal),
    _kin(kin)
{

}


void MayanLongCountDate::SetBaktun(int baktun)
{
    _baktun = baktun;
}

int MayanLongCountDate::Baktun() const
{
    return _baktun;
}

void MayanLongCountDate::SetKatun(int katun)
{
    _katun = katun;
}

int MayanLongCountDate::Katun() const
{
    return _katun;
}

void MayanLongCountDate::SetTun(int tun)
{
    _tun = tun;
}

int MayanLongCountDate::Tun() const
{
    return _tun;
}

void MayanLongCountDate::SetUinal(int uinal)
{
    _uinal = uinal;
}

int MayanLongCountDate::Uinal() const
{
    return _uinal;
}

void MayanLongCountDate::SetKin(int kin)
{
    _kin = kin;
}

int MayanLongCountDate::Kin() const
{
    return _kin;
}


int MayanLongCountDate::ToFixed() const
{
    return MAYAN_EPOCH + _baktun * 144000 + _katun * 7200 + _tun * 360 + _uinal * 20 + _kin;
}


void MayanLongCountDate::FromFixed(int fd)
{
    int longCount = fd - MAYAN_EPOCH;
    _baktun = Floor(longCount, 144000);

    int dayOfBaktun = Mod(longCount, 144000);
    _katun = Floor(dayOfBaktun, 7200);

    int dayOfKatun = Mod(dayOfBaktun, 7200);
    _tun = Floor(dayOfKatun, 360);

    int dayOfTun = Mod(dayOfKatun, 360);
    _uinal = Floor(dayOfTun, 20);

    _kin = Mod(dayOfTun, 20);
}


string MayanLongCountDate::ToString() const
{
    char temp[100];
    snprintf(temp, 100, "Baktun=%d, Katun=%d, Tun=%d, Uinal=%d, Kin=%d", _baktun, _katun, _tun, _uinal, _kin);
    return string(temp);
}



const string MayanHaabDate::MONTHS[] = { "Pop", "Uo", "Zip", "Zotz", "Tzec", "Xul", "Yaxkin",
                                         "Mol", "Chen", "Yax", "Zac", "Ceh", "Mac", "Kankin",
                                         "Muan", "Pax", "Kayab", "Cumku", "Uayeb" };
const int MayanHaabDate::MAYAN_HAAB_EPOCH = MayanLongCountDate::MAYAN_EPOCH - GetMayanHaabOrdinal(18, 8);


MayanHaabDate::MayanHaabDate() 
{
    FromFixed(MAYAN_HAAB_EPOCH);
}

MayanHaabDate::MayanHaabDate(int fd)
{
    FromFixed(fd);
}

MayanHaabDate::MayanHaabDate(int month, int day) : _month(month), _day(day)
{

}


void MayanHaabDate::SetMonth(int month)
{
    _month = month;
}


int MayanHaabDate::Month() const
{
    return _month;
}


void MayanHaabDate::SetDay(int day)
{
    _day = day;
}


int MayanHaabDate::Day() const
{
    return _day;
}


int MayanHaabDate::ToFixed() const
{
    throw runtime_error("Invalid operation");
}

void MayanHaabDate::FromFixed(int fd)
{
    int count = Mod(fd - MAYAN_HAAB_EPOCH, 365);
    _day = Mod(count, 20);
    _month = Floor(count, 20) + 1;
}



string MayanHaabDate::ToString() const
{
    char temp[100];
    snprintf(temp, 100, "%s %d, %d", MONTHS[_month - 1].c_str(), _month, _day);
    return string(temp);
}


int MayanHaabDate::GetMayanHaabOrdinal(int month, int day)
{
    return (month - 1) * 20 + day;
}


int MayanHaabDate::GetMayanHaabOnOrBefore(const MayanHaabDate& mhd, int fd)
{
    return MayanHaabDate(fd - Mod(fd - MAYAN_HAAB_EPOCH - GetMayanHaabOrdinal(mhd.Month(), mhd.Day()), 365));
}






const string MayanTzolkinDate::MONTHS[] = { "Imix", "Ik", "Akbal", "Kan", "Chicchan", "Cimi", "Manik",
                                            "Lamat", "Muluc", "Oc", "Chuen", "Eb", "Ben", "Ix",
                                            "Men", "Cib", "Caban", "Etzneb", "Cauac", "Ahau" };

const int MayanTzolkinDate::MAYAN_TZOLKIN_EPOCH = MayanLongCountDate::MAYAN_EPOCH - GetMayanTzolkinOrdinal(4, 20);


MayanTzolkinDate::MayanTzolkinDate()
{
    FromFixed(MAYAN_TZOLKIN_EPOCH);
}

MayanTzolkinDate::MayanTzolkinDate(int fd)
{
    FromFixed(fd);
}


MayanTzolkinDate::MayanTzolkinDate(int number, int name) : _number(number), _name(name)
{

}


int MayanTzolkinDate::Number() const
{
    return _number;
}

int MayanTzolkinDate::Name() const
{
    return _name;
}


int MayanTzolkinDate::ToFixed() const
{
    throw runtime_error("invalid operation");
}


void MayanTzolkinDate::FromFixed(int fd)
{
    int count = fd - MAYAN_TZOLKIN_EPOCH + 1;
    _number = Amod(count, 13);
    _name = Amod(count, 20);
}


string MayanTzolkinDate::ToString() const
{
    char temp[100];
    snprintf(temp, 100, "%d %s (%d)", _number, MONTHS[_name-1].c_str(), _name);
    return string(temp);
}

int MayanTzolkinDate::GetMayanTzolkinOrdinal(int number, int name)
{
    return Mod(number - 1 + 39 * (number - name), 260);
}

int MayanTzolkinDate::GetMayanTzolkinOnOrBefore(const MayanTzolkinDate& mtd, int fd)
{
    return fd - Mod(fd - MAYAN_TZOLKIN_EPOCH - GetMayanTzolkinOrdinal(mtd.Number(), mtd.Name()), 260);
}


int libcalendar::GetMayanCalendarRoundOnOrBefore(const MayanHaabDate& mhd, const MayanTzolkinDate& mtd, int fd)
{
    int haabCount = MayanHaabDate::GetMayanHaabOrdinal(mhd.Month(), mhd.Day()) + MayanHaabDate::MAYAN_HAAB_EPOCH;
    int tzolkinCount = MayanTzolkinDate::GetMayanTzolkinOrdinal(mtd.Number(), mtd.Name()) + MayanTzolkinDate::MAYAN_TZOLKIN_EPOCH;
    int diff = tzolkinCount - haabCount;

    if (Mod(diff, 5) == 0)
    {
        return fd - Mod(fd - haabCount - 365 * diff, 18980);
    }
    else
    {
        throw invalid_argument("not sure");
    }
}

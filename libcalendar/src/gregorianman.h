

#ifdef _MANAGED

#include "fixeddateman.h"
#include "eventman.h"
#include "gregorian.h"

using namespace System;


namespace libcalendarman
{

public ref class GregorianDate : public FixedDate
{
public:
    GregorianDate();
	GregorianDate(int fd);
	GregorianDate(int year, int month, int day);
    !GregorianDate();
    ~GregorianDate();

    property int Year
    {
        int get() { return _gd->Year(); }
        void set(int year) { _gd->SetYear(year); }
    }

    property int Month
    {
        int get() { return _gd->Month(); }
        void set(int month) { _gd->SetMonth(month); }
    }

    property int Day
    {
        int get() { return _gd->Day(); }
        void set(int day) { _gd->SetDay(day); }
    }

    property int OrdinalDayOfYear
    {
        int get() { return _gd->OrdinalDayOfYear(); }
    }

    property int NextMonth
    {
        int get() { return _gd->NextMonth(); }
    }

    property int PreviousMonth
    {
        int get() { return _gd->PreviousMonth(); }
    }

	virtual int ToFixed() override;
	virtual void FromFixed(int fixed) override;
	virtual String^ ToString() override;

	static bool IsLeapYear(int gregorianYear);
	static int GetYearFromFixed(int fixedDate);

	static int GetUSMemorialDay(int year);
	static int GetUSLaborDay(int year);
	static int GetUSElectionDay(int year);
	static int GetUSThanksgiving(int year);
	static int GetUSDSTBegin(int year);
	static int GetUSDSTEnd(int year);

	static int GetEaster(int gregorianYear);
	static int GetSeptuagesimaSunday(int gyear);
	static int GetSexagesimaSunday(int gyear);
	static int GetShroveSunday(int gyear);
	static int GetShroveMonday(int gyear);
	static int GetMardiGras(int gyear);
	static int GetAshWednesday(int gyear);
	static int GetPassionSunday(int gyear);
	static int GetPalmSunday(int gyear);
	static int GetHolyThursday(int gyear);
	static int GetGoodFriday(int gyear);
	static int GetRogationSunday(int gyear);
	static int GetAscensionDay(int gyear);
	static int GetPentecost(int gyear);
	static int GetWhitmundy(int gyear);
	static int GetTrinitySunday(int gyer);
	static int GetCorpusChristi(int gyear);
	static int GetCorpusChristiUSA(int gyear);

    static int GetDaysInMonth(int gyear, int month);

	static const int Epoch = libcalendar::GregorianDate::EPOCH;

    static initonly array<String^>^ MonthNames = gcnew array<String^>
    {
        gcnew String(libcalendar::GregorianDate::MONTHS[0].c_str()),
        gcnew String(libcalendar::GregorianDate::MONTHS[1].c_str()),
        gcnew String(libcalendar::GregorianDate::MONTHS[2].c_str()),
        gcnew String(libcalendar::GregorianDate::MONTHS[3].c_str()),
        gcnew String(libcalendar::GregorianDate::MONTHS[4].c_str()),
        gcnew String(libcalendar::GregorianDate::MONTHS[5].c_str()),
        gcnew String(libcalendar::GregorianDate::MONTHS[6].c_str()),
        gcnew String(libcalendar::GregorianDate::MONTHS[7].c_str()),
        gcnew String(libcalendar::GregorianDate::MONTHS[8].c_str()),
        gcnew String(libcalendar::GregorianDate::MONTHS[9].c_str()),
        gcnew String(libcalendar::GregorianDate::MONTHS[10].c_str()),
        gcnew String(libcalendar::GregorianDate::MONTHS[11].c_str())
    };

    static initonly array<String^>^ WeekdayNames = gcnew array<String^>
    {
        gcnew String(libcalendar::GregorianDate::DAYS_OF_WEEK[0].c_str()),
        gcnew String(libcalendar::GregorianDate::DAYS_OF_WEEK[1].c_str()),
        gcnew String(libcalendar::GregorianDate::DAYS_OF_WEEK[2].c_str()),
        gcnew String(libcalendar::GregorianDate::DAYS_OF_WEEK[3].c_str()),
        gcnew String(libcalendar::GregorianDate::DAYS_OF_WEEK[4].c_str()),
        gcnew String(libcalendar::GregorianDate::DAYS_OF_WEEK[5].c_str()),
        gcnew String(libcalendar::GregorianDate::DAYS_OF_WEEK[6].c_str()),
    };

    static initonly array<EventType^>^ USEventTypes = gcnew array<EventType^>
    {
        gcnew EventType("US", "Memorial Day", gcnew SingleEventDelegate( GregorianDate::GetUSMemorialDay ), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) ),
        gcnew EventType("US", "Labor Day", gcnew SingleEventDelegate( GregorianDate::GetUSLaborDay ), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) ),
        gcnew EventType("US", "Election Day", gcnew SingleEventDelegate( GregorianDate::GetUSElectionDay ), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) ),
        gcnew EventType("US", "Daylight Saving Time Begins", gcnew SingleEventDelegate( GregorianDate::GetUSDSTBegin ), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) ),
        gcnew EventType("US", "Daylight Saving Time Ends", gcnew SingleEventDelegate( GregorianDate::GetUSDSTEnd ), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) )
    };

    static initonly array<EventType^>^ ChristianEventTypes = gcnew array<EventType^>
    {
        gcnew EventType("Christian", "Easter", gcnew SingleEventDelegate( GregorianDate::GetEaster), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) ),
        gcnew EventType("Christian", "Septuagesima Sunday", gcnew SingleEventDelegate(GetSeptuagesimaSunday), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) ),
        gcnew EventType("Christian", "Sexagesima Sunday", gcnew SingleEventDelegate( GregorianDate::GetSexagesimaSunday ), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) ),
        gcnew EventType("Christian", "Shrove Sunday", gcnew SingleEventDelegate( GregorianDate::GetShroveSunday ), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) ),
        gcnew EventType("Christian", "Shrove Monday", gcnew SingleEventDelegate( GregorianDate::GetShroveMonday ), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) ),
        gcnew EventType("Christian", "Ash Wednesday", gcnew SingleEventDelegate( GregorianDate::GetAshWednesday ), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) ),
        gcnew EventType("Christian", "Passion Sunday", gcnew SingleEventDelegate( GregorianDate::GetPassionSunday ), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) ),
        gcnew EventType("Christian", "Holy Thursday", gcnew SingleEventDelegate( GregorianDate::GetHolyThursday ), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) ),
        gcnew EventType("Christian", "Good Friday", gcnew SingleEventDelegate( GregorianDate::GetGoodFriday ), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) ),
        gcnew EventType("Christian", "Rogation Sunday", gcnew SingleEventDelegate( GregorianDate::GetRogationSunday ), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) ),
        gcnew EventType("Christian", "Ascension Day", gcnew SingleEventDelegate( GregorianDate::GetAscensionDay ), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) ),
        gcnew EventType("Christian", "Pentcost", gcnew SingleEventDelegate( GregorianDate::GetPentecost ), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) ),
        gcnew EventType("Christian", "Whitmundy", gcnew SingleEventDelegate( GregorianDate::GetWhitmundy ), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) ),
        gcnew EventType("Christian", "Trinity Sunday", gcnew SingleEventDelegate( GregorianDate::GetTrinitySunday ), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) ),
        gcnew EventType("Christian", "Corpus Christi (non-USA)", gcnew SingleEventDelegate( GregorianDate::GetCorpusChristi ), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) ),
        gcnew EventType("Christian", "Corpus Christi (USA)", gcnew SingleEventDelegate( GregorianDate::GetCorpusChristiUSA ), gcnew DateCreateDelegate(GregorianDate::CreateGregorianDate) )
    };


    static List<Event^>^ GetChristianEvents(int gyear);
    static List<Event^>^ GetUSEvents(int gyear);

    static FixedDate^ CreateGregorianDate(int fd);

private:
    libcalendar::GregorianDate* _gd;
    
};

}

#endif



#pragma once

#include <vector>

#ifdef _MANAGED



namespace libcalendarman
{

public enum class Weekday { Sunday = 0, Monday, Tuesday, Wednesday, 
		                    Thursday, Friday, Saturday };

public enum class Months { January = 1, February, March, April, May, June, July, August, September, October, November, December };


public ref class FixedDate abstract
{
public:
    virtual int ToFixed() abstract;
    virtual void FromFixed(int fd) abstract;

    static operator int(FixedDate^ fd);
};



public ref class DateUtils abstract
{
public:
    static Weekday GetDayOfWeek(int fd);

    static int WeekdayOnOrBefore(int fd, Weekday weekday);
    static int WeekdayOnOrAfter(int fd, Weekday weekday);
    static int WeekdayNearest(int fd, Weekday weekday);
    static int WeekdayBefore(int fd, Weekday weekday);
    static int WeekdayAfter(int fd, Weekday weekday);

    static int NthWeekday(int fd, int n, Weekday weekday);
    static int FirstWeekday(int fd, Weekday weekday);
    static int LastWeekday(int fd, Weekday weekday);

    static int Floor(int a, int b);
    static long long Floor(long long a, long long b);
	
    static double Mod(double x, double y);
    static int Mod(int x, int y);

    static int Amod(int a, int b);
	
    static int Quotient(double x, double y);
};


ref class Convert abstract
{
public:
    static array<int>^ ConvertVectorToArray(const std::vector<int>& v);
};

}

#endif
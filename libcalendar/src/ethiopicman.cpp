

#include "ethiopicman.h"

using namespace libcalendarman;


EthiopicDate::EthiopicDate()
{
    _ed = new libcalendar::EthiopicDate();
}


EthiopicDate::EthiopicDate(int fd)
{
    _ed = new libcalendar::EthiopicDate(fd);
}


EthiopicDate::EthiopicDate(int year, int month, int day)
{
    _ed = new libcalendar::EthiopicDate(year, month, day);
}


EthiopicDate::!EthiopicDate()
{
    if (_ed)
    {
        delete _ed;
        _ed = 0;
    }
}


EthiopicDate::~EthiopicDate()
{
    this->!EthiopicDate();
}

void EthiopicDate::FromFixed(int fd) 
{
    _ed->FromFixed(fd);
}


int EthiopicDate::ToFixed() 
{
    return _ed->ToFixed();
}


String^ EthiopicDate::ToString() 
{
    return gcnew String( _ed->ToString().c_str() );
}



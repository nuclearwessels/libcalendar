

#pragma once

#include "fixeddate.h"

#include <vector>

namespace libcalendar
{

class OldHinduBase
{
public:
    static const int EPOCH;
    static const double ARYA_SOLAR_YEAR;
    static const double ARYA_SOLAR_MONTH;
    static const double ARYA_JOVIAN_PERIOD;

    static const std::vector<std::string> JOVIAN_CYCLES;

    static int GetHinduDayCount(int fd);
    static int GetJovianYear(int fd);
};


class OldHinduSolarDate : public OldHinduBase, public FixedDate
{
public:
    OldHinduSolarDate();
	OldHinduSolarDate(int year, int month, int day);
	OldHinduSolarDate(int fd);

	void FromFixed(int fd);
	int ToFixed() const;
	std::string ToString() const;
    
    void SetYear(int year);
    int Year() const;

    void SetMonth(int month);
    int Month() const;

    void SetDay(int day);
    int Day() const;    
    
    static const std::vector<std::string> MONTHS;

private:
    int year_, month_, day_;
};


class OldHinduLunarDate : public OldHinduBase, public FixedDate
{
public:
    OldHinduLunarDate();
    OldHinduLunarDate(int fd);
    OldHinduLunarDate(int year, int month, bool leap, int day);

    void SetYear(int year);
    int Year() const;

    void SetMonth(int month);
    int Month() const;

    void SetDay(int day);
    int Day() const;

    void SetLeap(bool leap);
    bool Leap() const;

    int ToFixed() const;
    void FromFixed(int fd);
    std::string ToString() const;

    static bool IsLeapYear(int lunarYear);

    static const double ARYA_LUNAR_MONTH;
    static const double ARYA_LUNAR_DAY;

    static const std::vector<std::string> MONTHS;

private:
    int year_, month_, day_;
    bool leap_;
};


}


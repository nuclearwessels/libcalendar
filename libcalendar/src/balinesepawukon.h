
#pragma once


#include "fixeddate.h"
#include "julian.h"


namespace libcalendar
{

class BalinesePawukonDate : public FixedDate
{
public:
    static const int EPOCH;

    BalinesePawukonDate();
    BalinesePawukonDate(int fd);
    BalinesePawukonDate(bool luang, int dwiwara, int triwara, int caturwara, int pancawara,
                        int sadwara, int saptawara, int asatawara, int sangawara, int dasawara);

    bool BaliLuang() const;
    int BaliDwiwara() const;
    int BaliTriwara() const;
    int BaliCaturwara() const;
    int BaliPancawara() const;
    int BaliSadwara() const;
    int BaliSaptawara() const;
    int BaliAsatawara() const;
    int BaliSangawara() const;
    int BaliDasawara() const;

    int ToFixed() const;

    void FromFixed(int fd);

    std::string ToString() const;

    static int GetBaliDayFromFixed(int fd);    
    static int GetBaliTriwaraFromFixed(int fd);
    static int GetBaliSadwaraFromFixed(int fd);
    static int GetBaliSaptawaraFromFixed(int fd);
    static int GetBaliPancawaraFromFixed(int fd);
    static int GetBaliWeekFromFixed(int fd);
    static int GetBaliDasawaraFromFixed(int fd);
    static int GetBaliDwiwaraFromFixed(int fd);
    static bool GetBaliLuangFromFixed(int fd);
    static int GetBaliSangawaraFromFixed(int fd);
    static int GetBaliAsatawaraFromFixed(int fd);
    static int GetBaliCaturawaFromFixed(int fd);
    static int GetBaliOnOrBefore(const BalinesePawukonDate& bpd, int fd);
    static void GetKajengKeliwonInGregorian(std::vector<int>& results, int gregorianYear);
    static void GetTumpekInGregorian(std::vector<int>& results, int gregorianYear);

protected:
    static const int FIVE_DAY_URIPS[];
    static const int SEVEN_DAY_URIPS[];

    static void GetPositionsInInterval(std::vector<int>& results, int n, int c, int delta, int start, int end);

private:
    bool _baliLuang;
    int _baliDwiwara;
    int _baliTriwara;
    int _baliCaturwara;
    int _baliPancawara;
    int _baliSadwara;
    int _baliSaptawara;
    int _baliAsatawara;
    int _baliSangawara;
    int _baliDasawara;
};



}   // namespace libcalendar


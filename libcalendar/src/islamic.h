

#ifndef ISLAMIC_H
#define ISLAMIC

#include "fixeddate.h"

#include <string>
#include <vector>

namespace libcalendar
{

class IslamicDate : public FixedDate
{
public:
	IslamicDate();
	IslamicDate(int fd);
	IslamicDate(int year, int month, int day);

	void SetYear(int year);
	int Year() const;

	void SetMonth(int month);
	int Month() const;

	void SetDay(int day);
	int Day() const;

	void FromFixed(int fixed);
	int ToFixed() const;

	std::string ToString() const;

	static bool IsLeapYear(int year);
	
	static std::vector<int> GetIslamicInGregorian(int gregorianYear, int islamicMonth, int islamicDay);

	static const int EPOCH;
	static const std::vector<std::string> MONTHS;
	static const std::vector<std::string> DAYS_OF_WEEK;
	
private:
	int year_, month_, day_;

    void ValidateDate() const;
};

}


#endif



#include "gregorianman.h"

using namespace libcalendarman;


GregorianDate::GregorianDate()
{
    _gd = new libcalendar::GregorianDate();
}

GregorianDate::GregorianDate(int fd)
{
    _gd = new libcalendar::GregorianDate(fd);
}


GregorianDate::GregorianDate(int year, int month, int day)
{
    _gd = new libcalendar::GregorianDate(year, month, day);
}


GregorianDate::!GregorianDate()
{
    if (_gd)
    {
        delete _gd;
        _gd = 0;
    }
}


GregorianDate::~GregorianDate()
{
    this->!GregorianDate();
}


int GregorianDate::ToFixed()
{
    return _gd->ToFixed();
}

void GregorianDate::FromFixed(int fixed)
{
    _gd->FromFixed(fixed);
}


String^ GregorianDate::ToString()
{
    return gcnew String( _gd->ToString().c_str() );
}

bool GregorianDate::IsLeapYear(int gregorianYear)
{
    return libcalendar::GregorianDate::IsLeapYear(gregorianYear);
}

int GregorianDate::GetYearFromFixed(int fixedDate)
{
    return libcalendar::GregorianDate::GetYearFromFixed(fixedDate);
}

int GregorianDate::GetUSMemorialDay(int year)
{
    return libcalendar::GregorianDate::GetUSMemorialDay(year);
}

int GregorianDate::GetUSLaborDay(int year)
{
    return libcalendar::GregorianDate::GetUSLaborDay(year);
}

int GregorianDate::GetUSElectionDay(int year)
{
    return libcalendar::GregorianDate::GetUSElectionDay(year);
}

int GregorianDate::GetUSThanksgiving(int year)
{
    return libcalendar::GregorianDate::GetUSThanksgiving(year);
}

int GregorianDate::GetUSDSTBegin(int year)
{
    return libcalendar::GregorianDate::GetUSDSTBegin(year);
}

int GregorianDate::GetUSDSTEnd(int year)
{
    return libcalendar::GregorianDate::GetUSDSTEnd(year);
}

int GregorianDate::GetEaster(int gyear)
{
    return libcalendar::GregorianDate::GetEaster(gyear);
}

int GregorianDate::GetSeptuagesimaSunday(int gyear)
{
    return libcalendar::GregorianDate::GetSeptuagesimaSunday(gyear);
}

int GregorianDate::GetSexagesimaSunday(int gyear)
{
    return libcalendar::GregorianDate::GetSexagesimaSunday(gyear);
}

int GregorianDate::GetShroveSunday(int gyear)
{
    return libcalendar::GregorianDate::GetShroveSunday(gyear);
}

int GregorianDate::GetShroveMonday(int gyear)
{
    return libcalendar::GregorianDate::GetShroveMonday(gyear);
}

int GregorianDate::GetMardiGras(int gyear)
{
    return libcalendar::GregorianDate::GetMardiGras(gyear);
}

int GregorianDate::GetAshWednesday(int gyear)
{
    return libcalendar::GregorianDate::GetAshWednesday(gyear);
}

int GregorianDate::GetPassionSunday(int gyear)
{
    return libcalendar::GregorianDate::GetPassionSunday(gyear);
}

int GregorianDate::GetPalmSunday(int gyear)
{
    return libcalendar::GregorianDate::GetPalmSunday(gyear);
}

int GregorianDate::GetHolyThursday(int gyear)
{
    return libcalendar::GregorianDate::GetHolyThursday(gyear);
}

int GregorianDate::GetGoodFriday(int gyear)
{
    return libcalendar::GregorianDate::GetGoodFriday(gyear);
}

int GregorianDate::GetRogationSunday(int gyear)
{
    return libcalendar::GregorianDate::GetRogationSunday(gyear);
}

int GregorianDate::GetAscensionDay(int gyear)
{
    return libcalendar::GregorianDate::GetAscensionDay(gyear);
}

int GregorianDate::GetPentecost(int gyear)
{
    return libcalendar::GregorianDate::GetPentecost(gyear);
}

int GregorianDate::GetWhitmundy(int gyear)
{
    return libcalendar::GregorianDate::GetWhitmundy(gyear);
}

int GregorianDate::GetTrinitySunday(int gyear)
{
    return libcalendar::GregorianDate::GetTrinitySunday(gyear);
}

int GregorianDate::GetCorpusChristi(int gyear)
{
    return libcalendar::GregorianDate::GetCorpusChristi(gyear);
}

int GregorianDate::GetCorpusChristiUSA(int gyear)
{
    return libcalendar::GregorianDate::GetCorpusChristiUSA(gyear);
}


List<Event^>^ GregorianDate::GetChristianEvents(int gyear)
{
    List<Event^>^ l = gcnew List<Event^>();

    for each (EventType^ et in ChristianEventTypes)
    {
        et->AddEvent(l, gyear);
    }

    return l;
}


List<Event^>^ GregorianDate::GetUSEvents(int gyear)
{
    List<Event^>^ l = gcnew List<Event^>();

    for each (EventType^ et in USEventTypes)
    {
        et->AddEvent(l, gyear);
    }

    return l;
}


int GregorianDate::GetDaysInMonth(int gyear, int month)
{
    return libcalendar::GregorianDate::GetDaysInMonth(gyear, month);
}


FixedDate^ GregorianDate::CreateGregorianDate(int fd)
{
    return gcnew GregorianDate(fd);
}

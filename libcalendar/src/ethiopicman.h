

#pragma once


using namespace System;

#include "fixeddateman.h"
#include "ethiopic.h"

namespace libcalendarman
{
    
public ref class EthiopicDate : public FixedDate
{
public:
    EthiopicDate();
    EthiopicDate(int fd);
    EthiopicDate(int year, int month, int day);
    !EthiopicDate();
    ~EthiopicDate();

    virtual void FromFixed(int fd) override;
	virtual int ToFixed() override;

    property int Year
    {
        void set(int year) { _ed->SetYear(year); }
        int get() { return _ed->Year(); }
    }

    property int Month
    {
        void set(int month) { _ed->SetMonth(month); }
        int get() { return _ed->Month(); }
    }

    property int Day
    {
        void set(int day) { _ed->SetDay(day);  }
        int get() { return _ed->Day(); }
    }

	virtual String^ ToString() override;

    static const int Epoch = 2796;

    static initonly array<String^>^ Months;
    static initonly array<String^>^ DaysOfWeek;

    static EthiopicDate()
    {
        Months = gcnew array<String^>(13);
        for (int i=0; i<13; i++)
        {
            Months[i] = gcnew String( libcalendar::EthiopicDate::MONTHS[i].c_str() );
        }

        DaysOfWeek = gcnew array<System::String^>(7);
        for (int i=0; i<7; i++)
        {
            DaysOfWeek[i] = gcnew System::String( libcalendar::EthiopicDate::DAYS_OF_WEEK[i].c_str());
        }
    }

private:
    libcalendar::EthiopicDate* _ed;
};

}



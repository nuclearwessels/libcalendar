

#pragma once

#include "fixeddateman.h"
#include "mayan.h"

using namespace System;


namespace libcalendarman
{


public ref class MayanLongCountDate : FixedDate
{
public:
    MayanLongCountDate();
    MayanLongCountDate(int fd);
    MayanLongCountDate(int baktun, int katun, int tun, int uinal, int kin);
    !MayanLongCountDate();
    ~MayanLongCountDate();

    property int Baktun
    {
        int get() { return _mlcd->Baktun(); }
        void set(int baktun) { _mlcd->SetBaktun(baktun); }
    }

    property int Katun
    {
        int get() { return _mlcd->Katun(); }
        void set(int katun) { _mlcd->SetKatun(katun); }
    }

    property int Tun
    {
        int get() { return _mlcd->Tun(); }
        void set(int tun) { _mlcd->SetTun(tun); }
    }

    property int Uinal
    {
        int get() { return _mlcd->Uinal(); }
        void set(int uinal) { _mlcd->SetUinal(uinal); }
    }

    property int Kin
    {
        int get() { return _mlcd->Kin(); }
        void set(int kin) { _mlcd->SetKin(kin); }
    }


    virtual int ToFixed() override;
    virtual void FromFixed(int fd) override;

    virtual String^ ToString() override;

    static const int MayanEpoch = libcalendar::MayanLongCountDate::MAYAN_EPOCH;

private:
    libcalendar::MayanLongCountDate* _mlcd;
};


public ref class MayanHaabDate : FixedDate
{
public:
    MayanHaabDate();
    MayanHaabDate(int fd);
    MayanHaabDate(int month, int day);
    !MayanHaabDate();
    ~MayanHaabDate();

    property int Month
    {
        int get() { return _mhd->Month(); }
        void set(int month) { _mhd->SetMonth(month); }
    }

    property int Day
    {
        int get() { return _mhd->Day(); }
        void set(int day) { _mhd->SetDay(day); }
    }

    virtual int ToFixed() override;
    virtual void FromFixed(int fd) override;

    virtual String^ ToString() override;

    static initonly array<String^>^ Months = gcnew array<String^>
    {
        "Pop", "Uo", "Zip", "Zotz", "Tzec", "Xul", "Yaxkin",
        "Mol", "Chen", "Yax", "Zac", "Ceh", "Mac", "Kankin",
        "Muan", "Pax", "Kayab", "Cumku", "Uayeb"    
    };

    static const int MayanHaabEpoch = libcalendar::MayanHaabDate::MAYAN_HAAB_EPOCH;

    static int GetMayanHaabOrdinal(int month, int day);
    static int GetMayanHaabOnOrBefore(MayanHaabDate^ mhd, int fd);

private:
    libcalendar::MayanHaabDate* _mhd;
};



public ref class MayanTzolkinDate : FixedDate
{
public:
    MayanTzolkinDate();
    MayanTzolkinDate(int fd);
    MayanTzolkinDate(int number, int name);
    !MayanTzolkinDate();
    ~MayanTzolkinDate();

    property int Number
    {
        int get() { return _mtd->Number(); }
    }

    property int Name
    {
        int get() { return _mtd->Name(); }
    }


    virtual int ToFixed() override;
    virtual void FromFixed(int fd) override;
    virtual String^ ToString() override;

    static int GetMayanTzolkinOrdinal(int number, int name);
    static int GetMayanTzolkinOnOrBefore(MayanTzolkinDate^ mtd, int fd);

    static initonly array<String^>^ Months = gcnew array<String^>
    {
        "Imix", "Ik", "Akbal", "Kan", "Chicchan", "Cimi", "Manik",
        "Lamat", "Muluc", "Oc", "Chuen", "Eb", "Ben", "Ix",
        "Men", "Cib", "Caban", "Etzneb", "Cauac", "Ahau"
    };

    static const int MayanTzolkinEpoch = libcalendar::MayanTzolkinDate::MAYAN_TZOLKIN_EPOCH;

private:
    libcalendar::MayanTzolkinDate* _mtd;
};



public ref class MayanDateUtils abstract
{
public:
    static int GetMayanCalendarRoundOnOrBefore(MayanHaabDate^ mhd, MayanTzolkinDate^ mtd, int fd);
};


}


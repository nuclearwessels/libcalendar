
#include "romanman.h"

using namespace libcalendarman;


RomanDate::RomanDate()
{
    _rd = new libcalendar::RomanDate();
}

RomanDate::RomanDate(int fixed)
{
    _rd = new libcalendar::RomanDate(fixed);
}

RomanDate::RomanDate(int year, int month, RomanEvents evt, int count, bool leap)
{
    _rd = new libcalendar::RomanDate(year, month, (libcalendar::RomanDate::Events) evt, count, leap);
}

RomanDate::!RomanDate()
{
    if (_rd)
    {
        delete _rd;
        _rd = 0;
    }
}

RomanDate::~RomanDate()
{
    this->!RomanDate();
}

int RomanDate::ToFixed()
{
    return _rd->ToFixed();
}

void RomanDate::FromFixed(int fd) 
{
    _rd->FromFixed(fd);
}

String^ RomanDate::ToString() 
{
    return gcnew String( _rd->ToString().c_str() );
}

int RomanDate::GetIdes(int month)
{
    return libcalendar::RomanDate::GetIdes(month);
}


int RomanDate::GetNones(int month)
{
    return libcalendar::RomanDate::GetNones(month);
}

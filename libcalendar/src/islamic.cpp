
#include "islamic.h"
#include "gregorian.h"
#include "julian.h"
#include "validate.h"

#include <sstream>

using namespace libcalendar;


const std::vector<std::string> IslamicDate::DAYS_OF_WEEK = 
{   
    "yaum al-ahad", "yaum al-ithnayna",
	"yaum ath-thalatha", "yaum al-arba'a'", "yaum al-hamis", 
	"yaum al-jum'a", "yaum as-sabt" 
};

const std::vector<std::string> IslamicDate::MONTHS = 
{ 
    "Muharram", "Safar", "Rabi' I",
	"Rabi' II", "Jumada I", "Jumada II", "Rajab", "Sha'ban",
	"Ramadan", "Shawwal", "Dhu al-Qa'da", "Dhu al-Hijja" 
};


const int IslamicDate::EPOCH = 227015;


IslamicDate::IslamicDate()
{
	FromFixed(EPOCH);
}


IslamicDate::IslamicDate(int fixed)
{
	FromFixed(fixed);
}


IslamicDate::IslamicDate(int year, int month, int day) 
{
    SetYear(year);
    SetMonth(month);
    SetDay(day);
}



void IslamicDate::SetYear(int year)
{
	year_ = year;

    ValidateDate();
}


int IslamicDate::Year() const
{
	return year_;
}


void IslamicDate::SetMonth(int month)
{   
    month_ = month;

    ValidateDate();
}


int IslamicDate::Month() const
{
	return month_;
}


void IslamicDate::SetDay(int day)
{
	day = day_;

    ValidateDate();
}


int IslamicDate::Day() const
{
	return day_;
}


void IslamicDate::ValidateDate() const
{
    ValidateGreaterEqual(day_, 1);
    
    switch (month_)
    {
    case 1:
    case 3:
    case 5:
    case 7:
    case 9:
    case 11:
    	ValidateLessEqual(day_, 30);
    	break;
    	
    case 2:
    case 4:
    case 6:
    case 8:
    case 10:
    	ValidateLessEqual(day_, 29);
    	break;

    case 12:
    	ValidateLessEqual(day_, IsLeapYear(year_) ? 30 : 29);
    	break;
    };

}



void IslamicDate::FromFixed(int fixed)
{
	year_ = Floor( 30 * (fixed - EPOCH) + 10646, 10631);
	int priorDays = fixed - IslamicDate(year_, 1, 1).ToFixed();
	month_ = Floor( 11 * priorDays + 330, 325);
	day_ = fixed - IslamicDate(year_, month_, 1).ToFixed() + 1;
}


int IslamicDate::ToFixed() const
{
	return day_ + 29 * (month_-1) + Floor( 6 * month_ - 1, 11)
		+ (year_-1) * 354 + Floor(3 + 11 * year_, 30) + EPOCH - 1;
}


std::string IslamicDate::ToString() const
{	
    std::string s = MONTHS[month_ - 1] + "(" + std::to_string(month_) + ") " +
        std::to_string(day_) + ", " +
        std::to_string(year_);
    
    return s;
}


bool IslamicDate::IsLeapYear(int year)
{
	return ( Mod(14 + 11 * year, 30) < 11 ) ? true : false;
}


std::vector<int> IslamicDate::GetIslamicInGregorian(int gregorianYear, int islamicMonth, int islamicDay)
{
    std::vector<int> results;

	const int jan1 = GregorianDate(gregorianYear, 1, 1).ToFixed();
	const int dec31 = GregorianDate(gregorianYear, 12, 31).ToFixed();
	
	const int y = IslamicDate(jan1).Year();
	
	for (int i=y; i<=y+2; i++)
	{
		int date = IslamicDate(i, islamicMonth, islamicDay).ToFixed();
		
		if (date>=jan1 && date<=dec31)
		{
			results.push_back(date);
		}
	}

    return results;
}



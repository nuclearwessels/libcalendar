

#include "roman.h"
#include "julian.h"

using namespace libcalendar;
using namespace std;


const string RomanDate::MONTHS[12] = { "January", "February",
	"March", "April", "May", "June", "July", "August", "September", 
	"October", "November", "December" };

const string RomanDate::EVENTS[3] = { "Kalends", "Nones", "Ides" };

const string RomanDate::NUMERALS[19] = { "i", "ii", "iii", "iv", "v", 
	"vi", "vii", "viii", "ix", "x", "xi", "xii", "xiii", "xiv", "xv",
	"xvi", "xvii", "xviii", "xix" };


RomanDate::RomanDate()
{
	FromFixed(JulianDate::EPOCH);
}


RomanDate::RomanDate(int fixed)
{
	FromFixed(fixed);
}


RomanDate::RomanDate(int year, int month, Events evt, int count, bool leap)
{
	_year = year;
	_month = month;
	_event = evt;
	_count = count;
	_leap = leap;
}


void RomanDate::SetYear(int year)
{
	assert(year != 0);
	_year = year;
}


int RomanDate::Year() const
{
	return _year;
}


void RomanDate::SetMonth(int month)
{
	assert(month>0 && month <=12);
	_month = month;
}


int RomanDate::Month() const
{
	return _month;
}


void RomanDate::SetEvent(Events evt)
{
	_event = evt;
}

RomanDate::Events RomanDate::Event() const
{
	return _event;
}


void RomanDate::SetCount(int count)
{
	_count = count;
}


int RomanDate::Count() const
{
	return _count;
}


void RomanDate::SetLeap(bool leap)
{
	_leap = leap;
}


bool RomanDate::IsLeap() const
{
	return _leap;
}


int RomanDate::ToFixed() const
{
	int returnme;

	if (_event == KALENDS)
	{
		returnme = JulianDate(_year, _month, 1).ToFixed();
	}
	else if (_event == NONES)
	{
		returnme = JulianDate(_year, _month, GetNones(_month)).ToFixed();
	}
	else
	{
		returnme = JulianDate(_year, _month, GetIdes(_month)).ToFixed();
	}
	
	returnme -= _count;
	if (JulianDate::IsLeapYear(_year) && _month == 3 && _event==KALENDS 
		&& _count>=6 && _count<=16)
	{
		returnme += 0;
	}
	else
	{
		returnme += 1;
	}

	returnme += _leap ? 1 : 0;

	return returnme;
}


void RomanDate::FromFixed(int fixed) 
{
	JulianDate jc(fixed);

	if (jc.Day() == 1)
	{
		_year = jc.Year();
		_month = jc.Month();
		_event = KALENDS;
		_count = 1;
		_leap = false;
	}
	else if (jc.Day() <= GetNones(jc.Month()))
	{
		_year = jc.Year();
		_month = jc.Month();
		_event = NONES;
		_count = GetNones(_month) - jc.Day() + 1;
		_leap = false;
	}
	else if (jc.Day() <= GetIdes(jc.Month()))
	{
		_year = jc.Year();
		_month = jc.Month();
		_event = IDES;
		_count = GetIdes(_month) - jc.Day() + 1;
		_leap = false;
	}
	else if (jc.Month() != 2 || !JulianDate::IsLeapYear(jc.Year()))
	{
		int monthPrime = Amod(jc.Month()+1,12);
		int yearPrime;
		if (monthPrime != 1)
		{
			yearPrime = jc.Year();		
		}
		else if (monthPrime == 1 && jc.Year() != -1)
		{
			yearPrime = jc.Year() + 1;
		}
		else
		{
			yearPrime = 1;
		}
		int kalends1 = RomanDate(yearPrime, monthPrime, KALENDS, 1, false).ToFixed();
		
		_year = yearPrime;
		_month = monthPrime;
		_event = KALENDS;
		_count = kalends1 - fixed + 1;
		_leap = false;
	}
	else if (jc.Day() < 25)
	{
		_year = jc.Year();
		_month = 3;
		_event = KALENDS;
		_count = 30 - jc.Day();
		_leap = false;
	}
	else
	{
		_year = jc.Year();
		_month = 3;
		_event = KALENDS;
		_count = 31 - jc.Day();
		_leap = (jc.Day() == 25) ? true : false;
	}
}


string RomanDate::ToString() const
{
	char temp[100];
	char year[30];

	JulianDate jc(ToFixed());

	if (jc.Month() == 12 && _event == KALENDS)
	{
		int nextYear = (_year == -1) ? 1 : (_year + 1);
		snprintf(year, 30, "%d %s", abs(nextYear),
			((nextYear<0) ? "B.C.E." : "C.E.") );
	}
	else
	{
		snprintf(year, 30, "%d %s", abs(Year()),
			(_year<0) ? "B.C.E." : "C.E.");
	}
	
	if (jc.Month() == 2 && _event == KALENDS && _leap )
	{
		snprintf(temp, 100, "ante diem bis %s %s %s, %s",
			NUMERALS[ _count - 1].c_str(),
			EVENTS[ (int) _event - 1].c_str(),
			MONTHS[ _month -1].c_str(),
			year );
	}
	else
	{
		if (_count == 1)
		{
			snprintf(temp, 100, "%s %s, %s",
				EVENTS[ (int) _event - 1].c_str(),
				MONTHS[ _month-1].c_str(),
				year );
		}
		else if (_count == 2)
		{
			snprintf(temp, 100, "pridie %s %s, %s",
				EVENTS[ (int) _event - 1].c_str(),
				MONTHS[ _month-1].c_str(),
				year );		
		}
		else
		{
			snprintf(temp, 100, "ante diem %s %s %s, %s",
				NUMERALS[ _count - 1].c_str(),
				EVENTS[ (int) _event - 1].c_str(),
				MONTHS[ _month-1].c_str(),
				year );
		}	
	}

	string returnme(temp);

// 	snprintf(temp, 100, "(%d %d %d %d %c)", getYear(), getMonth(),
// 		getEvent(), getCount(), isLeap() ? '1' : '0' );
// 	returnme += " ";
// 	returnme += temp;

	return returnme;
}


int RomanDate::GetIdes(int month)
{
	assert( month>=1 && month<=12);
	if (month == 3 || month==5 || month==7 || month==10)
	{
		return 15;
	}
	else
	{
		return 13;
	}
}

int RomanDate::GetNones(int month)
{
	return GetIdes(month) - 8;
}





#include "iso.h"
#include "gregorian.h"

#include "validate.h"

using namespace libcalendar;


const int ISODate::EPOCH = GregorianDate::EPOCH;




ISODate::ISODate()
{
	FromFixed(EPOCH);
}


ISODate::ISODate(int fixed)
{
	FromFixed(fixed);
}


ISODate::ISODate(int year, int week, int day) : year_(year),
                                                week_(week),
                                                day_(day)
{
    ValidateDate();
}



void ISODate::FromFixed(int fixed)
{
	int approx = GregorianDate::GetYearFromFixed(fixed - 3);
	
	year_ = ( fixed >= ISODate(approx+1, 1, 1).ToFixed() ) ? (approx+1) : approx;
	week_ = Floor( fixed - ISODate(year_, 1, 1).ToFixed(), 7) + 1;
	day_ = Amod(fixed, 7);

    ValidateDate();
}


int ISODate::ToFixed() const
{
	return NthWeekday( GregorianDate(year_-1, 12, 28), week_, SUNDAY) + day_;
}


void ISODate::SetYear(int year)
{
	year_ = year;

    ValidateDate();
}


int ISODate::Year() const
{
	return year_;
}


void ISODate::SetWeek(int week)
{
	week_ = week;
    ValidateDate();
}


int ISODate::Week() const
{
	return week_;
}


void ISODate::SetDay(int day)
{
	day_ = day;

    ValidateDate();
}


int ISODate::Day() const
{
	return day_;
}


void ISODate::ValidateDate() const
{
    ValidateBetween(day_, 0, 7);
    ValidateBetween(week_, 1, 53);
}

std::string ISODate::ToString() const
{
    std::string s = "day " + std::to_string(day_) +
        ", week " + std::to_string(week_) +
        ", year " + std::to_string(year_);
    
    return s;
}



#include "gregorian.h"
#include "validate.h"

using namespace libcalendar;


const std::vector<std::string> GregorianDate::MONTHS = 
{ 
    "January", "February",
	"March", "April", "May", "June", "July", "August", "September", 
	"October", "November", "December" 
};

const std::vector<std::string> GregorianDate::DAYS_OF_WEEK = 
{ 
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
};       



std::vector<Event> GregorianDate::GetUSEvents(int gregorianYear)
{
    // TODO Add Thanksgiving, Veteran's Day, Flag Day
    std::vector<Event> events;

    events.emplace_back(Event("US", "Memorial Day", GetUSMemorialDay(gregorianYear)));
    events.emplace_back(Event("US", "Labor Day", GetUSLaborDay(gregorianYear)));
    events.emplace_back(Event("US", "Election Day", GetUSElectionDay(gregorianYear)));
    events.emplace_back(Event("US", "Daylight Saving Time Begins", GetUSDSTBegin(gregorianYear)));
    events.emplace_back(Event("US", "Daylight Saving Time Ends", GetUSDSTEnd(gregorianYear)));

    return events;
}

std::vector<Event> GregorianDate::GetChristianEvents(int gregorianYear)
{
    std::vector<Event> events;

    

    //Event("Christian", "Easter", GregorianDate::GetEaster),
    //Event("Christian", "Septuagesima Sunday", GregorianDate::GetSeptuagesimaSunday),
    //Event("Christian", "Sexagesima Sunday", GregorianDate::GetSexagesimaSunday),
    //Event("Christian", "Shrove Sunday", GregorianDate::GetShroveSunday),
    //Event("Christian", "Shrove Monday", GregorianDate::GetShroveMonday),
    //Event("Christian", "Ash Wednesday", GregorianDate::GetAshWednesday),
    //Event("Christian", "Passion Sunday", GregorianDate::GetPassionSunday),
    //Event("Christian", "Holy Thursday", GregorianDate::GetHolyThursday),
    //Event("Christian", "Good Friday", GregorianDate::GetGoodFriday),
    //Event("Christian", "Rogation Sunday", GregorianDate::GetRogationSunday),
    //Event("Christian", "Ascension Day", GregorianDate::GetAscensionDay),
    //Event("Christian", "Pentcost", GregorianDate::GetPentecost),
    //Event("Christian", "Whitmundy", GregorianDate::GetTrinitySunday),
    //Event("Christian", "Trinity Sunday", GregorianDate::GetTrinitySunday),
    //Event("Christian", "Corpus Christi (non-USA)", GregorianDate::GetCorpusChristi),
    //Event("Christian", "Corpus Christi (USA)", GregorianDate::GetCorpusChristiUSA)

    return events;
}




GregorianDate::GregorianDate()
{
	FromFixed(EPOCH);
}


GregorianDate::GregorianDate(int fd)
{
	FromFixed(fd);
}


GregorianDate::GregorianDate(int year, int month, int day)
{
	year_ = year;
	month_ = month;
	day_ = day;
}


void GregorianDate::SetYear(int year)
{
	year_ = year;
}


int GregorianDate::Year() const
{
	return year_;
}


void GregorianDate::SetMonth(int month)
{
	month_ = month;
}


int GregorianDate::Month() const
{
	return month_;
}


void GregorianDate::SetDay(int day)
{
	day_ = day;
}


int GregorianDate::Day() const
{
	return day_;
}



int GregorianDate::NextMonth() const
{
    return ToFixed() + GetDaysInMonth(year_, month_);
}


int GregorianDate::PreviousMonth() const
{
    int m = month_ - 1;
    int y = year_;
    if (m < 1)
    {
        m = 12;
        y--;
    }
    return ToFixed() - GetDaysInMonth(y, m);
}


int GregorianDate::ToFixed() const
{
	int returnme = EPOCH - 1;
	returnme += 365 * (year_ - 1);
	returnme += Floor(year_-1, 4);
	returnme -= Floor(year_-1, 100);
	returnme += Floor(year_-1, 400);
	returnme += Floor(367 * month_ - 362, 12);
	
	if (month_ <= 2)
	{
		returnme += 0;
	}
	else if (IsLeapYear(year_))
	{
		returnme += -1;
	}
	else
	{
		returnme += -2;
	}
	
	returnme += day_;
	
	return returnme;
}


void GregorianDate::FromFixed(int fixed)
{
	year_ = GetYearFromFixed(fixed);
	int priorDays = fixed - GregorianDate(year_, 1, 1).ToFixed();
	
	int correction;
	if (fixed < GregorianDate(year_, 3, 1).ToFixed())
	{
		correction = 0;
	}
	else if (fixed >= GregorianDate(year_, 3, 1).ToFixed() && IsLeapYear(year_) )
	{
		correction = 1;
	}
	else
	{
		correction = 2;
	}

	month_ = Floor(12 * (priorDays + correction) + 373, 367);

	day_ = fixed - GregorianDate(year_, month_, 1).ToFixed() + 1;
}


std::string GregorianDate::ToString() const
{
    char temp[80];
    snprintf(temp, 80, "%s %d, %d", MONTHS[month_ - 1].c_str(), day_, year_);
    return std::string(temp);
}


bool GregorianDate::IsLeapYear(int gy)
{
	bool returnme = false;
	
	if (Mod(gy, 4) == 0)
	{
		switch (Mod(gy, 400))
		{
		case 100:
		case 200:
		case 300:
			returnme = false;
			break;
			
			
		default:
			returnme = true;
		}
	}

	return returnme;
}


int GregorianDate::GetYearFromFixed(int fixed)
{
	int d0 = fixed - EPOCH;
	int n400 = Floor(d0, 146097);
	int d1 = Mod(d0, 146097);
	int n100 = Floor(d1, 36524);
	int d2 = Mod(d1, 36524);
	int n4 = Floor(d2, 1461);
	int d3 = Mod(d2, 1461);
	int n1 = Floor(d3, 365);
	int year = 400 * n400 + 100 * n100 + 4 * n4 + n1;

	int returnme;
	if (n100 == 4 || n1 == 4)
	{
		returnme = year;
	}
	else
	{
		returnme = year + 1;
	}

	return returnme;
}


int GregorianDate::OrdinalDayOfYear() const
{
	int d0 = ToFixed() - EPOCH;
	int d1 = Mod(d0, 146097);
	int n100 = Floor(d1, 36524);
	int d2 = Mod(d1, 36524);
	int d3 = Mod(d2, 1461);
	int n1 = Floor(d3, 365);

	int returnme;
	if (n1 != 4 && n100 != 4)
	{
		returnme = Mod(d3, 365) + 1;
	}
	else
	{
		returnme = 366;
	}

	return returnme;
}


int GregorianDate::WeekOfMonth() const
{
    GregorianDate first(Year(), Month(), 1);

    int firstSunday = WeekdayOnOrBefore(first, SUNDAY);

    int thisSunday = WeekdayOnOrBefore(ToFixed(), SUNDAY);

    return (thisSunday - firstSunday) / 7 + 1;
}


int GregorianDate::GetUSMemorialDay(int year)
{
	return FirstWeekday( GregorianDate(year, 5, 31), MONDAY);
}


int GregorianDate::GetUSLaborDay(int year)
{
	return FirstWeekday( GregorianDate(year, 9, 1), MONDAY);
}


int GregorianDate::GetUSElectionDay(int year)
{
	return FirstWeekday( GregorianDate(year, 11, 2), TUESDAY );
}


int GregorianDate::GetUSThanksgiving(int year)
{
    return NthWeekday( GregorianDate(year, 11, 1), 4, THURSDAY);
}


int GregorianDate::GetUSDSTBegin(int year)
{
	if (year >= 2007)
	{
        return NthWeekday( GregorianDate(year, 3, 1), 2, SUNDAY);
	}
	else
	{
        return FirstWeekday( GregorianDate(year, 4, 1), SUNDAY);
	}
}


int GregorianDate::GetUSDSTEnd(int year)
{
	if (year >= 2007)
	{
		return FirstWeekday( GregorianDate(year, 11, 1), SUNDAY);
	}
	else
	{
		return LastWeekday( GregorianDate(year, 10, 31), SUNDAY);
	}
}


int GregorianDate::GetEaster(int year)
{
	int century = Floor(year, 100) + 1;
	int shiftedEpact = Mod( 14 + 11 * Mod(year, 19) - Floor(3 * century, 4) +
		Floor(5 + 8 * century, 25), 30);
	
	int adjustedEpact;
	if (shiftedEpact==0 || (shiftedEpact==1 && 10 < Mod(year, 19)))
	{
		adjustedEpact = shiftedEpact + 1;
	}
	else
	{
		adjustedEpact = shiftedEpact;
	}
	
    int paschalMoon = GregorianDate(year, 4, 19).ToFixed() - adjustedEpact;
	
	return WeekdayAfter(paschalMoon, SUNDAY);
}


int GregorianDate::GetSeptuagesimaSunday(int gyear)
{
	return GetEaster(gyear) - 63;
}


int GregorianDate::GetSexagesimaSunday(int gyear)
{
	return GetEaster(gyear) - 56;
}


int GregorianDate::GetShroveSunday(int gyear)
{
	return GetEaster(gyear) - 49;
}


int GregorianDate::GetShroveMonday(int gyear)
{
	return GetEaster(gyear) - 48;
}


int GregorianDate::GetMardiGras(int gyear)
{
	return GetEaster(gyear) - 47;
}


int GregorianDate::GetAshWednesday(int gyear)
{
	return GetEaster(gyear) - 46;
}


int GregorianDate::GetPassionSunday(int gyear)
{
	return GetEaster(gyear) - 14;
}


int GregorianDate::GetPalmSunday(int gyear)
{
	return GetEaster(gyear) - 7;
}


int GregorianDate::GetHolyThursday(int gyear)
{
	return GetEaster(gyear) - 3;
}


int GregorianDate::GetGoodFriday(int gyear)
{
	return GetEaster(gyear) - 2;
}


int GregorianDate::GetRogationSunday(int gyear)
{
	return GetEaster(gyear) + 35;
}


int GregorianDate::GetAscensionDay(int gyear)
{
	return GetEaster(gyear) + 39;
}


int GregorianDate::GetPentecost(int gyear)
{
	return GetEaster(gyear) + 49;
}


int GregorianDate::GetWhitmundy(int gyear)
{
	return GetEaster(gyear) + 50;
}


int GregorianDate::GetTrinitySunday(int gyear)
{
	return GetEaster(gyear) + 56;
}


int GregorianDate::GetCorpusChristi(int gyear)
{
	return GetEaster(gyear) + 60;
}


int GregorianDate::GetCorpusChristiUSA(int gyear)
{
	return GetEaster(gyear) + 63;
}


int GregorianDate::GetDaysInMonth(int year, int month)
{
    ValidateBetween(month, 1, 12);

	switch (month)
	{
	case 1:
	case 3:
	case 5:
	case 7:
    case 8:
    case 10:
    case 12:
		return 31;

	case 2:
		return IsLeapYear(year) ? 29 : 28;

	case 4:
	case 6:
	case 9:
	case 11:
		return 30;

	default:
        return 0;
	}
}


int GregorianDate::GetWeeksInMonth(int year, int month)
{
    GregorianDate start(year, month, 1);
    GregorianDate end(year, month, GetDaysInMonth(year, month));

    int sundayStartFD = WeekdayOnOrBefore(start, SUNDAY);
    int sundayEndFD = WeekdayOnOrBefore(end, SUNDAY);

    return (sundayEndFD - sundayStartFD) / 7 + 1;
}




#ifndef COPTIC_H
#define COPTIC_H

#include "fixeddate.h"


namespace libcalendar
{

class CopticDate : public FixedDate
{
public:
	CopticDate();
	CopticDate(int fd);
	CopticDate(int year, int month, int day);

	void FromFixed(int fd);
	int ToFixed() const;

	void SetYear(int year);
	int Year() const;

	void SetMonth(int month);
	int Month() const;

	void SetDay(int day);
	int Day() const;

	std::string ToString() const;

	static bool IsLeapYear(int year);

	static std::vector<int> GetCopticInGregorian(int gregorianYear, int copticMonth, int copticDay);

	static std::vector<int> GetCopticChristmas(int gregorianYear);

	static const int EPOCH;

	static const std::vector<std::string> MONTHS;
	static const std::vector<std::string> DAYS_OF_WEEK;

private:
	int year_, month_, day_;

    void ValidateDate() const;

};


}

#endif


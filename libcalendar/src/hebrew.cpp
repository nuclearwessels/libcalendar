

#include "hebrew.h"
#include "coptic.h"
#include "gregorian.h"
#include "julian.h"

#include <algorithm>

using namespace std;
using namespace libcalendar;


const string HebrewDate::DAYS_OF_WEEK[7] = { "yom rishon", "yom sheni",
	"yom shelishi", "yom revi'i", "yom hamishi", 
	"yom shishi", "yom shabbat" };

const int HebrewDate::EPOCH = JulianDate(-3761, 10, 7).ToFixed();

const int HebrewDate::TISHRI = 7;
const int HebrewDate::NISAN = 1;

std::vector<Event> HebrewDate::GetHebrewEvents(int gregorianYear)
{
    std::vector<Event> events;

    //events.emplace_back(Event("Hebrew", "Hebrew New Year", GetNewYear(gregorianYear),
    events.emplace_back(Event("Hebrew", "Yom Kippur", GetYomKippur(gregorianYear)));
    events.emplace_back(Event("Hebrew", "Rosh Ha Shanah", GetRoshHaShanah(gregorianYear)));
    events.emplace_back(Event("Hebrew", "Sukkot", GetSukkot(gregorianYear)));
    events.emplace_back(Event("Hebrew", "Hoshana Rabba", GetHoshanaRabba(gregorianYear)));
    events.emplace_back(Event("Hebrew", "Shemini Azeret", GetSheminiAzeret(gregorianYear)));
    events.emplace_back(Event("Hebrew", "Simhat Torah", GetSimhatTorah(gregorianYear)));
    events.emplace_back(Event("Hebrew", "Passover", GetPassover(gregorianYear)));
    events.emplace_back(Event("Hebrew", "Shavuot", GetShavuot(gregorianYear)));
    events.emplace_back(Event("Hebrew", "Purim", GetPurim(gregorianYear)));
    events.emplace_back(Event("Hebrew", "Ta Anit Esther", GetTaAnitEsther(gregorianYear)));
    events.emplace_back(Event("Hebrew", "Yom Ha Zikkaron", GetYomHaZikkaron(gregorianYear)));
    events.emplace_back(Event("Hebrew", "Sh Ela", GetShEla(gregorianYear)));

    std::vector<int> birkathHaHamaDates = GetBirkathHaHama(gregorianYear);

    events.emplace_back(Event("Hebrew", "Birkath Ha Hama", birkathHaHamaDates));

    return events;
};


HebrewDate::HebrewDate()
{
	FromFixed(EPOCH);
}


HebrewDate::HebrewDate(int fixed)
{
	FromFixed(fixed);
}


HebrewDate::HebrewDate(int year, int month, int day)
{
	SetYear(year);
	SetMonth(month);
	SetDay(day);
}


void HebrewDate::SetYear(int year)
{
	year_ = year;
}


int HebrewDate::Year() const
{
	return year_;
}


void HebrewDate::SetMonth(int month)
{
	month_ = month;
}


int HebrewDate::Month() const
{
	return month_;
}


void HebrewDate::SetDay(int day)
{

	day_ = day;
}


int HebrewDate::Day() const
{
	return day_;
}


void HebrewDate::FromFixed(int fixed)
{
	//Rational fr(fixed - epoch);
	//fr /= Rational(35975351LL, 98496LL);
	//long long temp = floor( fr.getNumerator(), fr.getDenominator() ) + 1;
	int approx = (int) ((double) (fixed - EPOCH) / 365.25f);
	//assert( temp <= 2147483647LL && temp >= -2147483648LL);
	
	//int approx = (int) temp;
	
	int y = approx - 1;
	bool done = false;
	while (!done)
	{
		if (GetNewYear(y) <= fixed)
		{
			year_ = y;
			y++;
		}
		else
		{
			done = true;
		}
	}
	
	int start = (fixed < HebrewDate(year_, NISAN, 1).ToFixed()) ? TISHRI : NISAN;
	
	month_ = GetLastMonthOfYear(year_);
	
	for (int m=GetLastMonthOfYear(year_); m >= start; m--)
	{
		if (fixed <= HebrewDate(year_, m, GetLastDayOfMonth(m, year_)).ToFixed())
		{
			month_ = m;
		}
	}
	
	day_ = fixed - HebrewDate(year_, month_, 1).ToFixed() + 1;
}


int HebrewDate::ToFixed() const
{
	int returnme = GetNewYear(year_) + day_ - 1;
	
	if (month_ < TISHRI)
	{
		for (int m = TISHRI; m <= GetLastMonthOfYear(year_); m++)
		{
			returnme += GetLastDayOfMonth(m, year_);
		}
		
		for (int m=NISAN; m<month_; m++)
		{
			returnme += GetLastDayOfMonth(m, year_);
		}
	}
	else
	{
		for (int m = TISHRI; m < month_; m++)
		{
			returnme += GetLastDayOfMonth(m, year_);
		}
	}

	return returnme;
}


string HebrewDate::ToString() const
{
	char temp[100];
	snprintf(temp, 100, "%s(%d) %d, %d", GetMonthName(month_, IsLeapYear(year_)).c_str(), 
                                         month_, 
                                         day_, 
                                         year_);
	return string(temp);
}


string HebrewDate::GetMonthName(int hebrewMonth, bool leapYear)
{
	string returnme; 
	
	switch (hebrewMonth)
	{
	case 1:  returnme = "Nisan"; break;
	case 2:  returnme = "Iyyar"; break;
	case 3:  returnme = "Sivan"; break;
	case 4:  returnme = "Tammuz"; break;
	case 5:  returnme = "Av"; break;
	case 6:  returnme = "Elul"; break;
	case 7:  returnme = "Tishri"; break;
	case 8:  returnme = "Marheshvan"; break;
	case 9:  returnme = "Kislev"; break;
	case 10: returnme = "Tevet"; break;
	case 11: returnme = "Shevat"; break;
	case 12: returnme = (leapYear ? "Adar I" : "Adar"); break;
	case 13: returnme = "Adar II"; break;
	}

	return returnme;
}

bool HebrewDate::IsLeapYear(int year)
{
	return ( Mod(7*year+1, 19)<7) ? true : false;
}


int HebrewDate::GetLastMonthOfYear(int year)
{
	return (IsLeapYear(year) ? 13 : 12);
}


double HebrewDate::GetMolad(int hebrewMonth, int hebrewYear)
{
	int y = (hebrewMonth < TISHRI) ? (hebrewYear+1) : hebrewYear;
	int monthsElapsed = hebrewMonth - TISHRI + Floor(235 * y - 234, 19);

	double molad = (double) EPOCH - 876.0f / 25920.0f +
		(double) monthsElapsed * ( 29.0f + 0.5f + 793.0f / 25920.0f);

	return molad;
}


int HebrewDate::GetElapsedDays(int hebrewYear)
{
	int monthsElapsed = Floor(235 * hebrewYear - 234, 19);
	long long partsElapsed = 12084LL + 13753LL * (long long) monthsElapsed;
	int day = (int) (29 * monthsElapsed + Floor( partsElapsed, 25920LL));
	return (Mod(3 * (day+1), 7) < 3) ? (day + 1) : day;
}


int HebrewDate::GetNewYearDelay(int hebrewYear)
{
	int returnme;
	
	int ny0 = GetElapsedDays(hebrewYear-1);
	int ny1 = GetElapsedDays(hebrewYear);
	int ny2 = GetElapsedDays(hebrewYear+1);
	
	if ( (ny2 - ny1) == 356)
	{
		returnme = 2;
	}
	else if ( (ny1 - ny0) == 382)
	{
		returnme = 1;
	}
	else
	{
		returnme = 0;
	}
	
	return returnme;
}


int HebrewDate::GetNewYear(int hebrewYear)
{
	return EPOCH + GetElapsedDays(hebrewYear) + GetNewYearDelay(hebrewYear);
}


int HebrewDate::GetLastDayOfMonth(int hebrewMonth, int hebrewYear)
{
	int returnme;
	
	switch (hebrewMonth)
	{
	case 2:
	case 4:
	case 6:
	case 10:
	case 13:
		returnme = 29;
		break;
		
	case 12:
		returnme = (IsLeapYear(hebrewYear) ? 30 : 29);
		break;
		
	case 8:
		returnme = (IsLongMarheshvan(hebrewYear) ? 30 : 29);
		break;
		
	case 9:
		returnme = (IsShortKislev(hebrewYear) ? 29 : 30);
		break;
		
	default:
		returnme = 30;
	}

	return returnme;
}


bool HebrewDate::IsLongMarheshvan(int hebrewYear)
{
	bool returnme;
	
	switch (GetDaysInYear(hebrewYear))
	{
	case 355:
	case 385:
		returnme = true;
		break;
		
	default:
		returnme = false;
	}
	
	return returnme;
}


bool HebrewDate::IsShortKislev(int hebrewYear)
{
	bool returnme;
	
	switch (GetDaysInYear(hebrewYear))
	{
	case 353:
	case 383:
		returnme = true;
		break;
		
	default:
		returnme = false;
	}
	
	return returnme;

}


int HebrewDate::GetDaysInYear(int hebrewYear)
{
	return GetNewYear(hebrewYear+1) - GetNewYear(hebrewYear);
}



int HebrewDate::GetYomKippur(int gregorianYear)
{
	int hebrewYear = gregorianYear - GregorianDate::GetYearFromFixed(EPOCH) + 1;
		
    return HebrewDate(hebrewYear, TISHRI, 10).ToFixed();
}


int HebrewDate::GetRoshHaShanah(int gregorianYear)
{
	int hebrewYear = gregorianYear - GregorianDate::GetYearFromFixed(EPOCH) + 1;
		
	return HebrewDate(hebrewYear, TISHRI, 1);
}


int HebrewDate::GetSukkot(int gregorianYear)
{
	int hebrewYear = gregorianYear - GregorianDate::GetYearFromFixed(EPOCH) + 1;
		
	return HebrewDate(hebrewYear, TISHRI, 15);
}


int HebrewDate::GetHoshanaRabba(int gregorianYear)
{
	int hebrewYear = gregorianYear - GregorianDate::GetYearFromFixed(EPOCH) + 1;
		
	return HebrewDate(hebrewYear, TISHRI, 21);
}


int HebrewDate::GetSheminiAzeret(int gregorianYear)
{
	int hebrewYear = gregorianYear - GregorianDate::GetYearFromFixed(EPOCH) + 1;
		
	return HebrewDate(hebrewYear, TISHRI, 22);
}


int HebrewDate::GetSimhatTorah(int gregorianYear)
{
	int hebrewYear = gregorianYear - GregorianDate::GetYearFromFixed(EPOCH) + 1;
		
	return HebrewDate(hebrewYear, TISHRI, 23);
}


int HebrewDate::GetPassover(int gregorianYear)
{
	int hebrewYear = gregorianYear - GregorianDate::GetYearFromFixed(EPOCH);
	
	return HebrewDate(hebrewYear, NISAN, 15);
}


int HebrewDate::GetShavuot(int gregorianYear)
{
	int hebrewYear = gregorianYear - GregorianDate::GetYearFromFixed(EPOCH);
	
	return HebrewDate(hebrewYear, 3, 21);
}


bool HebrewDate::GetOmer(int fd, int& completedWeeks, int& excessDays)
{
	int c = fd - GetPassover(GregorianDate::GetYearFromFixed(fd));
	
	if (c>=1 && c<=49)
	{
		completedWeeks = Floor(c, 7);
		excessDays = Mod(c, 7);
        return true;
	}
	else
	{
		return false;
	}
}


int HebrewDate::GetPurim(int gregorianYear)
{
	int hebrewYear = gregorianYear - GregorianDate::GetYearFromFixed(EPOCH);

	int lastMonth = GetLastMonthOfYear(hebrewYear);
	
	return HebrewDate(hebrewYear, lastMonth, 14);
}


int HebrewDate::GetTaAnitEsther(int gregorianYear)
{
	HebrewDate purim(GetPurim(gregorianYear));
	
	HebrewDate returnme;
	
	if (GetDayOfWeek(purim) == SUNDAY)
	{
		returnme = purim - 3;
	}
	else
	{
		returnme = purim - 1;
	}
	
	return returnme;
}


int HebrewDate::GetTishahBeAv(int gregorianYear)
{
	int hebrewYear = gregorianYear - GregorianDate::GetYearFromFixed(EPOCH);
	
	HebrewDate av9(hebrewYear, 5, 9);
	
	HebrewDate returnme;
	
	if (GetDayOfWeek(av9) == SATURDAY)
	{
		returnme = av9 + 1;
	}
	else
	{
		returnme = av9;
	}
	
	return returnme;
}


int HebrewDate::GetYomHaZikkaron(int gregorianYear)
{
	int hebrewYear = gregorianYear - GregorianDate::GetYearFromFixed(EPOCH);
		
	HebrewDate iyyar4(hebrewYear, 2, 4);
	
	if (WEDNESDAY < GetDayOfWeek(iyyar4)) 
	{
		return WeekdayBefore(iyyar4, WEDNESDAY);
	}
	else
	{
		return iyyar4;
	}
}


int HebrewDate::GetShEla(int gregorianYear)
{
	return CopticDate(gregorianYear, 3, 26);
}



std::vector<int> HebrewDate::GetBirkathHaHama(int gregorianYear)
{
    vector<int> results;

    vector<int> dates = CopticDate::GetCopticInGregorian(gregorianYear, 7, 30);
	
	if (dates.size())
	{
		if (Mod(CopticDate(dates[0]).Year(), 28) == 17)
		{
            copy(dates.begin(), dates.end(), results.end() );
		}
	}

    return results;
}


int HebrewDate::GetHebrewBirthday(int birthdate, int hebrewYear)
{
	HebrewDate hbd(birthdate);
	
	if ( hbd.Month() == GetLastMonthOfYear(hbd.Year()))
	{
		return HebrewDate(hebrewYear, GetLastMonthOfYear(hebrewYear), hbd.Day() );
	}
	else
	{
		return HebrewDate(hebrewYear, hbd.Month(), 1) + hbd.Day() - 1;
	}
}
	

std::vector<int> HebrewDate::GetHebrewBirthdayInGregorian(int birthdate, int gregorianYear)
{
    std::vector<int> results;

	const int jan1 = GregorianDate(gregorianYear, 1, 1).ToFixed();
	const int dec31 = GregorianDate(gregorianYear, 12, 31).ToFixed();
	
	const int y = HebrewDate(jan1).Year();
	
	for (int i=y; i<=y+1; i++)
	{
		int date = GetHebrewBirthday(birthdate, i);
		
		if (date>=jan1 && date<=dec31)
		{
			results.push_back(date);
		}
	}

    return results;
}


int HebrewDate::GetYahrzeit(int deathdate, int hebrewYear)
{
	HebrewDate hdd(deathdate);
	
	if ( hdd.Month() == 8 && 
         hdd.Day() == 30 &&
		 !IsLongMarheshvan(hdd.Year()+1) )
	{
		return HebrewDate(hebrewYear, 9, 1) - 1;
	}
	else if (hdd.Month() == 9 && 
             hdd.Day() == 30 &&
		     IsShortKislev(hdd.Year()+1) )
	{
		return HebrewDate(hebrewYear, 10, 1) - 1;
	}
	else if (hdd.Month() == 13)
	{
		return HebrewDate(hebrewYear, GetLastMonthOfYear(hebrewYear), hdd.Day() );
	}
	else if (hdd.Day() == 30 && 
             hdd.Month() == 12 && 
             !IsLeapYear(hebrewYear) )
	{
		return HebrewDate(hebrewYear, 11, 30);
	}
	else
	{
		return HebrewDate(hebrewYear, hdd.Month(), 1) + hdd.Day() - 1;
	}
}


std::vector<int> HebrewDate::GetYahrzeitInGregorian(int deathdate, int gregorianYear)
{	
    std::vector<int> results;

	const int jan1 = GregorianDate(gregorianYear, 1, 1).ToFixed();
	const int dec31 = GregorianDate(gregorianYear, 12, 31).ToFixed();
	
	const int y = HebrewDate(jan1).Year();
	
	for (int i=y; i<=y+1; i++)
	{
		int date = GetYahrzeit(deathdate, y);
		
		if (date>=jan1 && date<=dec31)
		{
			results.push_back(date);
		}
	}

    return results;
}





#include "datetimeman.h"


using namespace libcalendarman;

//DateTime::DateTime()
//{
//    _dt = new libcalendar::DateTime();
//}
//
//
//DateTime::DateTime(int fd, int hour, int min, double second)
//{
//    _dt = new libcalendar::DateTime(fd, hour, min, second);
//}
//
//
//DateTime::DateTime(double dt)
//{
//    _dt = new libcalendar::DateTime(dt);
//}
//
//
//DateTime::~DateTime()
//{
//    this->!DateTime();
//}
//
//
//DateTime::!DateTime()
//{
//    if (_dt)
//    {
//        delete _dt;
//        _dt = 0;
//    }
//}
//
//
//DateTime^ DateTime::operator = (double d)
//{
//    *_dt = d;
//    return this;
//}
//
//DateTime^ DateTime::operator = (int i)
//{
//    *_dt = i;
//    return this;
//}
//
//DateTime::operator double()
//{
//    return (double) *_dt;
//}
//
//DateTime::operator int()
//{
//    return (int) *_dt;
//}


double DateTime::UniversalFromLocal(double dt, Locale^ l)
{
    libcalendar::Locale lcl = GetLocale(l);
    return libcalendar::UniversalFromLocal(dt, lcl);
}


double DateTime::LocalFromUniversal(double dt, Locale^ l)
{
    libcalendar::Locale lcl = GetLocale(l);
    return libcalendar::LocalFromUniversal( dt, lcl);
}


double DateTime::StandardFromUniversal(double dt, Locale^ l)
{
    libcalendar::Locale lcl = GetLocale(l);
    return libcalendar::StandardFromUniversal( dt, lcl);
}


double DateTime::UniversalFromStandard(double dt, Locale^ l)
{
    libcalendar::Locale lcl = GetLocale(l);
    return libcalendar::UniversalFromStandard( dt, lcl);
}

double DateTime::StandardFromLocal(double dt, Locale^ l)
{
    libcalendar::Locale lcl = GetLocale(l);
    return libcalendar::StandardFromLocal( dt, lcl);
}

double DateTime::LocalFromStandard(double dt, Locale^ l)
{
    libcalendar::Locale lcl = GetLocale(l);
    return libcalendar::LocalFromStandard( dt, lcl);
}

double DateTime::DynamicalFromUniversal(double dt)
{
    return libcalendar::DynamicalFromUniversal(dt);
}


double DateTime::UniversalFromDynamical(double dt)
{
    return libcalendar::UniversalFromDynamical(dt);
}

double DateTime::ApparentFromLocal(double dt)
{
    return libcalendar::ApparentFromLocal(dt);
}

double DateTime::LocalFromApparent(double dt)
{
    return libcalendar::LocalFromApparent(dt);
}

double DateTime::EphemerisCorrection(double dt)
{
    return libcalendar::EphemerisCorrection(dt);
}


double DateTime::JulianCenturies(double dt)
{
    return libcalendar::JulianCenturies(dt);
}


double DateTime::EquationOfTime(double dt)
{
    return libcalendar::EquationOfTime(dt);
}

double DateTime::Obliquity(double dt)
{
    return libcalendar::Obliquity(dt);
}


double DateTime::Sidereal(double dt)
{
    return libcalendar::Sidereal( dt );
}

double DateTime::Midnight(double dt, Locale^ l)
{
    libcalendar::Locale lcl = GetLocale(l);
    return libcalendar::Midnight( dt, lcl);
}


double DateTime::Midday(double dt, Locale^ l)
{
    libcalendar::Locale lcl = GetLocale(l);
    return libcalendar::Midnight( dt, lcl);
}


double DateTime::SolarLongitude(double dt)
{
    return libcalendar::SolarLongitude( dt);
}

double DateTime::Nutation(double dt)
{
    return libcalendar::Nutation( dt);
}

double DateTime::Aberration(double dt)
{
    return libcalendar::Aberration( dt);
}

double DateTime::SolarLongitudeAfter(double dt, double phi)
{
    return libcalendar::SolarLongitudeAfter( dt, phi);
}


double DateTime::LunarLongitude(double dt)
{
    return libcalendar::LunarLongitude(dt);
}


double DateTime::NthNewMoon(int n)
{
    return libcalendar::NthNewMoon(n);
}


double DateTime::NewMoonAfter(double dt)
{
    return libcalendar::NewMoonAfter(dt);
}


double DateTime::NewMoonBefore(double dt)
{
    return libcalendar::NewMoonBefore(dt);
}


double DateTime::LunarPhase(double dt)
{
    return libcalendar::LunarPhase(dt);
}

double DateTime::LunarPhaseBefore(double dt, double phi)
{
    return libcalendar::LunarPhaseBefore(dt, phi);
}

double DateTime::LunarPhaseAfter(double dt, double phi)
{
    return libcalendar::LunarPhaseAfter(dt, phi);
}

libcalendar::Locale DateTime::GetLocale(Locale^ l)
{
    //Locale(double latDeg, double lonDeg, double elevMeters, int timeZone);
    return libcalendar::Locale( l->LatDeg, l->LonDeg, l->Elevation, l->TimeZone);
}

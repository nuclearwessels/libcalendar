

#pragma once

using namespace System;
using namespace System::Collections::Generic;

#include "fixeddateman.h"

namespace libcalendarman
{

public delegate int SingleEventDelegate(int year);
public delegate array<int>^ MultipleEventDelegate(int year);

public delegate FixedDate^ DateCreateDelegate(int fd);

ref class Event;

public ref class EventType
{
public:
    EventType(String^ category, String^ name, SingleEventDelegate^ sed, DateCreateDelegate^ dfd);
    EventType(String^ category, String^ name, MultipleEventDelegate^ med, DateCreateDelegate^ dfd);

    property String^ Category
    {
        String^ get() { return _category; }
    }

    property String^ Name
    {
        String^ get() { return _name; }
    }

    property SingleEventDelegate^ SingleEvent
    {
        SingleEventDelegate^ get() { return _singleEvent; }
    }

    property MultipleEventDelegate^ MultipleEvent
    {
        MultipleEventDelegate^ get() { return _multipleEvent; }
    }

    property DateCreateDelegate^ DateCreate
    {
        DateCreateDelegate^ get() { return _dateCreate; }
    }

    void AddEvent(List<Event^>^ events, int year);

private:
    String^ _name, ^_category;
    SingleEventDelegate^ _singleEvent;
    MultipleEventDelegate^ _multipleEvent;
    DateCreateDelegate^ _dateCreate;
};


public ref class Event
{
public:
    Event(EventType^ eventType, int fd);

    property EventType^ Type
    {
        EventType^ get() { return _eventType; }
    }

    property int FixedDate
    {
        int get() { return _fd; }
    }

    

private:
    EventType^ _eventType;
    int _fd;
};


}

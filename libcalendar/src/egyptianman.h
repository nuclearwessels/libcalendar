

#pragma once

using namespace System;

#include "fixeddateman.h"
#include "egyptian.h"

namespace libcalendarman
{
    
public ref class EgyptianDate : public FixedDate
{
public:
    EgyptianDate();
    EgyptianDate(int fd);
    EgyptianDate(int year, int month, int day);
    !EgyptianDate();
    ~EgyptianDate();

    virtual void FromFixed(int fd) override;
	virtual int ToFixed() override;

    property int Year
    {
        void set(int year) { _ed->SetYear(year); }
        int get() { return _ed->Year(); }
    }

    property int Month
    {
        void set(int month) { _ed->SetMonth(month); }
        int get() { return _ed->Month(); }
    }

    property int Day
    {
        void set(int day) { _ed->SetDay(day);  }
        int get() { return _ed->Day(); }
    }

	virtual String^ ToString() override;

    static const int Epoch = -272787;

private:
    libcalendar::EgyptianDate* _ed;
};



}






#pragma once


#include "fixeddate.h"
#include "event.h"

namespace libcalendar
{

class HebrewDate : public FixedDate
{
public:
	HebrewDate();
	HebrewDate(int fixed);
	HebrewDate(int year, int month, int day);

	void SetYear(int year);
	int Year() const;

	void SetMonth(int month);
	int Month() const;

	void SetDay(int day);
	int Day() const;

	void FromFixed(int fd);
	int ToFixed() const;

	std::string ToString() const;
	
	static std::string GetMonthName(int hebrewMonth, bool leapYear);
	static bool IsLeapYear(int year);
	static int GetLastMonthOfYear(int year);
	static double GetMolad(int hebrewMonth, int hebrewYear);
	static int GetElapsedDays(int hebrewYear);
	static int GetNewYearDelay(int hebrewYear);
	static int GetNewYear(int hebrewYear);
	static int GetLastDayOfMonth(int hebrewMonth, int hebrewYear);
	static bool IsLongMarheshvan(int hebrewYear);
	static bool IsShortKislev(int hebrewYear);
	static int GetDaysInYear(int hebrewYear);		
	static int GetYomKippur(int gregorianYear);
	static int GetRoshHaShanah(int gregorianYear);
	static int GetSukkot(int gregorianYear);
	static int GetHoshanaRabba(int gregorianYear);
	static int GetSheminiAzeret(int gregorianYear);
	static int GetSimhatTorah(int gregorianYear);
	static int GetPassover(int gregorianYear);
	static bool GetOmer(int fd, int& completedWeeks, int& excessDays);
	static int GetShavuot(int gregorianYear);
	static int GetPurim(int gregorianYear);
	static int GetTaAnitEsther(int gregorianYear);
	static int GetTishahBeAv(int gregorianYear);
	static int GetYomHaZikkaron(int gregorianYear);
	static int GetShEla(int gregorianYear);
	static std::vector<int> GetBirkathHaHama(int gregorianYear);
	
	static int GetHebrewBirthday(int birthdate, int hebrewYear);
	static std::vector<int> GetHebrewBirthdayInGregorian(int birthdate, int gregorianYear);
	
	static int GetYahrzeit(int deathdate, int hebrewYear);
	static std::vector<int> GetYahrzeitInGregorian(int deathdate, int gregorianYear);
	
	static const int EPOCH;
	static const std::string DAYS_OF_WEEK[7];
	
    static std::vector<Event> GetHebrewEvents(int gregorianYear);

private:
	int year_, month_, day_;
	
	static const int TISHRI, NISAN; 
};

}

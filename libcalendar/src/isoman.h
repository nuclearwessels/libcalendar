

#pragma once

#include "fixeddateman.h"
#include "iso.h"

namespace libcalendarman
{

public ref class ISODate : public FixedDate
{
public:
	ISODate();
	ISODate(int fixed);
	ISODate(int year, int week, int day);
    !ISODate();
    ~ISODate();

	virtual void FromFixed(int fixed) override;
	virtual int ToFixed() override;

    property int Year
    {
        int get() { return id->Year(); }
        void set(int year) { id->SetYear(year); }
    }

    property int Week
    {
        int get() { return id->Week(); }
        void set(int week) { id->SetWeek(week); }
    }

    property int Day
    {
        int get() { return id->Day(); }
        void set(int day) { id->SetDay(day); }
    }

	virtual System::String^ ToString() override;

	static const int Epoch = libcalendar::ISODate::EPOCH;

private:
	libcalendar::ISODate* id;
};


}


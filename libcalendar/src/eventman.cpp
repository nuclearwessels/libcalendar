

#include "eventman.h"

#include "gregorianman.h"
#include "hebrewman.h"
#include "julianman.h"


using namespace libcalendarman;


EventType::EventType(String^ category, String^ name, SingleEventDelegate^ sed, DateCreateDelegate^ dcd) : 
    _name(name),
    _category(category),
    _singleEvent(sed),
    _multipleEvent(nullptr),
    _dateCreate(dcd)
{

}


EventType::EventType(String^ category, String^ name, MultipleEventDelegate^ med, DateCreateDelegate^ dcd) : 
    _name(name),
    _category(category),
    _singleEvent(nullptr),
    _multipleEvent(med),
    _dateCreate(dcd)
{


}


void EventType::AddEvent(List<Event^>^ events, int year)
{
    if (_singleEvent != nullptr)
    {
        Event^ evt = gcnew Event(this, _singleEvent(year) );
        events->Add(evt);
    }
    else
    {
        array<int>^ fds = _multipleEvent(year);
        for each (int fd in fds)
        {
            Event^ evt = gcnew Event(this, fd );
            events->Add(evt);
        }
    }
}


//static array<EventType^>^ EventType::GetAllEvents()
//{
//    List<EventType^>^ l = gcnew List<EventType^>();
//        
//    for each (EventType^ et in GregorianDate::ChristianEventTypes)
//    {
//        l->Add(et);
//    }
//
//    for each (EventType^ et in GregorianDate::USEventTypes)
//    {
//        l->Add(et);
//    }
//    
//    for each (EventType^ et in JulianDate::OrthodoxEventTypes)
//    {
//        l->Add(et);
//    }
//
//    for each (EventType^ et in HebrewDate::HebrewEventTypes)
//    {
//        l->Add(et);
//    }
//
//    return l->ToArray();
//}




//array<Event^>^ EventType::GetAllEvents(int gyear)
//{
//    List<Event^>^ l = gcnew List<Event^>();
//
//    for each (EventType^ et in GetAllEventTypes())
//    {
//        et->AddEvent(l, gyear);
//    }
//
//    return l->ToArray();
//}
//

Event::Event(EventType^ eventType, int fd) : _eventType(eventType), 
                                             _fd(fd)
{


}


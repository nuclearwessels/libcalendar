

#include "event.h"

using namespace libcalendar;
using namespace std;


Event::Event(const std::string& category, const std::string& name, int fd) :
    category_(category),
    name_(name)
{
    fds_.push_back(fd);
}

Event::Event(const std::string& category, const std::string& name, const std::vector<int>& fds) :
    category_(category),
    name_(name)
{
    std::copy(fds.cbegin(), fds.cend(), std::back_inserter(fds_));
}

const std::string& Event::name() const
{
    return name_;
}


const string& Event::category() const
{
    return category_;
}



const std::vector<int>& Event::fixedDates() const
{
    return fds_;
}


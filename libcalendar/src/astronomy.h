

#pragma once


namespace libcalendar
{

class Locale
{
public:
    Locale();
    Locale(double latDeg, double lonDeg, double elevMeters, int timeZone);

    void SetLat(double lat, bool degrees = true);
    double Lat() const;
    double LatRad() const;

    void SetLon(double lon, bool degrees = true);
    double Lon() const;
    double LonRad() const;

    void SetElev(double meters);
    double Elev() const;

    void SetTimeZone(int zone);
    int TimeZone() const;

    static double ConvertDMSToDeg(double deg, double min, double seconds);
    static void ConvertDegToDMS(double degrees, double& deg, double& min, double& seconds);

private:
    double _lat, _lon, _elev;
    int _timeZone;
};


//const Locale URBANA;
//const Locale MECCA;
//const Locale JERUSALEM;

const Locale URBANA(40.1L, -88.2L, 225.0, -6);
const Locale MECCA( Locale::ConvertDMSToDeg(21, 25, 24),
                    Locale::ConvertDMSToDeg(39, 49, 24),
                    298, 3);
const Locale JERUSALEM( 31.8, 35.2, 800, 2);


}


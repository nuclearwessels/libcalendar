

#include "oldhinduman.h"


using namespace libcalendarman;


int OldHindu::GetHinduDayCount(int fd)
{
    return libcalendar::OldHindu::GetHinduDayCount(fd);
}

int OldHindu::GetJovianYear(int fd)
{
    return libcalendar::OldHindu::GetJovianYear(fd);
}


OldHinduSolarDate::OldHinduSolarDate()
{
    _ohsd = new libcalendar::OldHinduSolarDate();
}

OldHinduSolarDate::OldHinduSolarDate(int year, int month, int day)
{
    _ohsd = new libcalendar::OldHinduSolarDate(year, month, day);
}


OldHinduSolarDate::OldHinduSolarDate(int fd)
{
    _ohsd = new libcalendar::OldHinduSolarDate(fd);
}


OldHinduSolarDate::!OldHinduSolarDate()
{
    if (_ohsd) 
    {
        delete _ohsd;
        _ohsd = 0;
    }
}


OldHinduSolarDate::~OldHinduSolarDate()
{
    this->!OldHinduSolarDate();
}


void OldHinduSolarDate::FromFixed(int fd)
{
    _ohsd->FromFixed(fd);
}

int OldHinduSolarDate::ToFixed() 
{
    return _ohsd->ToFixed();
}


String^ OldHinduSolarDate::ToString() 
{
    return gcnew String( _ohsd->ToString().c_str() );
}
        





OldHinduLunarDate::OldHinduLunarDate()
{
    _ohld = new libcalendar::OldHinduLunarDate();
}


OldHinduLunarDate::OldHinduLunarDate(int fd)
{
    _ohld = new libcalendar::OldHinduLunarDate(fd);
}

OldHinduLunarDate::OldHinduLunarDate(int year, int month, bool leap, int day)
{
    _ohld = new libcalendar::OldHinduLunarDate(year, month, leap, day);
}


OldHinduLunarDate::~OldHinduLunarDate()
{
    this->!OldHinduLunarDate();
}

OldHinduLunarDate::!OldHinduLunarDate()
{
    if (_ohld)
    {
        delete _ohld;
        _ohld = 0;
    }
}


int OldHinduLunarDate::ToFixed()
{
    return _ohld->ToFixed();
}

void OldHinduLunarDate::FromFixed(int fd)
{
    _ohld->FromFixed(fd);
}

bool OldHinduLunarDate::IsLeapYear(int lunarYear)
{
    return libcalendar::OldHinduLunarDate::IsLeapYear(lunarYear);
}










#pragma once

#include "astronomy.h"


namespace libcalendarman
{

public ref class Locale
{
public:
    Locale();
    Locale(double latDeg, double lonDeg, double elevMeters, int timeZone);
    Locale(const libcalendar::Locale& locale);
    ~Locale();
    
    property double Lat
    {
        void set(double value) { _locale->SetLat(value); }
        double get() { return _locale->Lat(); }
    }

    property double LatDeg
    {
        void set(double value) { _locale->SetLat(value, true); }
        double get() { return _locale->Lat(); }
    }

    property double LatRad
    {
        void set(double value) { _locale->SetLat(value, false); }
        double get() { return _locale->LatRad(); }
    }

    property double Lon
    {
        void set(double value) { _locale->SetLon(value); }
        double get() { return _locale->Lon(); }
    }

    property double LonDeg
    {
        void set(double value) { _locale->SetLon(value, true); }
        double get() { return _locale->Lon(); }
    }

    property double LonRad
    {
        void set(double value) { _locale->SetLon(value, false); }
        double get() { return _locale->LonRad(); }
    }

    property double Elevation
    {
        void set(double meters) { _locale->SetElev(meters); }
        double get() { return _locale->Elev(); }
    }

    property int TimeZone
    {
        void set(int zone) { _locale->SetTimeZone(zone); }
        int get() { return _locale->TimeZone(); }
    }

    static double ConvertDMSToDeg(double deg, double min, double seconds);

    static void ConvertDegToDMS(double degrees, double% deg, double% min, double% seconds);

protected:
    !Locale();

private:
    libcalendar::Locale* _locale;
};


public ref class Locations abstract 
{
public:
    static initonly Locale^ Urbana, ^Mecca, ^Jerusalem;

    static Locations()
    {
        Urbana = gcnew Locale(libcalendar::URBANA);
        Mecca = gcnew Locale(libcalendar::MECCA);
        Jerusalem = gcnew Locale(libcalendar::JERUSALEM);
    }
};


public enum class SeasonLongitudes
{
    Spring = 0,
    Summer = 90,
    Autumn = 180,
    Winter = 270
};

}

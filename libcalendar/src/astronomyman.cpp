
#include "astronomyman.h"

using namespace libcalendarman;


Locale::Locale()
{
    _locale = new libcalendar::Locale();        
}


Locale::Locale(double latDeg, double lonDeg, double elevMeters, int timeZone)
{
    _locale = new libcalendar::Locale(latDeg, lonDeg, elevMeters, timeZone);
}


Locale::Locale(const libcalendar::Locale& l)
{
    _locale = new libcalendar::Locale(l);
}

Locale::~Locale()
{
    this->!Locale();
}


Locale::!Locale()
{
    if (_locale)
    {
        delete _locale;
        _locale = 0;
    }
}



double Locale::ConvertDMSToDeg(double deg, double min, double seconds)
{
    return libcalendar::Locale::ConvertDMSToDeg(deg, min, seconds);
}


void Locale::ConvertDegToDMS(double degrees, double% deg, double% min, double% seconds)
{
    double _deg, _min, _seconds;
    libcalendar::Locale::ConvertDegToDMS(degrees, _deg, _min, _seconds);
    deg = _deg;
    min = _min;
    seconds = _seconds;
}



#include "islamicman.h"

using namespace libcalendarman;
using namespace System;

#include <vector>



IslamicDate::IslamicDate()
{
    id = new libcalendar::IslamicDate();
}

IslamicDate::IslamicDate(int fd)
{
    id = new libcalendar::IslamicDate(fd);
}


IslamicDate::IslamicDate(int year, int month, int day)
{
    id = new libcalendar::IslamicDate(year, month, day);
}


IslamicDate::!IslamicDate()
{
    if (id)
    {
        delete id;
        id = 0;
    }

}

IslamicDate::~IslamicDate()
{
    this->!IslamicDate();
}


void IslamicDate::FromFixed(int fixed)
{
    id->FromFixed(fixed);
}

int IslamicDate::ToFixed()
{
    return id->ToFixed();
}

String^ IslamicDate::ToString() 
{
    return gcnew String(id->ToString().c_str());
}

bool IslamicDate::IsLeapYear(int year)
{
    return libcalendar::IslamicDate::IsLeapYear(year);
}
	
array<int>^ IslamicDate::GetIslamicInGregorian(int gregorianYear, int islamicMonth, int islamicDay)
{
    std::vector<int> results;

    libcalendar::IslamicDate::GetIslamicInGregorian(results, gregorianYear, islamicMonth, islamicDay);

    array<int>^ returnme = gcnew array<int>( (int) results.size() );

    for (int i=0; i< (int) results.size(); i++)
    {
        returnme[i] = results[i];
    }

    return returnme;
}




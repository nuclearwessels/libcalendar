
#include "fixeddate.h"
#include "fixeddateman.h"

#include <vector>

//using namespace libcalendar;
using namespace libcalendarman;



libcalendarman::FixedDate::operator int (FixedDate^ fd)
{
    return fd->ToFixed();
}


libcalendarman::Weekday DateUtils::GetDayOfWeek(int fd)
{
    return (libcalendarman::Weekday) libcalendar::GetDayOfWeek(fd);
}


int DateUtils::WeekdayOnOrBefore(int fd, libcalendarman::Weekday weekday)
{
    return libcalendar::WeekdayOnOrBefore(fd, (libcalendar::Weekday) weekday);
}

int DateUtils::WeekdayOnOrAfter(int fd, libcalendarman::Weekday weekday)
{
    return libcalendar::WeekdayOnOrAfter(fd, (libcalendar::Weekday) weekday);
}

int DateUtils::WeekdayNearest(int fd, libcalendarman::Weekday weekday)
{
    return libcalendar::WeekdayNearest(fd, (libcalendar::Weekday) weekday);
}

int DateUtils::WeekdayBefore(int fd, libcalendarman::Weekday weekday)
{
    return libcalendar::WeekdayBefore(fd, (libcalendar::Weekday) weekday);
}

int DateUtils::WeekdayAfter(int fd, libcalendarman::Weekday weekday)
{
    return libcalendar::WeekdayAfter(fd, (libcalendar::Weekday) weekday);
}

int DateUtils::NthWeekday(int fd, int n, libcalendarman::Weekday weekday)
{
    return libcalendar::NthWeekday(fd, n, (libcalendar::Weekday) weekday);
}

int DateUtils::FirstWeekday(int fd, libcalendarman::Weekday weekday)
{
    return libcalendar::FirstWeekday(fd, (libcalendar::Weekday) weekday);
}

int DateUtils::LastWeekday(int fd, libcalendarman::Weekday weekday)
{
    return libcalendar::LastWeekday(fd, (libcalendar::Weekday) weekday);
}

int DateUtils::Floor(int a, int b)
{
    return libcalendar::Floor(a, b);
}


long long DateUtils::Floor(long long a, long long b)
{
    return libcalendar::Floor(a, b);
}
	
double DateUtils::Mod(double x, double y)
{
    return libcalendar::Mod(x, y);
}

int DateUtils::Mod(int x, int y)
{
    return libcalendar::Mod(x, y);
}

int DateUtils::Amod(int a, int b)
{
    return libcalendar::Amod(a, b);
}
	
int DateUtils::Quotient(double x, double y)
{
    return libcalendar::Quotient(x, y);
}



array<int>^ Convert::ConvertVectorToArray(const std::vector<int>& v)
{
    array<int>^ a = gcnew array<int>( (int) v.size() );

    for (int i=0; i< (int) v.size(); i++)
    {
        a[i] = v[i];
    }

    return a;
}
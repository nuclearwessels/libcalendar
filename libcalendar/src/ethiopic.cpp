
#include "ethiopic.h"
#include "coptic.h"
#include "gregorian.h"
#include "julian.h"

using namespace libcalendar;
using namespace std;


const int EthiopicDate::EPOCH = 2796;

const string EthiopicDate::MONTHS[13] = { "Maskaram", "Teqemt", "Hedar", "Takhsas",
	"Ter", "Yakatit", "Magabit", "Miyazya", "Genbot", "Sane", "Hamle",
	"Nahase", "Paguemen" };

const string EthiopicDate::DAYS_OF_WEEK[7] = { "Ihud", "Sanyo", "Maksanyo",
	"Rob/Rabu`e", "Hamus", "Arb", "Kidamme" };


EthiopicDate::EthiopicDate()
{
	FromFixed(EPOCH);
}


EthiopicDate::EthiopicDate(int fixed)
{
	FromFixed(fixed);
}


EthiopicDate::EthiopicDate(int year, int month, int day) : _year(year), _month(month), _day(day)
{

}


void EthiopicDate::SetYear(int year)
{
	_year = year;
}


int EthiopicDate::Year() const
{
	return _year;
}


void EthiopicDate::SetMonth(int month)
{
	assert( month > 0  && month <= 13);
	_month = month;
}


int EthiopicDate::Month() const
{
	return _month;
}


void EthiopicDate::SetDay(int day)
{
	assert( _day >= 0 && _day <= ( (_month < 13) ? 30 : ( IsLeapYear(_year) ? 6 : 5) ) );
	_day = day;
}


int EthiopicDate::Day() const
{
	return _day;
}


void EthiopicDate::FromFixed(int fixed)
{
	CopticDate cc(fixed + CopticDate::EPOCH - EPOCH);
	_year = cc.Year();
	_month = cc.Month();
	_day = cc.Day();
}


int EthiopicDate::ToFixed() const
{
	return EPOCH + CopticDate(_year, _month, _day).ToFixed() - CopticDate::EPOCH;
}


string EthiopicDate::ToString() const
{
	char temp[100];

	snprintf(temp, 100, "%s(%d) %d, %d E.E.", MONTHS[_month-1].c_str(), _month,
		_day, _year );

	return string(temp);
}


void EthiopicDate::GetEthiopicInGregorian(vector<int>& results, int gregorianYear,
	int ethiopicMonth, int ethiopicDay)
{
    results.clear();
	
	int jan1 = GregorianDate(gregorianYear, 1, 1).ToFixed();
	int dec31 = GregorianDate(gregorianYear, 12, 31).ToFixed();

	int y = EthiopicDate(jan1).Year();

	int date1 = EthiopicDate(y, ethiopicMonth, ethiopicDay).ToFixed();
	int date2 = EthiopicDate(y+1, ethiopicMonth, ethiopicDay).ToFixed();

	if (date1 >= jan1  &&  date1<=dec31)
	{
		results.push_back(date1);
	}

	if (date2>=jan1  &&  date2<=dec31)
	{
		results.push_back(date2);
	}
}


void EthiopicDate::GetEthiopicChristmas(vector<int>& results, int gregorianYear)
{
	return GetEthiopicInGregorian(results, gregorianYear, 4, 29);
}



bool EthiopicDate::IsLeapYear(int ethiopicYear)
{
	return (Mod(ethiopicYear, 4) == 3) ? true : false;
}


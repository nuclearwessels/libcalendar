
#pragma once

#include "fixeddateman.h"
#include "balinesepawukon.h"
#include "julianman.h"

using namespace System;

namespace libcalendarman
{

public ref class BalinesePawukonDate : FixedDate
{
public:
    static initonly int Epoch;

    BalinesePawukonDate();
    BalinesePawukonDate(int fd);
    BalinesePawukonDate(bool luang, int dwiwara, int triwara, int caturwara, int pancawara,
                        int sadwara, int saptawara, int asatawara, int sangawara, int dasawara);
    ~BalinesePawukonDate();

    static BalinesePawukonDate()
    {
        JulianDate^ jd = gcnew JulianDate(146.0L, false);
        Epoch = jd->ToFixed();
    }

    property bool BaliLuang
    {
        bool get() { return _bpd->BaliLuang(); }
    }

    property int BaliDwiwara
    {
        int get() { return _bpd->BaliDwiwara(); }
    }

    property int BaliTriwara
    {
        int get() { return _bpd->BaliTriwara(); }
    }

    property int BaliCaturwara
    {
        int get() { return _bpd->BaliCaturwara(); }
    }

    property int BaliPancawara
    {
        int get() { return _bpd->BaliPancawara(); }
    }

    property int BaliSadwara
    {
        int get() { return _bpd->BaliSadwara(); }
    }
    property int BaliSaptawara
    {
        int get() { return _bpd->BaliSaptawara(); }
    }
    property int BaliAsatawara
    {
        int get() { return _bpd->BaliAsatawara(); }
    }

    property int BaliSangawara
    {
        int get() { return _bpd->BaliSangawara(); }
    }

    property int BaliDasawara
    {
        int get() { return _bpd->BaliDasawara(); }
    }

    virtual int ToFixed() override;
    virtual void FromFixed(int fd) override;

    virtual String^ ToString() override;

    static int GetBaliDayFromFixed(int fd);    
    static int GetBaliTriwaraFromFixed(int fd);
    static int GetBaliSadwaraFromFixed(int fd);
    static int GetBaliSaptawaraFromFixed(int fd);
    static int GetBaliPancawaraFromFixed(int fd);
    static int GetBaliWeekFromFixed(int fd);
    static int GetBaliDasawaraFromFixed(int fd);
    static int GetBaliDwiwaraFromFixed(int fd);
    static bool GetBaliLuangFromFixed(int fd);
    static int GetBaliSangawaraFromFixed(int fd);
    static int GetBaliAsatawaraFromFixed(int fd);
    static int GetBaliCaturawaFromFixed(int fd);
    static int GetBaliOnOrBefore(BalinesePawukonDate^ bpd, int fd);
    static array<int>^ GetKajengKeliwonInGregorian(int gregorianYear);
    static array<int>^ GetTumpekInGregorian(int gregorianYear);

protected:
    !BalinesePawukonDate();

private:
    libcalendar::BalinesePawukonDate* _bpd;
};



}   // namespace libcalendar


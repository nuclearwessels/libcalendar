

#include "isoman.h"

using namespace libcalendarman;
using namespace System;


ISODate::ISODate()
{
    id = new libcalendar::ISODate();
}

ISODate::ISODate(int fixed)
{
    id = new libcalendar::ISODate(fixed);
}

ISODate::ISODate(int year, int week, int day)
{
    id = new libcalendar::ISODate(year, week, day);
}


ISODate::!ISODate()
{
    if (id)
    {
        delete id;
        id = 0;
    }
}

ISODate::~ISODate()
{
    this->!ISODate();
}


void ISODate::FromFixed(int fixed)
{
    id->FromFixed(fixed);
}

int ISODate::ToFixed() 
{
    return id->ToFixed();
}


String^ ISODate::ToString()
{
    return gcnew String( id->ToString().c_str() );
}

//const int ISODate::Epoch = libcalendar::ISODate::EPOCH;



#ifndef ETHIOPIC_H
#define ETHIOPIC_H

#include "fixeddate.h"

namespace libcalendar
{

class EthiopicDate : public FixedDate
{
public:
	EthiopicDate();
	EthiopicDate(int fd);
	EthiopicDate(int year, int month, int day);

	void FromFixed(int fd);
	int ToFixed() const;

	void SetYear(int year);
	int Year() const;

	void SetMonth(int month);
	int Month() const;

	void SetDay(int day);
	int Day() const;

	std::string ToString() const;

	static bool IsLeapYear(int year);

	static void GetEthiopicInGregorian(std::vector<int>& results, int gregorianYear,
		                               int copticMonth, int copticDay);

	static void GetEthiopicChristmas(std::vector<int>& results, int gregorianYear);

	static const int EPOCH;

	static const std::string MONTHS[13];
	static const std::string DAYS_OF_WEEK[7];

private:
	int _year, _month, _day;

};

}

#endif

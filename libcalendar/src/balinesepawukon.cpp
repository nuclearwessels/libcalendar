
#include "balinesepawukon.h"

#include "gregorian.h"

#include <algorithm>

using namespace libcalendar;
using namespace std;


const int BalinesePawukonDate::EPOCH = JulianDate(146.0L, false).ToFixed();

const int BalinesePawukonDate::FIVE_DAY_URIPS[] = { 5, 9, 7, 4, 8 };
const int BalinesePawukonDate::SEVEN_DAY_URIPS[] = { 5, 4, 3, 7, 8, 6, 9 };



BalinesePawukonDate::BalinesePawukonDate()
{
    FromFixed(EPOCH);
}

BalinesePawukonDate::BalinesePawukonDate(int fd)
{
    FromFixed(fd);
}

BalinesePawukonDate::BalinesePawukonDate(bool luang, int dwiwara, int triwara, int caturwara, int pancawara,
                                         int sadwara, int saptawara, int asatawara, int sangawara, int dasawara)  :
    _baliLuang(luang),
    _baliDwiwara(dwiwara),
    _baliTriwara(triwara),
    _baliCaturwara(caturwara),
    _baliPancawara(pancawara),
    _baliSadwara(sadwara),
    _baliSaptawara(saptawara),
    _baliAsatawara(asatawara),
    _baliSangawara(sangawara),
    _baliDasawara(dasawara)
{

}   



bool BalinesePawukonDate::BaliLuang() const
{
    return _baliLuang;
}

int BalinesePawukonDate::BaliDwiwara() const
{
    return _baliDwiwara;
}

int BalinesePawukonDate::BaliTriwara() const
{
    return _baliTriwara;
}

int BalinesePawukonDate::BaliCaturwara() const
{
    return _baliCaturwara;
}

int BalinesePawukonDate::BaliPancawara() const
{
    return _baliPancawara;
}

int BalinesePawukonDate::BaliSadwara() const
{
    return _baliSadwara;
}

int BalinesePawukonDate::BaliSaptawara() const
{
    return _baliSaptawara;
}

int BalinesePawukonDate::BaliAsatawara() const
{
    return _baliAsatawara;
}

int BalinesePawukonDate::BaliSangawara() const
{
    return _baliSangawara;
}

int BalinesePawukonDate::BaliDasawara() const
{
    return _baliDasawara;
}


int BalinesePawukonDate::ToFixed() const
{
    throw logic_error("Invalid operation");
    return 0;
}


void BalinesePawukonDate::FromFixed(int fd)
{
    _baliLuang = GetBaliLuangFromFixed(fd);
    _baliDwiwara = GetBaliDwiwaraFromFixed(fd);
    _baliTriwara = GetBaliTriwaraFromFixed(fd);
    _baliCaturwara = GetBaliCaturawaFromFixed(fd);
    _baliPancawara = GetBaliPancawaraFromFixed(fd);
    _baliSadwara = GetBaliSadwaraFromFixed(fd);
    _baliSaptawara = GetBaliSaptawaraFromFixed(fd);
    _baliAsatawara = GetBaliAsatawaraFromFixed(fd);
    _baliSangawara = GetBaliSangawaraFromFixed(fd);
    _baliDasawara = GetBaliDasawaraFromFixed(fd);
}


std::string BalinesePawukonDate::ToString() const
{
    char temp[100];
    snprintf(temp, 100, "Luang=%s, Dwiwara=%d, Triwara=%d, Caturwara=%d, Pancawara=%d, Sadwara=%d, "
                        "Saptawara=%d, Asatawara=%d, Sangawara=%d, Dasawara=%d",
        _baliLuang ? "t" : "f",
        _baliDwiwara,
        _baliTriwara,
        _baliCaturwara,
        _baliPancawara,
        _baliSadwara,
        _baliSaptawara,
        _baliAsatawara,
        _baliSangawara,
        _baliDasawara);
    return string(temp);
}


int BalinesePawukonDate::GetBaliDayFromFixed(int fd)
{
    //JulianDate jd;
    //jd.SetJulianDay(146);

    //int jdfd = jd.ToFixed();

    //int epoch = EPOCH;
    return Mod(fd - EPOCH, 210);
}


int BalinesePawukonDate::GetBaliTriwaraFromFixed(int fd)
{
    //return Mod(fd - EPOCH, 3) + 1;
    return Mod(GetBaliDayFromFixed(fd), 3) + 1;
}


int BalinesePawukonDate::GetBaliSadwaraFromFixed(int fd)
{
    return Mod(GetBaliDayFromFixed(fd), 6) + 1;
}


int BalinesePawukonDate::GetBaliSaptawaraFromFixed(int fd)
{
    return Mod(GetBaliDayFromFixed(fd), 7) + 1;
}


int BalinesePawukonDate::GetBaliPancawaraFromFixed(int fd)
{
    return Mod(GetBaliDayFromFixed(fd) + 1, 5) + 1;
}


int BalinesePawukonDate::GetBaliWeekFromFixed(int fd)
{
    return Floor(GetBaliDayFromFixed(fd), 7) + 1;
}


int BalinesePawukonDate::GetBaliDasawaraFromFixed(int fd)
{
    int i = GetBaliPancawaraFromFixed(fd) - 1;
    int j = GetBaliSaptawaraFromFixed(fd) - 1;
    return Mod(FIVE_DAY_URIPS[i] + SEVEN_DAY_URIPS[j] + 1, 10);
}


int BalinesePawukonDate::GetBaliDwiwaraFromFixed(int fd)
{
    return Amod(GetBaliDasawaraFromFixed(fd), 2);
}


bool BalinesePawukonDate::GetBaliLuangFromFixed(int fd)
{
    return Mod(GetBaliDasawaraFromFixed(fd), 2) == 0;
}


int BalinesePawukonDate::GetBaliSangawaraFromFixed(int fd)
{
    return Mod( max(0, GetBaliDayFromFixed(fd) - 3), 9) + 1;
}


int BalinesePawukonDate::GetBaliAsatawaraFromFixed(int fd)
{
    int day = GetBaliDayFromFixed(fd);
    return Mod( max(6, 4 + Mod(day-70, 210)), 8) + 1;
}
    

int BalinesePawukonDate::GetBaliCaturawaFromFixed(int fd)
{
    return Amod(GetBaliAsatawaraFromFixed(fd), 4);
}


int BalinesePawukonDate::GetBaliOnOrBefore(const BalinesePawukonDate& bpd, int fd)
{
    int a5 = bpd.BaliPancawara() - 1;
    int a6 = bpd.BaliSadwara() - 1;
    int b7 = bpd.BaliSaptawara() - 1;
    int b35 = Mod(a5 + 14 + 15 * (b7 - a5), 35);
    int days = a6 + 36 * (b35 - a6);
    int delta = GetBaliDayFromFixed(0);

    return fd - Mod(fd + delta - days, 210);
}


void BalinesePawukonDate::GetKajengKeliwonInGregorian(std::vector<int>& results, int gregorianYear)
{
    GregorianDate jan1(gregorianYear, 1, 1);
    GregorianDate dec31(gregorianYear, 12, 31);
    int delta = GetBaliDayFromFixed(0);

    return GetPositionsInInterval(results, 9, 15, delta, jan1, dec31);

}


void BalinesePawukonDate::GetTumpekInGregorian(std::vector<int>& results, int gregorianYear)
{
    GregorianDate jan1(gregorianYear, 1, 1);
    GregorianDate dec31(gregorianYear, 12, 31);
    int delta = GetBaliDayFromFixed(0);

    return GetPositionsInInterval(results, 14, 35, delta, jan1, dec31 );
}


void BalinesePawukonDate::GetPositionsInInterval(std::vector<int>& results, int n, int c, int delta, int start, int end)
{
    int pos = start + Mod(n - start - delta - 1, c);

    if (pos > end)
    {
        // nothing to do
    }
    else
    {
        results.push_back(pos);
        vector<int> v2;
        GetPositionsInInterval(v2, n, c, delta, pos + 1, end);
    }
}



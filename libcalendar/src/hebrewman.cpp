

#include "hebrewman.h"

#include <vector>

using namespace libcalendarman;
using namespace System;



HebrewDate::HebrewDate()
{
    _hd = new libcalendar::HebrewDate();
}


HebrewDate::HebrewDate(int fixed)
{
    _hd = new libcalendar::HebrewDate(fixed);
}


HebrewDate::HebrewDate(int year, int month, int day)
{
    _hd = new libcalendar::HebrewDate(year, month, day);
}


HebrewDate::!HebrewDate()
{
    if (_hd)
    {
        delete _hd;
        _hd = 0;
    }
}


HebrewDate::~HebrewDate()
{
    this->!HebrewDate();
}


void HebrewDate::FromFixed(int fd)
{
    _hd->FromFixed(fd);
}


int HebrewDate::ToFixed()
{
    return _hd->ToFixed();
}


System::String^ HebrewDate::ToString()
{
    return gcnew String(_hd->ToString().c_str());
}
	
String^ HebrewDate::GetMonthName(int hebrewMonth, bool leapYear)
{
    return gcnew String(libcalendar::HebrewDate::GetMonthName(hebrewMonth, leapYear).c_str());
}

bool HebrewDate::IsLeapYear(int year)
{
    return libcalendar::HebrewDate::IsLeapYear(year);
}


int HebrewDate::GetLastMonthOfYear(int year)
{
    return libcalendar::HebrewDate::GetLastMonthOfYear(year);
}

double HebrewDate::GetMolad(int hebrewMonth, int hebrewYear)
{
    return libcalendar::HebrewDate::GetMolad(hebrewMonth, hebrewYear);
}


int HebrewDate::GetElapsedDays(int hebrewYear)
{
    return libcalendar::HebrewDate::GetElapsedDays(hebrewYear);
}


int HebrewDate::GetNewYearDelay(int hebrewYear)
{
    return libcalendar::HebrewDate::GetNewYearDelay(hebrewYear);
}


int HebrewDate::GetNewYear(int hebrewYear)
{
    return libcalendar::HebrewDate::GetNewYear(hebrewYear);
}


int HebrewDate::GetLastDayOfMonth(int hebrewMonth, int hebrewYear)
{
    return libcalendar::HebrewDate::GetLastDayOfMonth(hebrewMonth, hebrewYear);
}


bool HebrewDate::IsLongMarheshvan(int hebrewYear)
{
    return libcalendar::HebrewDate::IsLongMarheshvan(hebrewYear);
}

bool HebrewDate::IsShortKislev(int hebrewYear)
{
    return libcalendar::HebrewDate::IsShortKislev(hebrewYear);
}

int HebrewDate::GetDaysInYear(int hebrewYear)
{
    return libcalendar::HebrewDate::GetDaysInYear(hebrewYear);
}

	
int HebrewDate::GetYomKippur(int gregorianYear)
{
    return libcalendar::HebrewDate::GetYomKippur(gregorianYear);
}


int HebrewDate::GetRoshHaShanah(int gregorianYear)
{
    return libcalendar::HebrewDate::GetRoshHaShanah(gregorianYear);
}

int HebrewDate::GetSukkot(int gregorianYear)
{
    return libcalendar::HebrewDate::GetSukkot(gregorianYear);
}

int HebrewDate::GetHoshanaRabba(int gregorianYear)
{
    return libcalendar::HebrewDate::GetHoshanaRabba(gregorianYear);
}

int HebrewDate::GetSheminiAzeret(int gregorianYear)
{
    return libcalendar::HebrewDate::GetSheminiAzeret(gregorianYear);
}

int HebrewDate::GetSimhatTorah(int gregorianYear)
{
    return libcalendar::HebrewDate::GetSimhatTorah(gregorianYear);
}

int HebrewDate::GetPassover(int gregorianYear)
{
    return libcalendar::HebrewDate::GetPassover(gregorianYear);
}


bool HebrewDate::GetOmer(int fd, int% completedWeeks, int% excessDays)
{
    int cw, ed;
    bool ok = libcalendar::HebrewDate::GetOmer(fd, cw, ed);
    completedWeeks = cw;
    excessDays = ed;
    return ok;
}


int HebrewDate::GetShavuot(int gregorianYear)
{
    return libcalendar::HebrewDate::GetShavuot(gregorianYear);
}

int HebrewDate::GetPurim(int gregorianYear)
{
    return libcalendar::HebrewDate::GetPurim(gregorianYear);
}


int HebrewDate::GetTaAnitEsther(int gregorianYear)
{
    return libcalendar::HebrewDate::GetTaAnitEsther(gregorianYear);
}

int HebrewDate::GetTishahBeAv(int gregorianYear)
{
    return libcalendar::HebrewDate::GetTishahBeAv(gregorianYear);
}

int HebrewDate::GetYomHaZikkaron(int gregorianYear)
{
    return libcalendar::HebrewDate::GetYomHaZikkaron(gregorianYear);
}

int HebrewDate::GetShEla(int gregorianYear)
{
    return libcalendar::HebrewDate::GetShEla(gregorianYear);
}

array<int>^ HebrewDate::GetBirkathHaHama(int gregorianYear)
{
    std::vector<int> v;
    libcalendar::HebrewDate::GetBirkathHaHama(v, gregorianYear);
    return Convert::ConvertVectorToArray(v);
}
	
int HebrewDate::GetHebrewBirthday(int birthdate, int hebrewYear)
{
    return libcalendar::HebrewDate::GetHebrewBirthday(birthdate, hebrewYear);
}

array<int>^ HebrewDate::GetHebrewBirthdayInGregorian(int birthdate, int gregorianYear)
{
    std::vector<int> v;
    libcalendar::HebrewDate::GetHebrewBirthdayInGregorian(v, birthdate, gregorianYear);
    return Convert::ConvertVectorToArray(v);
}
	
int HebrewDate::GetYahrzeit(int deathdate, int hebrewYear)
{
    return libcalendar::HebrewDate::GetYahrzeit(deathdate, hebrewYear);
}


array<int>^ HebrewDate::GetYahrzeitInGregorian(int deathdate, int gregorianYear)
{
    std::vector<int> v;
    libcalendar::HebrewDate::GetYahrzeitInGregorian(v, deathdate, gregorianYear);
    return Convert::ConvertVectorToArray(v);
}


FixedDate^ HebrewDate::CreateHebrewDate(int fd)
{
    return gcnew HebrewDate(fd);
}


array<Event^>^ HebrewDate::GetHebrewEvents(int gyear)
{
    List<Event^>^ events = gcnew List<Event^>();

    for each (EventType^ et in HebrewEventTypes)
    {
        et->AddEvent(events, gyear);
    }

    return events->ToArray();
}

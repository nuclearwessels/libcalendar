

#pragma once

#include "fixeddate.h"
#include <string>

namespace libcalendar
{

class ArmenianDate : public FixedDate
{
public:
    ArmenianDate();
    ArmenianDate(int fd);
    ArmenianDate(int year, int month, int day);

    void SetYear(int year);
    int Year() const;

    void SetMonth(int month);
    int Month() const;

    void SetDay(int day);
    int Day() const;

    void FromFixed(int fd);
    int ToFixed() const;
    std::string ToString() const;

    static const int EPOCH = 201443;

    static const std::string MONTHS[12];

private:
    int _year, _month, _day;
};


}

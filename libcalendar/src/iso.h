

#pragma once

#include "fixeddate.h"

namespace libcalendar
{

class ISODate : public FixedDate
{
public:
	ISODate();
	ISODate(int fixed);
	ISODate(int year, int week, int day);

	void FromFixed(int fixed);
	int ToFixed() const;

	void SetYear(int year);
	int Year() const;

	void SetWeek(int week);
	int Week() const;

	void SetDay(int day);
	int Day() const;

	std::string ToString() const;

	static const int EPOCH;

private:
	int year_, week_, day_;

    void ValidateDate() const;
};


}


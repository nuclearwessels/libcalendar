

#include "fixeddate.h"


#ifdef _WIN32
#define snprintf _snprintf
#endif

using namespace libcalendar;
using namespace std;



Weekday libcalendar::GetDayOfWeek(int fd)
{
	//return (weekday_t) (toFixed() % 7);
	return (Weekday) (Mod( (int) fd, (int) 7));
}


FixedDate::operator int() const
{
	return ToFixed();
}


int libcalendar::WeekdayOnOrBefore(int fd, Weekday weekday)
{
	int returnme = fd - (int) GetDayOfWeek( fd - (int) weekday);
	return returnme;
}


int libcalendar::WeekdayOnOrAfter(int fd, Weekday weekday)
{
	return WeekdayOnOrBefore(fd + 6, weekday);
}


int libcalendar::WeekdayNearest(int fd, Weekday weekday)
{
	return WeekdayOnOrBefore(fd + 3, weekday);
}

int libcalendar::WeekdayBefore(int fd, Weekday weekday)
{
	return WeekdayOnOrBefore(fd - 1, weekday);
}


int libcalendar::WeekdayAfter(int fd, Weekday weekday)
{
	return WeekdayOnOrBefore(fd + 7, weekday);
}


int libcalendar::NthWeekday(int fd, int n, Weekday weekday)
{
	if (n>0)
	{
		return 7 * n + WeekdayBefore(fd, weekday);
	}
	else
	{
		return 7 * n + WeekdayAfter(fd, weekday);
	}
}


int libcalendar::FirstWeekday(int fd, Weekday weekday)
{
	return NthWeekday(fd, 1, weekday);
}


int libcalendar::LastWeekday(int fd, Weekday weekday)
{
	return NthWeekday(fd, -1, weekday);
}


int libcalendar::Floor(int a, int b)
{
	int returnme = 0;

	int c = a / b;

	if ( a < 0)
	{
		returnme = (a % b) ? (c - 1) : c;
	}
	else
	{
		returnme = c;
	}

// 	int floatFloor = (int) ::floor( (double) a / (double) b);
// 	assert( returnme == floatFloor);

	return returnme;
}


long long libcalendar::Floor(long long a, long long b)
{
	long long returnme = 0;
	
	long long c = a / b;

	if ( a < 0)
	{
		returnme = (a % b) ? (c - 1) : c;
	}
	else
	{
		returnme = c;
	}

// 	long long floatFloor = (long long) ::floor( (double) a / (double) b);
// 	assert( returnme == floatFloor);

	return returnme;
}


double libcalendar::Mod(double x, double y) 
{
	return x - y * ::floor(x / y);
}

int libcalendar::Mod(int x, int y) 
{
    double f = ::floor( (double) x / (double) y);
    double m1 = (int)(x - y * f);
    //return m;

    double m2 = fmod( (double) x, (double) y);
    if (m2 < 0.0L)
    {
        m2 += y;
    }
    return (int) m2;
}


int libcalendar::Amod(int x, int y)
{
	int r = Mod( (int) x, (int) y);
	return (r == 0) ? (y) : r;
}


int libcalendar::Quotient(double x, double y)
{
	return (int) ::floor(x/y);
}


double libcalendar::Round(double x)
{
    return floor( x + 0.5 );
}


double libcalendar::Arctan(double x, int quad)
{
    double alpha = atan(x) * 180.0 / M_PI;

    switch (quad)
    {
    case 1:
    case 4:
        return alpha;
        break;

    default:
        return alpha + 180.0;
    }
}


//// *******************************************************************************
//// *******************************************************************************
//// *******************************************************************************
//
//
//const int OldHinduDate::epoch = JulianDate(-3102, 2, 18).toFixed();
//
//int OldHinduDate::getHinduDayCount(const FixedDate& c)
//{
//	return c.toFixed() - epoch;
//}
//

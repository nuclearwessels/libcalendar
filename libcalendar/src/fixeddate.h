

#ifndef CALENDAR_H
#define CALENDAR_H

#define _USE_MATH_DEFINES
#include <cmath>
#include <cstdlib>
#include <cassert>
#include <string>
#include <vector>


namespace libcalendar
{

enum Weekday { SUNDAY = 0, MONDAY, TUESDAY, WEDNESDAY, 
		       THURSDAY, FRIDAY, SATURDAY };

enum Months { JANUARY = 1, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER };

class FixedDate
{
public:
    virtual int ToFixed() const = 0;
    virtual void FromFixed(int fd) = 0;

    virtual std::string ToString() const = 0;

    operator int () const;
};



Weekday GetDayOfWeek(int fd);


int WeekdayOnOrBefore(int fd, Weekday weekday);
int WeekdayOnOrAfter(int fd, Weekday weekday);
int WeekdayNearest(int fd, Weekday weekday);
int WeekdayBefore(int fd, Weekday weekday);
int WeekdayAfter(int fd, Weekday weekday);

int NthWeekday(int fd, int n, Weekday weekday);
int FirstWeekday(int fd, Weekday weekday);
int LastWeekday(int fd, Weekday weekday);

int Floor(int a, int b);
long long Floor(long long a, long long b);
	
double Mod(double x, double y);
int Mod(int x, int y);

int Amod(int a, int b);
	
int Quotient(double x, double y);


double Round(double x);
double Arctan(double a, int quad);


}

#endif


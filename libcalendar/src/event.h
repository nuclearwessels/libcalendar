

#pragma once

#include <string>
#include <vector>
#include <functional>

#include "fixeddate.h"

namespace libcalendar
{

class Event
{
public:
    Event(const std::string& category, const std::string& name, int fd);
    Event(const std::string& category, const std::string& name, const std::vector<int>& fds);

    const std::string& name() const;
    const std::string& category() const;

    const std::vector<int>& fixedDates() const;

private:
    std::string name_;
    std::string category_;

    std::vector<int> fds_;
};


}


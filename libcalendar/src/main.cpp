
#include "calendar.h"

#include <cstdio>
#include <iostream>
using namespace std;


void test(int fixed)
{
	printf("FixedDate: %s\n", FixedDate(fixed).toString().c_str());
	printf("Gregorian: %s\n", GregorianDate(fixed).toString().c_str());
	printf("Julian: %s\n", JulianDate(fixed).toString().c_str());
	printf("Roman: %s\n", RomanDate(fixed).toString().c_str());
	printf("Coptic: %s\n", CopticDate(fixed).toString().c_str());
	printf("Ethiopic: %s\n", EthiopicDate(fixed).toString().c_str());
	printf("ISO: %s\n", ISODate(fixed).toString().c_str());
	printf("Islamic: %s\n", IslamicDate(fixed).toString().c_str());
	printf("Hebrew: %s\n", HebrewDate(fixed).toString().c_str());
	printf("\n");
}


void printList(const datelist_t& l)
{
	datelist_t::const_iterator il;
	
	for (il=l.begin(); il != l.end(); il++)
	{
		printf("%d ", *il);
	}
}


int main()
{
	test(-214193);
	test(-61387);
	test(25469);

	for (unsigned int i=2008; i<2100; i++)
	{
		printf("%d %20s %20s\n", i, GregorianDate(GregorianDate::getEaster(i)).toString().c_str(), 
			GregorianDate(JulianDate::getOrthodoxEaster(i)).toString().c_str() );
	}

	return 0;
}


//for (int y=-1000; y<=3000; y++)
//{	
//	printf("%d: ", y);
//	vector<int>::const_iterator i;
//	vector<int> v = JulianDate::getEasternOrthodoxChristmas(y);
//	for (i=v.begin(); i!=v.end(); i++)
//	{
//		printf("%s ", GregorianDate(*i).toString().c_str());
//	}
//	printf("\n");
//}



#ifndef ROMAN_H
#define ROMAN_H


#include "fixeddate.h"


namespace libcalendar
{

//! A class to describe the time of the month according to Roman custom.
class RomanDate : public FixedDate
{
public:
    enum Events { KALENDS = 1, NONES = 2, IDES = 3 };

	RomanDate();
	RomanDate(int fixed);
	RomanDate(int year, int month, Events event, int count, bool leap);

	void SetYear(int year);
	int Year() const;

	void SetMonth(int month);
	int Month() const;

	void SetEvent(Events evt);
	Events Event() const;

	void SetCount(int count);
	int Count() const;

	void SetLeap(bool leap);
	bool IsLeap() const;

	int ToFixed() const;
	void FromFixed(int fd);

	std::string ToString() const;

	static int GetIdes(int month);
	static int GetNones(int month);

    static const std::string MONTHS[12];
	static const std::string EVENTS[3];
	static const std::string NUMERALS[19];

private:
	int _year;
	int _month;
	Events _event;
	int _count;
	bool _leap;

};

}

#endif


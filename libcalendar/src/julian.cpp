
#include "julian.h"
#include "gregorian.h"

using namespace libcalendar;
using namespace std;


const double JulianDate::JULIAN_DAY_EPOCH = -1721424.5f;
const double JulianDate::MODIFIED_JULIAN_DAY_EPOCH = 678576.0f;


const std::string JulianDate::MONTHS[12] = { "January", "February",
	"March", "April", "May", "June", "July", "August", "September", 
	"October", "November", "December" };

const string JulianDate::DAYS_OF_WEEK[7] = 
{ 
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
};       


const int JulianDate::EPOCH = GregorianDate(0, 12, 30).ToFixed();


std::vector<Event> JulianDate::GetOrthodoxEvents(int gregorianYear)
{
    std::vector<Event> events;

    std::vector<int> fds;
    GetEasternOrthodoxChristmas(fds, gregorianYear);

    events.emplace_back(Event("Christian Orthodox", "Orthodox Christmas", fds));
    events.emplace_back(Event("Christian Orthodox", "Orthodox Easter", GetOrthodoxEaster(gregorianYear)));

    return events;
}


JulianDate::JulianDate() 
{
	FromFixed(EPOCH);
}


JulianDate::JulianDate(int fd) 
{
	FromFixed(fd);
}


JulianDate::JulianDate(int year, int month, int day)
{
	_year = year;
	assert(year != 0);
	
	_month = month;
	assert( month>0 && month<=12);
	
	_day = day;
	assert( _day > 0);

    switch (month)
	{
	case 2:
		assert( _day<= (IsLeapYear(year) ? 29 : 28));
		break;	
	
	case 9:
	case 4:
	case 6:
	case 11:
		assert(_day<=30);
		break;
	
	default:
		assert(_day<=31);
		break;
	}
}


JulianDate::JulianDate(double jd, bool isModifiedJulianDay)
{
    if (isModifiedJulianDay)
    {
        SetModifiedJulianDay(jd);
    }
    else
    {
        SetJulianDay(jd);
    }
}


int JulianDate::ToFixed() const
{
	int y = (_year < 0) ? (_year + 1) : _year;
	
	int returnme = EPOCH - 1;
	returnme += 365 * (y - 1);
	returnme += Quotient( y - 1, 4);
	returnme += Quotient( 367 * _month - 362, 12);

	if (_month <= 2)
	{
		returnme += 0;
	}
	else if (IsLeapYear(_year))
	{
		returnme += -1;
	}
	else 
	{
		returnme += -2;
	}

	returnme += _day;

	return returnme;
}


void JulianDate::FromFixed(int fd)
{
	int approx = Quotient( 4 * (fd - EPOCH) + 1464, 1461);
	_year = (approx <= 0) ? (approx - 1) : approx;

	int priorDays = fd - JulianDate(_year, 1, 1).ToFixed();
	
	int correction;
	if (fd < JulianDate(_year, 3, 1).ToFixed())
	{
		correction = 0;
	}
	else if ( IsLeapYear(_year))
	{
		correction = 1;
	}
	else
	{
		correction = 2;
	}

	_month = Quotient( 12 * (priorDays + correction) + 373, 367);
	_day = fd - JulianDate(_year, _month, 1).ToFixed() + 1;
}


void JulianDate::SetYear(int year)
{
    assert( year != 0 );
	_year = year;
}


int JulianDate::Year() const
{
	return _year;
}


void JulianDate::SetMonth(int month)
{
	_month = month;
}


int JulianDate::Month() const
{
	return _month;
}


int JulianDate::NextMonth() const
{
    return ToFixed() + GetDaysInMonth(_year, _month);
}


int JulianDate::PreviousMonth() const
{
    int m = _month - 1;
    int y = _year;
    if (m < 1)
    {
        m = 12;
        y--;
    }
    return ToFixed() - GetDaysInMonth(y, m);
}



void JulianDate::SetDay(int day)
{
	_day = day;
}


int JulianDate::Day() const
{
	return _day;
}


void JulianDate::SetJulianDay(double jd)
{
    FromFixed( (int) ::floor(jd + JULIAN_DAY_EPOCH) );
}


double JulianDate::JulianDay() const
{
    return (double)ToFixed() - JULIAN_DAY_EPOCH;
}


void JulianDate::SetModifiedJulianDay(double mjd)
{
    FromFixed((int)(mjd + MODIFIED_JULIAN_DAY_EPOCH));
}


double JulianDate::ModifiedJulianDay() const
{
    return (double)ToFixed() - MODIFIED_JULIAN_DAY_EPOCH;
}


string JulianDate::ToString() const
{
	char temp[80];
	snprintf(temp, 80, "%s %d, %d %s", MONTHS[_month-1].c_str(),
		_day, abs(_year),
		( _year<0) ? "B.C.E." : "C.E." );

	return string(temp);
}


void JulianDate::GetJulianInGregorian( vector<int>& results, 
                                       int gregorianYear, 
                                       int julianMonth,
	                                   int julianDay)
{
	int jan1 = GregorianDate(gregorianYear, 1, 1).ToFixed();
	int dec31 = GregorianDate(gregorianYear, 12, 31).ToFixed();

	int y = JulianDate(jan1).Year();
	int yprime = (y == -1) ? 1 : (y+1);

	int date1 = JulianDate(y, julianMonth, julianDay).ToFixed();
	int date2 = JulianDate(yprime, julianMonth, julianDay).ToFixed();

	if ( date1 >= jan1 && date1 <= dec31)
	{
		results.push_back(date1);
	}
	
	if (date2 >= jan1 && date2 <= dec31)
	{
		results.push_back(date2);
	}
	
}


void JulianDate::GetEasternOrthodoxChristmas(vector<int>& results, int gy)
{
	return GetJulianInGregorian(results, gy, 12, 25);
}


int JulianDate::GetOrthodoxEaster(int gregorianYear)
{
	int shiftedEpact = Mod(14 + 11 * Mod(gregorianYear, 19), 30);
	int jyear = (gregorianYear > 0) ? gregorianYear : (gregorianYear-1);
	int paschalMoon = JulianDate(jyear, 4, 19).ToFixed() - shiftedEpact;
	return WeekdayAfter(paschalMoon, SUNDAY);
}


bool JulianDate::IsLeapYear(int year)
{
	return (Mod(year, 4) == ((year>0) ? 0 : 3));
}


int JulianDate::GetDaysInMonth(int jyear, int month)
{
    switch (month)
    {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
        return 31;

    case 2:
        return IsLeapYear(jyear) ? 29 : 28;

    case 4:
    case 6:
    case 9:
    case 11:
        return 30;

    default:
        assert(false);
        return 0;
    }
}

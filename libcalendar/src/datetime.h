

#pragma once

#include "astronomy.h"
#include "gregorian.h"

namespace libcalendar
{

const double SPRING = 0.0;
const double SUMMER = 90.0;
const double AUTUMN = 180.0;
const double WINTER = 270.0;
          


double UniversalFromLocal(double dt, const Locale& l);
double LocalFromUniversal(double dt, const Locale& l);
double StandardFromUniversal(double dt, const Locale& l);
double UniversalFromStandard(double dt, const Locale& l);
double StandardFromLocal(double dt, const Locale& l);
double LocalFromStandard(double dt, const Locale& l);
double DynamicalFromUniversal(double dt);
double UniversalFromDynamical(double dt);
double ApparentFromLocal(double dt);
double LocalFromApparent(double dt);

double EphemerisCorrection(double dt);
double JulianCenturies(double dt);

double EquationOfTime(double dt);
double Obliquity(double dt);
double Sidereal(double dt);

double Midnight(double dt, const Locale& l);
double Midday(double dt, const Locale& l);

double SolarLongitude(double dt);
double Nutation(double dt);
double Aberration(double dt);
double SolarLongitudeAfter(double dt, double phi);


const double J2000 = (double) GregorianDate(2000, JANUARY, 1) + 0.5;
const double MEAN_TROPICAL_YEAR = 365.242189;

const double MEAN_SYNODIC_MONTH = 29.530588853;

double NthNewMoon(int n);
double LunarLongitude(double dt);
double NewMoonAfter(double dt);
double NewMoonBefore(double dt);
double LunarPhase(double dt);
double LunarPhaseBefore(double dt, double phi);
double LunarPhaseAfter(double dt, double phi);
double LunarLatitude(double dt);
double LunarAltitude(double dt, const Locale& l);
double MomentFromDepression(double approx, const Locale& l, double alpha);
double Dawn(int fd, const Locale& locale, double alpha);
double Dusk(int fd, const Locale& locale, double alpha);
double Sunrise(int fd, const Locale& locale);
double Sunset(int fd, const Locale& locale);


const double DEG_TO_RAD = M_PI / 180.0;
const double RAD_TO_DEG = 180.0 / M_PI;

const double MOON_PHASE_NEW = 0.0;
const double MOON_PHASE_FULL = 180.0;
const double MOON_PHASE_FIRST_QUARTER = 90.0;
const double MOON_PHASE_LAST_QUARTER = 270.0;


}



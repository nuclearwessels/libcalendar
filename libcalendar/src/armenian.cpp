

#include "armenian.h"
#include "egyptian.h"

#include <string>
using namespace libcalendar;
using namespace std;


ArmenianDate::ArmenianDate()
{
    FromFixed(EPOCH);
}

ArmenianDate::ArmenianDate(int fd)
{
    FromFixed(fd);
}

ArmenianDate::ArmenianDate(int year, int month, int day) : _year(year),
                                                           _month(month),
                                                           _day(day)
{

}

void ArmenianDate::SetYear(int year)
{
    _year = year;
}


int ArmenianDate::Year() const
{
    return _year;
}

void ArmenianDate::SetMonth(int month)
{
    _month = month;
}

int ArmenianDate::Month() const
{
    return _month;
}

void ArmenianDate::SetDay(int day)
{
    _day = day;
}

int ArmenianDate::Day() const
{
    return _day;
}

void ArmenianDate::FromFixed(int fd)
{
    EgyptianDate ed (fd + EgyptianDate::EPOCH - EPOCH);
    _year = ed.Year();
    _month = ed.Month();
    _day = ed.Day();
}


int ArmenianDate::ToFixed() const
{
    EgyptianDate ed(_year, _month, _day);
    return EPOCH + ed.ToFixed() - EgyptianDate::EPOCH;
}


std::string ArmenianDate::ToString() const
{
    char temp[100];
    snprintf(temp, 100, "year %d month %d day %d", _year, _month, _day);
    return string(temp);
}


const string ArmenianDate::MONTHS[12] = 
{
    "Nawasardi", "Hori", "Sahmi", "Tre",
    "K'aloch", "Arach", "Mehekani", "Areg", "Ahekani", "Mareri", 
	"Margach", "Hrotich"
};

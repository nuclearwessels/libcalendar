

#include "julianman.h"

using namespace libcalendarman;

#include <vector>



JulianDate::JulianDate()
{
    _jd = new libcalendar::JulianDate();
}


JulianDate::JulianDate(int fd)
{
    _jd = new libcalendar::JulianDate(fd);
}

JulianDate::JulianDate(int year, int month, int day)
{
    _jd = new libcalendar::JulianDate(year, month, day);
}


JulianDate::JulianDate(double jd, bool isModifiedJulianDay)
{
    _jd = new libcalendar::JulianDate(jd, isModifiedJulianDay);
}


JulianDate::!JulianDate()
{
    if (_jd)
    {
        delete _jd;
        _jd = 0;
    }
}


JulianDate::~JulianDate()
{
    this->!JulianDate();
}



int JulianDate::ToFixed()
{
    return _jd->ToFixed();
}

void JulianDate::FromFixed(int fixed)
{
    _jd->FromFixed(fixed);
}

String^ JulianDate::ToString()
{
    return gcnew String(_jd->ToString().c_str());
}


bool JulianDate::IsLeapYear(int year)
{
    return libcalendar::JulianDate::IsLeapYear(year);
}


array<int>^ JulianDate::GetJulianInGregorian(int gregorianYear, int julianMonth, int julianDay)
{
    std::vector<int> results;

    libcalendar::JulianDate::GetJulianInGregorian(results, gregorianYear, julianMonth, julianDay);

    array<int>^ returnme = gcnew array<int>( (int) results.size());

    for (int i = 0; i < (int) results.size(); i++)
    {
        returnme[i] = results[i];
    }

    return returnme;
}


array<int>^ JulianDate::GetEasternOrthodoxChristmas(int gregorianYear)
{
    std::vector<int> results;
    libcalendar::JulianDate::GetEasternOrthodoxChristmas(results, gregorianYear);

    array<int>^ returnme = gcnew array<int>( (int) results.size());

    for (int i = 0; i < (int) results.size(); i++)
    {
        returnme[i] = results[i];
    }

    return returnme;
}


int JulianDate::GetOrthodoxEaster(int gregorianYear)
{
    return libcalendar::JulianDate::GetOrthodoxEaster(gregorianYear);
}


FixedDate^ JulianDate::CreateJulianDate(int fd)
{
    return gcnew JulianDate(fd);
}


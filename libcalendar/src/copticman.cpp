
#include "copticman.h"

#include <vector>

using namespace libcalendarman;
using namespace System;



CopticDate::CopticDate()
{
    _cd = new libcalendar::CopticDate();
}


CopticDate::CopticDate(int fd)
{
    _cd = new libcalendar::CopticDate(fd);
}


CopticDate::CopticDate(int year, int month, int day)
{
    _cd = new libcalendar::CopticDate(year, month, day);
}


CopticDate::!CopticDate()
{
    if (_cd)
    {
        delete _cd;
        _cd = 0;
    }
}

CopticDate::~CopticDate()
{
    this->!CopticDate();
}

void CopticDate::FromFixed(int fd) 
{
    _cd->FromFixed(fd);
}


int CopticDate::ToFixed() 
{
    return _cd->ToFixed();
}


String^ CopticDate::ToString() 
{
    return gcnew String(_cd->ToString().c_str());
}

bool CopticDate::IsLeapYear(int year)
{
    return libcalendar::CopticDate::IsLeapYear(year);
}

array<int>^ CopticDate::GetCopticInGregorian(int gregorianYear, int copticMonth, int copticDay)
{
    std::vector<int> v;
    libcalendar::CopticDate::GetCopticInGregorian(v, gregorianYear, copticMonth, copticDay);
    return Convert::ConvertVectorToArray(v);
}

array<int>^ CopticDate::GetCopticChristmas(int gregorianYear)
{
    std::vector<int> v;
    libcalendar::CopticDate::GetCopticChristmas(v, gregorianYear);
    return Convert::ConvertVectorToArray(v);
}

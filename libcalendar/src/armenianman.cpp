
#include "armenianman.h"

using namespace libcalendarman;
using namespace System;

ArmenianDate::ArmenianDate()
{
    _ad = new libcalendar::ArmenianDate();
}


ArmenianDate::ArmenianDate(int fd)
{
    _ad = new libcalendar::ArmenianDate(fd);
}


ArmenianDate::ArmenianDate(int year, int month, int day)
{
    _ad = new libcalendar::ArmenianDate(year, month, day);
}


ArmenianDate::!ArmenianDate()
{
    if (_ad)
    {
        delete _ad;
        _ad = 0;
    }
}


ArmenianDate::~ArmenianDate()
{
    this->!ArmenianDate();
}

void ArmenianDate::FromFixed(int fd) 
{
    _ad->FromFixed(fd);
}


int ArmenianDate::ToFixed() 
{
    return _ad->ToFixed();
}


String^ ArmenianDate::ToString() 
{
    return gcnew String( _ad->ToString().c_str() );
}



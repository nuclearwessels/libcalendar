

#include "egyptianman.h"

using namespace libcalendarman;


EgyptianDate::EgyptianDate()
{
    _ed = new libcalendar::EgyptianDate();
}


EgyptianDate::EgyptianDate(int fd)
{
    _ed = new libcalendar::EgyptianDate(fd);
}


EgyptianDate::EgyptianDate(int year, int month, int day)
{
    _ed = new libcalendar::EgyptianDate(year, month, day);
}


EgyptianDate::!EgyptianDate()
{
    if (_ed)
    {
        delete _ed;
        _ed = 0;
    }
}


EgyptianDate::~EgyptianDate()
{
    this->!EgyptianDate();
}

void EgyptianDate::FromFixed(int fd) 
{
    _ed->FromFixed(fd);
}


int EgyptianDate::ToFixed() 
{
    return _ed->ToFixed();
}


String^ EgyptianDate::ToString() 
{
    return gcnew String( _ed->ToString().c_str() );
}





#pragma once

#include "fixeddate.h"

namespace libcalendar
{
    
class EgyptianDate : public FixedDate
{
public:
    EgyptianDate();
    EgyptianDate(int fd);
    EgyptianDate(int year, int month, int day);

    void SetYear(int year);
    int Year() const;

    void SetMonth(int month);
    int Month() const;

    void SetDay(int day);
    int Day() const;

    void FromFixed(int fd);
    int ToFixed() const;
    std::string ToString() const;

    static const int EPOCH = -272787;

private:
    int _year, _month, _day;
};



}
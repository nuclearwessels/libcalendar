

#ifdef _MANAGED

#include "fixeddateman.h"
#include "roman.h"

using namespace System;

namespace libcalendarman
{

public ref class RomanDate : public FixedDate
{
public:
    enum class RomanEvents { Kalends = 1, Nones = 2, Ides = 3 };

	RomanDate();
	RomanDate(int fixed);
	RomanDate(int year, int month, RomanEvents event, int count, bool leap);
    !RomanDate();
    ~RomanDate();

    property int Year
    {
        int get() { return _rd->Year(); }
        void set(int year) { _rd->SetYear(year); }
    }


    property int Month
    {
        int get() { return _rd->Month(); }
        void set(int month) { _rd->SetMonth(month); }
    }

    
    property RomanEvents Event
    {
        RomanEvents get() { return (RomanEvents) _rd->Event(); }
        void set(RomanEvents evt) { _rd->SetEvent( (libcalendar::RomanDate::Events) evt); }
    }

    property int Count
    {
        int get() { return _rd->Count(); }
        void set(int count) { _rd->SetCount(count); }
    }

    property bool Leap
    {
        bool get() { return _rd->IsLeap(); }
        void set(bool leap) { _rd->SetLeap(leap); }
    }


	virtual int ToFixed() override;
	virtual void FromFixed(int fd) override;

	virtual String^ ToString() override;

	static int GetIdes(int month);
	static int GetNones(int month);

    static initonly array<String^>^ Months;
    static initonly array<String^>^ Events;
    static initonly array<String^>^ Numerals;

    static RomanDate()
    {
        Months = gcnew array<String^>(12);
        for (int i=0; i<12; i++)
        {
            Months[i] = gcnew String(libcalendar::RomanDate::MONTHS[i].c_str());
        }

        Events = gcnew array<String^>(3);
        for (int i=0; i<3; i++)
        {
            Events[i] = gcnew String(libcalendar::RomanDate::EVENTS[i].c_str());
        }

        Numerals = gcnew array<String^>(19);
        for (int i=0; i<19; i++)
        {
            Numerals[i] = gcnew String(libcalendar::RomanDate::NUMERALS[i].c_str());
        }

    }

private:
    libcalendar::RomanDate* _rd;
};

}

#endif

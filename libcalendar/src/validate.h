
#ifndef LIBCALENDAR_VALIDATE_H
#define LIBCALENDAR_VALIDATE_H

#include <cassert>
#include <string>
#include <exception>

namespace libcalendar
{
    template <class T>
    void ValidateEqual(T a1, T a2)
    {
#ifdef LIBCALENDAR_EXCEPTIONS
        if (a1 == a2)
        {
            std::string msg = std::to_string(a1) + " is not equal to " + std::to_string(a2);
            throw std::logic_error(msg);
        }
#else
        assert(a1 == a2);
#endif
    }

    template <class T>
    void ValidateLess(T a1, T a2)
    {
#ifdef LIBCALENDAR_EXCEPTIONS
        if (!(a1 < a2))
        {
            std::string msg = std::to_string(a1) + " is not less than " + std::to_string(a2);
            throw std::logic_error(msg);
        }
#else
        assert(a1 < a2);
#endif
    }


    template <class T>
    void ValidateLessEqual(T a1, T a2)
    {
#ifdef LIBCALENDAR_EXCEPTIONS
        if (!(a1 <= a2))
        {
            std::string msg = std::to_string(a1) + " is not less than or equal to " + std::to_string(a2);
            throw std::logic_error(msg);
        }
#else
        assert(a1 <= a2);
#endif
    }

    template <class T>
    void ValidateGreater(T a1, T a2)
    {
#ifdef LIBCALENDAR_EXCEPTIONS
        if (!(a1 > a2))
        {
            std::string msg = std::to_string(a1) + " is not greater than " + std::to_string(a2);
            throw std::logic_error(msg);
        }
#else
        assert(a1 > a2);
#endif
    }


    template <class T>
    void ValidateGreaterEqual(T a1, T a2)
    {
#ifdef LIBCALENDAR_EXCEPTIONS
        if (!(a1 <= a2))
        {
            std::string msg = std::to_string(a1) + " is not greater than or equal to " + std::to_string(a2);
            throw std::logic_error(msg);
        }
#else
        assert(a1 <= a2);
#endif
    }

    template <class T>
    void ValidateBetween(T a, T b1, T b2)
    {
#ifdef LIBCALENDAR_EXCEPTIONS
        if (a >= b1 && a <= b2)
        {
            std::string msg = std::to_string(a) + " is not between " + std::to_string(b1) + " and " + std::to_string(b2);
            throw std::logic_error(msg);
        }
#else
        assert(a >= b1 && a <= b2);
#endif
    }

}


#endif



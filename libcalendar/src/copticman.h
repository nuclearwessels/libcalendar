
#pragma once

#include "fixeddateman.h"
#include "coptic.h"

using namespace System;

namespace libcalendarman
{

public ref class CopticDate : public FixedDate
{
public:
	CopticDate();
	CopticDate(int fd);
	CopticDate(int year, int month, int day);
    !CopticDate();
    ~CopticDate();

	virtual void FromFixed(int fd) override;
	virtual int ToFixed() override;

    property int Year
    {
        void set(int year) { _cd->SetYear(year); }
        int get() { return _cd->Year(); }
    }

    property int Month
    {
        void set(int month) { _cd->SetMonth(month); }
        int get() { return _cd->Month(); }
    }

    property int Day
    {
        void set(int day) { _cd->SetDay(day);  }
        int get() { return _cd->Day(); }
    }

	virtual String^ ToString() override;

	static bool IsLeapYear(int year);

	static array<int>^ GetCopticInGregorian(int gregorianYear, int copticMonth, int copticDay);

	static array<int>^ GetCopticChristmas(int gregorianYear);

	static const int Epoch = libcalendar::CopticDate::EPOCH;

	static initonly array<String^>^ Months;
	static initonly array<String^>^ DaysOfWeek;

    static CopticDate()
    {
        Months = gcnew array<String^>(13);
        for (int i=0; i<13; i++)
        {
            Months[i] = gcnew String( libcalendar::CopticDate::MONTHS[i].c_str());
        }

        DaysOfWeek = gcnew array<String^>(7);
        for (int i=0; i<7; i++)
        {
            DaysOfWeek[i] = gcnew String( libcalendar::CopticDate::DAYS_OF_WEEK[i].c_str());
        }
    }

private:
	libcalendar::CopticDate* _cd;

};


}


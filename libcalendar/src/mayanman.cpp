

#include "mayanman.h"

using namespace libcalendarman;


MayanLongCountDate::MayanLongCountDate()
{
    _mlcd = new libcalendar::MayanLongCountDate();
}

MayanLongCountDate::MayanLongCountDate(int fd)
{
    _mlcd = new libcalendar::MayanLongCountDate(fd);
}


MayanLongCountDate::MayanLongCountDate(int baktun, int katun, int tun, int uinal, int kin)
{
    _mlcd = new libcalendar::MayanLongCountDate(baktun, katun, tun, uinal, kin);
}

MayanLongCountDate::!MayanLongCountDate()
{
    if (_mlcd)
    {
        delete _mlcd;
        _mlcd = 0;
    }
}


MayanLongCountDate::~MayanLongCountDate()
{
    this->!MayanLongCountDate();
}



int MayanLongCountDate::ToFixed()
{
    return _mlcd->ToFixed();
}


void MayanLongCountDate::FromFixed(int fd) 
{
    _mlcd->FromFixed(fd);
}


String^ MayanLongCountDate::ToString()
{
    return gcnew String( _mlcd->ToString().c_str() );
}




MayanHaabDate::MayanHaabDate()
{
    _mhd = new libcalendar::MayanHaabDate();
}

MayanHaabDate::MayanHaabDate(int fd)
{
    _mhd = new libcalendar::MayanHaabDate(fd);
}


MayanHaabDate::MayanHaabDate(int month, int day)
{
    _mhd = new libcalendar::MayanHaabDate(month, day);
}


MayanHaabDate::!MayanHaabDate()
{
    if (_mhd)
    {
        delete _mhd;
        _mhd = 0;
    }
}

MayanHaabDate::~MayanHaabDate()
{
    this->!MayanHaabDate();
}



int MayanHaabDate::ToFixed()
{
    throw gcnew InvalidOperationException("ToFixed() not possible with Mayan Haab date.");
}


void MayanHaabDate::FromFixed(int fd)
{

}



String^ MayanHaabDate::ToString() 
{
    return gcnew String( _mhd->ToString().c_str() );
}

int MayanHaabDate::GetMayanHaabOrdinal(int month, int day)
{
    return libcalendar::MayanHaabDate::GetMayanHaabOrdinal(month, day);
}


int MayanHaabDate::GetMayanHaabOnOrBefore(MayanHaabDate^ mhd, int fd)
{
    libcalendar::MayanHaabDate mhd2(mhd->Month, mhd->Day);

    return libcalendar::MayanHaabDate::GetMayanHaabOnOrBefore(mhd2, fd);
}




MayanTzolkinDate::MayanTzolkinDate()
{
    _mtd = new libcalendar::MayanTzolkinDate();
}


MayanTzolkinDate::MayanTzolkinDate(int fd)
{
    _mtd = new libcalendar::MayanTzolkinDate(fd);
}


MayanTzolkinDate::MayanTzolkinDate(int number, int name)
{
    _mtd = new libcalendar::MayanTzolkinDate(number, name);
}


MayanTzolkinDate::!MayanTzolkinDate()
{
    if (_mtd)
    {
        delete _mtd;
        _mtd = 0;
    }
}


MayanTzolkinDate::~MayanTzolkinDate()
{
    this->!MayanTzolkinDate();
}


int MayanTzolkinDate::ToFixed()
{
    return _mtd->ToFixed();
}

void MayanTzolkinDate::FromFixed(int fd) 
{
    _mtd->FromFixed(fd);
}


String^ MayanTzolkinDate::ToString() 
{
    return gcnew String( _mtd->ToString().c_str() );
}


int MayanTzolkinDate::GetMayanTzolkinOrdinal(int number, int name)
{
    return libcalendar::MayanTzolkinDate::GetMayanTzolkinOrdinal(number, name);
}

int MayanTzolkinDate::GetMayanTzolkinOnOrBefore(MayanTzolkinDate^ mtd, int fd)
{
    libcalendar::MayanTzolkinDate mtd2(mtd->Number, mtd->Name);

    return libcalendar::MayanTzolkinDate::GetMayanTzolkinOnOrBefore(mtd2, fd);
}




int MayanDateUtils::GetMayanCalendarRoundOnOrBefore(MayanHaabDate^ mhd, MayanTzolkinDate^ mtd, int fd)
{
    libcalendar::MayanHaabDate mhd2(mhd->Month, mhd->Day);
    libcalendar::MayanTzolkinDate mtd2(mtd->Number, mtd->Name);

    return libcalendar::GetMayanCalendarRoundOnOrBefore(mhd2, mtd2, fd);
}





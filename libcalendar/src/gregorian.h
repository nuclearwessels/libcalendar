

#ifndef GREGORIAN_H
#define GREGORIAN_H

#include "fixeddate.h"
#include "event.h"

namespace libcalendar
{

class GregorianDate : public FixedDate
{
public:
	GregorianDate();
	GregorianDate(int fd);
	GregorianDate(int year, int month, int day);

	void SetYear(int year);
	int Year() const;

	void SetMonth(int month);
	int Month() const;
    int NextMonth() const;
    int PreviousMonth() const;
	
	void SetDay(int day);
	int Day() const;

	int OrdinalDayOfYear() const;
    int WeekOfMonth() const;

	int ToFixed() const;
	void FromFixed(int fixed);
	std::string ToString() const;

	static bool IsLeapYear(int gregorianYear);
	static int GetYearFromFixed(int fixedDate);

	static int GetUSMemorialDay(int year);
	static int GetUSLaborDay(int year);
	static int GetUSElectionDay(int year);
	static int GetUSThanksgiving(int year);
	static int GetUSDSTBegin(int year);
	static int GetUSDSTEnd(int year);

	static int GetEaster(int gregorianYear);
	static int GetSeptuagesimaSunday(int gyear);
	static int GetSexagesimaSunday(int gyear);
	static int GetShroveSunday(int gyear);
	static int GetShroveMonday(int gyear);
	static int GetMardiGras(int gyear);
	static int GetAshWednesday(int gyear);
	static int GetPassionSunday(int gyear);
	static int GetPalmSunday(int gyear);
	static int GetHolyThursday(int gyear);
	static int GetGoodFriday(int gyear);
	static int GetRogationSunday(int gyear);
	static int GetAscensionDay(int gyear);
	static int GetPentecost(int gyear);
	static int GetWhitmundy(int gyear);
	static int GetTrinitySunday(int gyer);
	static int GetCorpusChristi(int gyear);
	static int GetCorpusChristiUSA(int gyear);

    static int GetDaysInMonth(int year, int month);
    static int GetWeeksInMonth(int year, int month);

    static std::vector<Event> GetUSEvents(int gregorianYear);
    static std::vector<Event> GetChristianEvents(int gregorianYear);

	static const int EPOCH = 1;

    static const std::vector<std::string> MONTHS;
    static const std::vector<std::string> DAYS_OF_WEEK;

private:
	int year_, month_, day_;
};

}

#endif

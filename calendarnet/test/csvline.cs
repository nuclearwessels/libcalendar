
using System;
using System.Collections.Generic;
using System.IO;

namespace calendarnet.test
{

    public class CSVLine : List<String>
    {
        public CSVLine(string line)
        {
            string[] elements = line.Split(',');
            foreach (string s in elements)
            {
                Add(s);
            }
        }


        public int ToInt(int i)
        {
            return Int32.Parse(this[i]);
        }


        public double ToDouble(int i)
        {
            return Double.Parse(this[i]);
        }


        public static List<CSVLine> ReadCSVFile(string filename)
        {
            List<CSVLine> returnme = new List<CSVLine>();

            StreamReader sr = new StreamReader(new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read));

            string s = sr.ReadLine();

            while (s != null)
            {
                returnme.Add(new CSVLine(s));
                s = sr.ReadLine();
            }

            sr.Close();

            return returnme;
        }
    }

}


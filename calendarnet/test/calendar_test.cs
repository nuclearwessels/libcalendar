﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using System.Reflection;

using calendarnet;
using calendarnet.test;

namespace calendarnet_test
{

    [TestFixture]
    public class CalendarTest
    {
        private List<CSVLine> dates1, dates2, dates3, dates4, dates5;

        public CalendarTest()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            FileInfo fi = new FileInfo(assembly.Location);

            dates1 = CSVLine.ReadCSVFile(Path.Combine(fi.DirectoryName, "dates1.csv"));
            dates2 = CSVLine.ReadCSVFile(Path.Combine(fi.DirectoryName, "dates2.csv"));
            dates3 = CSVLine.ReadCSVFile(Path.Combine(fi.DirectoryName, "dates3.csv"));
            dates4 = CSVLine.ReadCSVFile(Path.Combine(fi.DirectoryName, "dates4.csv"));
            dates5 = CSVLine.ReadCSVFile(Path.Combine(fi.DirectoryName, "dates5.csv"));

            dates1.RemoveRange(0, 2);
            dates2.RemoveRange(0, 2);
            dates3.RemoveRange(0, 2);
            dates4.RemoveRange(0, 2);
            dates5.RemoveRange(0, 2);
        }


        [Test]
        public void TestRational()
        {
            Rational r = new Rational(0, 2, 4);
            r.Reduce();
            Assert.IsTrue(r.Whole == 0 && r.Num == 1 && r.Denom == 2);

            Rational r1 = new Rational(0, 1, 8);
            Rational sum = r + r1;
            sum.Reduce();
            Assert.IsTrue(sum.Whole == 0 && sum.Num == 5 && sum.Denom == 8);

            Rational subtract = r - r1;
            subtract.Reduce();
            Assert.IsTrue(subtract.Whole == 0 && subtract.Num == 3 && subtract.Denom == 8);

            Rational mult = r * r1;
            mult.Reduce();
            Assert.IsTrue(mult.Whole == 0 && mult.Num == 1 && mult.Denom == 16);

            Rational divide = r / r1;
            divide.Reduce();
            Assert.IsTrue(divide.Whole == 4 && divide.Num == 0 && divide.Denom == 2);


            // test Mod
            Rational m = Rational.Mod(r, r1);
            Assert.IsTrue(m == 0.0f);
        }


        [Test]
        public void TestWeekday()
        {
            string[] dows = { "Sunday", "Monday", "Tuesday", "Wednesday",
                "Thursday", "Friday", "Saturday" };

            foreach (CSVLine csvLine in dates1)
            {
                int fd = csvLine.ToInt(0);

                int dow = Array.FindIndex(dows, a => a.CompareTo(csvLine[1]) == 0);
                Assert.IsFalse(dow == -1);

                JulianDate jd = new JulianDate(fd);

                bool ok = (int)DateUtils.GetDayOfWeek(jd.ToFixed()) == dow;

                Console.WriteLine("Day of week for Fixed Date {0} is {1}: {2}", fd, dow, ok);
                Assert.IsTrue(ok);
            }

        }


        [Test]
        public void TestJulianDay()
        {
            foreach (CSVLine l in dates1)
            {
                int fd = l.ToInt(0);
                double jd = l.ToDouble(2);

                // from fixed to Julian Day
                JulianDate jd2 = new JulianDate(fd);
                bool ok = jd2.JulianDay == jd;
                Console.WriteLine("Fixed date {0} is Julian Day {1} : {2}", fd, jd2.JulianDay, ok);
                Assert.IsTrue(ok);

                // from Julian Day to fixed
                jd2 = new JulianDate();
                jd2.JulianDay = jd;
                ok = jd2.ToFixed() == fd;
                Console.WriteLine("Julian Day {0} is fixed date {1} : {2}", jd, fd, ok);
                Assert.IsTrue(ok);
            }

        }


        [Test]
        public void TestModifiedJulianDay()
        {
            foreach (CSVLine l in dates1)
            {
                int fd = l.ToInt(0);
                double mjd = l.ToDouble(3);

                // from fixed to Julian Day
                JulianDate jd = new JulianDate(fd);
                bool ok = (int)jd.ModifiedJulianDay == mjd;
                Console.WriteLine("Fixed date {0} is Modified Julian Day {1} : {2}", fd, jd.ModifiedJulianDay, ok);
                Assert.IsTrue(ok);

                // from Julian Day to fixed
                jd = new JulianDate();
                jd.ModifiedJulianDay = mjd;
                ok = jd.ToFixed() == fd;
                Console.WriteLine("Modified Julian Day {0} is fixed date {1} : {2}", jd, fd, ok);
                Assert.IsTrue(ok);
            }
        }


        [Test]
        public void TestGregorian()
        {
            foreach (CSVLine l in dates1)
            {
                int fd = l.ToInt(0);
                int year = l.ToInt(4);
                int month = l.ToInt(5);
                int day = l.ToInt(6);

                // validate FromFixed
                GregorianDate gd1 = new GregorianDate(fd);
                bool ok = gd1.Year == year && gd1.Month == month && gd1.Day == day;
                Console.WriteLine("Fixed date {0} has Gregorian date {1} : {2}", fd, gd1, ok);
                Assert.IsTrue(ok);

                // validate ToFixed
                GregorianDate gd2 = new GregorianDate(year, month, day);
                ok = gd2.ToFixed() == fd;
                Console.WriteLine("Gregorian date {0} has fixed date {1}: {2}", gd2, fd, ok);
                Assert.IsTrue(ok);
            }
        }


        [Test]
        public void TestISO()
        {
            foreach (CSVLine l in dates1)
            {
                int fd = l.ToInt(0);
                int year = l.ToInt(7);
                int week = l.ToInt(8);
                int day = l.ToInt(9);

                // validate FromFixed
                ISODate id = new ISODate(fd);
                bool ok = id.Year == year && id.Week == week && id.Day == day;
                Console.WriteLine("Fixed date {0} has ISO date {1} : {2}", fd, id, ok);
                Assert.IsTrue(ok);

                id = new ISODate(year, week, day);
                ok = id.ToFixed() == fd;
                Console.WriteLine("ISO date {0} has fixed date {1} : {2}", id, fd, ok);
                Assert.IsTrue(ok);
            }
        }


        [Test]
        public void TestJulian()
        {
            foreach (CSVLine l in dates1)
            {
                int fd = l.ToInt(0);
                int year = l.ToInt(10);
                int month = l.ToInt(11);
                int day = l.ToInt(12);

                // validate FromFixed
                JulianDate jd = new JulianDate(fd);
                bool ok = jd.Year == year && jd.Month == month && jd.Day == day;
                Console.WriteLine("Fixed date {0} has Julian date {1} : {2}", fd, jd, ok);
                Assert.IsTrue(ok);

                jd = new JulianDate(year, month, day);
                ok = jd.ToFixed() == fd;
                Console.WriteLine("Julian date {0} has fixed date {1} : {2}", jd, fd, ok);
                Assert.IsTrue(ok);
            }
        }


        [Test]
        public void TestRoman()
        {
            foreach (CSVLine l in dates1)
            {
                int fd = l.ToInt(0);
                int year = l.ToInt(13);
                int month = l.ToInt(14);
                int evt = l.ToInt(15);
                int count = l.ToInt(16);
                bool leap = l[17] == "t";

                RomanDate rd = new RomanDate(fd);
                bool ok = rd.Year == year && rd.Month == month && (int)rd.Event == evt &&
                    rd.Count == count && rd.Leap == leap;
                Console.WriteLine("Fixed date {0} has Roman Date {1} : {2}", fd, rd, ok);
                Assert.IsTrue(ok);

                rd = new RomanDate(year, month, (RomanDate.EventType)evt, count, leap);
                ok = rd.ToFixed() == fd;
                Console.WriteLine("Roman date {0} has fixed date {1} : {2}", rd, fd, ok);
                Assert.IsTrue(ok);
            }
        }


        [Test]
        public void TestEgyptian()
        {
            foreach (CSVLine l in dates1)
            {
                int fd = l.ToInt(0);
                int year = l.ToInt(18);
                int month = l.ToInt(19);
                int day = l.ToInt(20);

                // validate FromFixed
                EgyptianDate ed = new EgyptianDate(fd);
                bool ok = ed.Year == year && ed.Month == month && ed.Day == day;
                Console.WriteLine("Fixed date {0} has Egyptian date {1} : {2}", fd, ed, ok);
                Assert.IsTrue(ok);

                ed = new EgyptianDate(year, month, day);
                ok = ed.ToFixed() == fd;
                Console.WriteLine("Egytian date {0} has fixed date {1} : {2}", ed, fd, ok);
                Assert.IsTrue(ok);
            }
        }


        [Test]
        public void TestArmenian()
        {
            foreach (CSVLine l in dates1)
            {
                int fd = l.ToInt(0);
                int year = l.ToInt(21);
                int month = l.ToInt(22);
                int day = l.ToInt(23);

                // validate FromFixed
                ArmenianDate ad = new ArmenianDate(fd);
                bool ok = ad.Year == year && ad.Month == month && ad.Day == day;
                Console.WriteLine("Fixed date {0} has Armenian date {1} : {2}", fd, ad, ok);
                Assert.IsTrue(ok);

                ad = new ArmenianDate(year, month, day);
                ok = ad.ToFixed() == fd;
                Console.WriteLine("Armenian date {0} has fixed date {1} : {2}", ad, fd, ok);
                Assert.IsTrue(ok);
            }
        }


        [Test]
        public void TestCoptic()
        {
            foreach (CSVLine l in dates1)
            {
                int fd = l.ToInt(0);
                int year = l.ToInt(24);
                int month = l.ToInt(25);
                int day = l.ToInt(26);

                // validate FromFixed
                CopticDate cd = new CopticDate(fd);
                bool ok = cd.Year == year && cd.Month == month && cd.Day == day;
                Console.WriteLine("Fixed date {0} has Coptic date {1} : {2}", fd, cd, ok);
                Assert.IsTrue(ok);

                cd = new CopticDate(year, month, day);
                ok = cd.ToFixed() == fd;
                Console.WriteLine("Coptic date {0} has fixed date {1} : {2}", cd, fd, ok);
                Assert.IsTrue(ok);
            }

        }


        [Test]
        public void TestEthiopic()
        {
            foreach (CSVLine l in dates2)
            {
                int fd = l.ToInt(0);
                int year = l.ToInt(1);
                int month = l.ToInt(2);
                int day = l.ToInt(3);

                // validate FromFixed
                EthiopicDate ed = new EthiopicDate(fd);
                bool ok = ed.Year == year && ed.Month == month && ed.Day == day;
                Console.WriteLine("Fixed date {0} has Ethiopic date {1} : {2}", fd, ed, ok);
                Assert.IsTrue(ok);

                ed = new EthiopicDate(year, month, day);
                ok = ed.ToFixed() == fd;
                Console.WriteLine("Ethiopic date {0} has fixed date {1} : {2}", ed, fd, ok);
                Assert.IsTrue(ok);
            }
        }


        [Test]
        public void TestIslamic()
        {
            foreach (CSVLine l in dates2)
            {
                int fd = l.ToInt(0);
                int year = l.ToInt(4);
                int month = l.ToInt(5);
                int day = l.ToInt(6);

                // validate FromFixed
                IslamicDate id = new IslamicDate(fd);
                bool ok = id.Year == year && id.Month == month && id.Day == day;
                Console.WriteLine("Fixed date {0} has Islamic date {1} : {2}", fd, id, ok);
                Assert.IsTrue(ok);

                id = new IslamicDate(year, month, day);
                ok = id.ToFixed() == fd;
                Console.WriteLine("Islamic date {0} has fixed date {1} : {2}", id, fd, ok);
                Assert.IsTrue(ok);
            }
        }


        [Test]
        public void TestOldHinduSolar()
        {
            foreach (CSVLine l in dates4)
            {
                int fd = l.ToInt(0);
                int year = l.ToInt(9);
                int month = l.ToInt(10);
                int day = l.ToInt(11);

                // validate FromFixed
                OldHinduSolarDate ohsd = new OldHinduSolarDate(fd);
                bool ok = ohsd.Year == year && ohsd.Month == month && ohsd.Day == day;
                Console.WriteLine("Fixed date {0} has Old Hindu Solar date {1} : {2}", fd, ohsd, ok);
                Assert.IsTrue(ok);

                ohsd = new OldHinduSolarDate(year, month, day);
                ok = ohsd.ToFixed() == fd;
                Console.WriteLine("Old Hindu Solar date {0} has fixed date {1} : {2}", ohsd, fd, ok);
                Assert.IsTrue(ok);
            }
        }

#if IGNORE
        [Test]
        public void TestOldHinduLunar()
        {
            List<bool> result = new List<bool>();

            foreach (CSVLine l in dates4)
            {
                int fd = l.ToInt(0);
                int year = l.ToInt(15);
                int month = l.ToInt(16);
                bool leap = (l[17].CompareTo("t") == 0);
                int day = l.ToInt(18);

                // validate FromFixed
                OldHinduLunarFPDate ohld = new OldHinduLunarFPDate(fd);
                bool ok = ohld.Year == year && ohld.Month == month && ohld.Leap == leap && ohld.Day == day;
                Console.WriteLine("Fixed date {0} has Old Hindu Lunar date {1} : {2}", fd, ohld, ok);
                //Assert.IsTrue(ok);

                //ohld = new OldHinduLunarDate(year, month, leap, day);
                //ok = ohld.ToFixed() == fd;
                //Console.WriteLine("Old Hindu Lunar date {0} has fixed date {1} : {2}", ohld, fd, ok);
                //Assert.IsTrue(ok);
            }

        }
#endif

        [Test]
        public void TestHebrew()
        {
            foreach (CSVLine l in dates3)
            {
                int fd = l.ToInt(0);
                int year = l.ToInt(1);
                int month = l.ToInt(2);
                int day = l.ToInt(3);

                HebrewDate hd;
                bool ok;

                // validate FromFixed
                hd = new HebrewDate(fd);
                ok = hd.Year == year && hd.Month == month && hd.Day == day;
                Console.WriteLine("Fixed date {0} has Hebrew date {1} : {2}", fd, hd, ok);
                Assert.IsTrue(ok);

                hd = new HebrewDate(year, month, day);
                ok = hd.ToFixed() == fd;
                Console.WriteLine("Hebrew date {0} has fixed date {1} : {2}", hd, fd, ok);
                Assert.IsTrue(ok);
            }
        }


        [Test]
        public void TestMayan()
        {
            foreach (CSVLine l in dates2)
            {
                int fd = l.ToInt(0);
                int baktun = l.ToInt(20);
                int katun = l.ToInt(21);
                int tun = l.ToInt(22);
                int uinal = l.ToInt(23);
                int kin = l.ToInt(24);

                MayanLongCountDate mlcd;
                bool ok;

                // validate FromFixed
                mlcd = new MayanLongCountDate(fd);
                ok = mlcd.Baktun == baktun &&
                     mlcd.Katun == katun &&
                     mlcd.Tun == tun &&
                     mlcd.Uinal == uinal &&
                     mlcd.Kin == kin;
                Console.WriteLine("Fixed date {0} has Mayan date {1} : {2}", fd, mlcd, ok);
                Assert.IsTrue(ok);

                mlcd = new MayanLongCountDate(baktun, katun, tun, uinal, kin);
                ok = mlcd.ToFixed() == fd;
                Console.WriteLine("Mayan date {0} has fixed date {1} : {2}", mlcd, fd, ok);
                Assert.IsTrue(ok);
            }

        }


        [Test]
        public void TestMayanHaab()
        {
            foreach (CSVLine l in dates2)
            {
                int fd = l.ToInt(0);
                int month = l.ToInt(25);
                int day = l.ToInt(26);

                // validate FromFixed
                MayanHaabDate mhd = new MayanHaabDate(fd);
                bool ok = mhd.Month == month && mhd.Day == day;
                Console.WriteLine("Fixed date {0} has Mayan Haab date {1} : {2}", fd, mhd, ok);
                Assert.IsTrue(ok);

            }
        }


        [Test]
        public void TestMayanTzolkin()
        {
            foreach (CSVLine l in dates2)
            {
                int fd = l.ToInt(0);
                int number = l.ToInt(27);
                int name = l.ToInt(28);

                // validate FromFixed
                MayanTzolkinDate mtd = new MayanTzolkinDate(fd);
                bool ok = mtd.Name == name && mtd.Number == number;
                Console.WriteLine("Fixed date {0} has Mayan Tzolkin date {1} : {2}", fd, mtd, ok);
                Assert.IsTrue(ok);
            }
        }


#if IGNORE
        [Test]
        public void TestBalinesePawukon()
        {
            foreach (CSVLine l in dates3)
            {
                int fd = l.ToInt(0);
                bool luang = String.Compare(l[4].Substring(0, 1), "t", true) == 0;
                int dwiwara = l.ToInt(5);
                int triwara = l.ToInt(6);
                int caturwara = l.ToInt(7);
                int pancawara = l.ToInt(8);
                int sadwara = l.ToInt(9);
                int saptawara = l.ToInt(10);
                int asatawara = l.ToInt(11);
                int sangawara = l.ToInt(12);
                int dasawara = l.ToInt(13);

                BalinesePawukonDate bpd;
                bool ok;

                // validate FromFixed
                bpd = new BalinesePawukonDate(fd);
                ok = bpd.BaliLuang == luang && 
                     bpd.BaliDwiwara == dwiwara && 
                     bpd.BaliTriwara == triwara &&
                     bpd.BaliCaturwara == caturwara && 
                     bpd.BaliPancawara == pancawara && 
                     bpd.BaliSadwara == sadwara && 
                     bpd.BaliSaptawara == saptawara && 
                     bpd.BaliAsatawara == asatawara &&
                     bpd.BaliSangawara == sangawara && 
                     bpd.BaliDasawara == dasawara;
                Console.WriteLine("Fixed date {0} has Balinese Pawukon date {1} : {2}", fd, bpd, ok);
                Assert.IsTrue(ok);
            }
        }
#endif

    }





}

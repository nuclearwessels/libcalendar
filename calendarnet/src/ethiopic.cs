﻿
using System;
using System.Collections.Generic;
using System.Text;


namespace calendarnet
{

	public class EthiopicDate : FixedDateBase, IFixedDate
	{
		public const int Epoch = 2796;
		private int year, month, day;

		private static readonly string[] Months = { "Maskaram", "Teqemt", "Hedar", "Takhsas",
			"Ter", "Yakatit", "Magabit", "Miyazya", "Genbot", "Sane", "Hamle",
			"Nahase", "Paguemen" };

		private static readonly string[] DaysOfWeek = { "Ihud", "Sanyo", "Maksanyo",
					"Rob/Rabu`e", "Hamus", "Arb", "Kidamme" };


		public int Year
		{
			set { year = value; }
			get { return year; }
		}

		public int Month
		{
			set
			{
				if (value < 0 || value > 13)
				{
					throw new ArgumentOutOfRangeException();
				}
				month = value;
			}

			get { return month; }
		}


		public string MonthName
		{
			get
			{
				return Months[month - 1];
			}
		}


		public int Day
		{
			set
			{
				if (value <= 0 || value > ((Month < 13) ? 30 : (IsLeapYear(Year) ? 6 : 5)))
				{
					throw new ArgumentOutOfRangeException();
				}
				day = value;
			}

			get { return day; }
		}


		public string WeekdayName
		{
			get
			{
				//string[] dow = { "Ihud", "Sanyo", "Maksanyo",
				//    "Rob/Rabu`e", "Hamus", "Arb", "Kidamme" };
				return DaysOfWeek[ (int) DateUtils.GetDayOfWeek(ToFixed()) ];
			}
		}


		public EthiopicDate()
		{
			FromFixed(Epoch);
		}


		public EthiopicDate(int fd)
		{
			FromFixed(fd);
		}


        public EthiopicDate(IFixedDate fd)
            : this(fd.ToFixed())
        {

        }


		public EthiopicDate(int year, int month, int day)
		{
			Year = year;
			Month = month;
			Day = day;
		}


		public void FromFixed(int fd)
		{
			CopticDate cd = new CopticDate(fd + CopticDate.Epoch - Epoch);
			Year = cd.Year;
			Month = cd.Month;
			Day = cd.Day;
		}


		public int ToFixed()
		{
			CopticDate cd = new CopticDate(year, month, day);
			return Epoch + cd.ToFixed() - CopticDate.Epoch;
		}


		public override string ToString()
		{
			return String.Format("{0}({1}) {2}, {3} E.E.", MonthName, Month,
				Day, Year);
		}


		public List<EthiopicDate> GetEthiopicInGregorian(int gregorianYear,
			int ethiopicMonth, int ethiopicDay)
		{
			List<EthiopicDate> returnme = new List<EthiopicDate>();

			GregorianDate jan1 = new GregorianDate(gregorianYear, 1, 1);
			GregorianDate dec31 = new GregorianDate(gregorianYear, 12, 31);

			int y = new EthiopicDate(jan1.ToFixed()).Year;

			EthiopicDate date1 = new EthiopicDate(y, ethiopicMonth, ethiopicDay);
			EthiopicDate date2 = new EthiopicDate(y + 1, ethiopicMonth, ethiopicDay);

			if (date1.ToFixed() >= jan1.ToFixed() && date1.ToFixed() <= dec31.ToFixed())
			{
				returnme.Add(date1);
			}

			if (date2.ToFixed() >= jan1.ToFixed() && date2.ToFixed() <= dec31.ToFixed())
			{
				returnme.Add(date2);
			}

			return returnme;
		}


		public List<EthiopicDate> GetEthiopicChristmas(int gregorianYear)
		{
			return GetEthiopicInGregorian(gregorianYear, 4, 29);
		}

		public static bool IsLeapYear(int ethiopicYear)
		{
			return (DateUtils.Mod(ethiopicYear, 4) == 3) ? true : false;
		}


	}


}


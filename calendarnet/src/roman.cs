﻿
using System;
using System.Collections.Generic;
using System.Text;


namespace calendarnet
{
	public class RomanDate : FixedDateBase, IFixedDate
	{
		public enum EventType { Kalends = 1, Nones, Ides };
		private int month, year;
		private EventType evt;
		private bool leap;
		private int count;

        public static readonly string[] MonthNames = { "January", "February", "March",
			"April", "May", "June", "July", "August", "September", "October",
			"November", "December" };

        public static readonly string[] EventNames = { "Kalends", "Nones", "Ides" };

		public int Year
		{
			set { year = value; }
			get { return year; }
		}

		public int Month
		{
			set
			{
				if (value < 1 || value > 12)
				{
					throw new ArgumentOutOfRangeException("Month",
						"Month must be between 1 and 12.");
				}
				month = value;
			}

			get { return month; }
		}


        public string MonthName
        {
            get { return MonthNames[month - 1]; }
        }


		public EventType Event
		{
			set { evt = value; }
			get { return evt; }
		}


		public string EventName
		{
			get
			{
                return EventNames[ (int) evt - 1];
			}
		}


		public int Count
		{
			set
			{
				if (count < 0)
				{
					throw new ArgumentOutOfRangeException();
				}

				// TODO need to check for valid range

				count = value;
			}
			get { return count; }
		}


		public bool Leap
		{
			set { leap = value; }
			get { return leap; }
		}


		public RomanDate()
		{
			FromFixed(JulianDate.Epoch);
		}


		public RomanDate(int fd)
		{
			FromFixed(fd);
		}


        public RomanDate(IFixedDate fd)
            : this(fd.ToFixed())
        {

        }


		public RomanDate(int year, int month, EventType evt, int count, bool leap)
		{
			Year = year;
			Month = month;
			Event = evt;
			Count = count;
			Leap = leap;
		}


		public int ToFixed()
		{
			int returnme = 0;

			switch (evt)
			{
			case EventType.Kalends:
				JulianDate jd1 = new JulianDate(year, month, 1);
				returnme = jd1.ToFixed();
				break;

			case EventType.Nones:
				JulianDate jdNones = new JulianDate(year, month, GetNones(month));
				returnme = jdNones.ToFixed();
				break;

			case EventType.Ides:
				JulianDate jdIdes = new JulianDate(year, month, GetIdes(month));
				returnme = jdIdes.ToFixed();
				break;
			}

			returnme -= count;
			if (JulianDate.IsLeapYear(year) && month == 3 && evt == EventType.Kalends
				&& count >= 6 && count <= 16)
			{
				returnme += 0;
			}
			else
			{
				returnme += 1;
			}

			returnme += Leap ? 1 : 0;

			return returnme;
		}


		public void FromFixed(int fd)
		{
			JulianDate jd = new JulianDate(fd);

			if (jd.Day == 1)
			{
				Year = jd.Year;
				Month = jd.Month;
				Event = EventType.Kalends;
				Count = 1;
				Leap = false;
			}
			else if (jd.Day <= GetNones(jd.Month))
			{
				Year = jd.Year;
				Month = jd.Month;
				Event = EventType.Nones;
				Count = GetNones(month) - jd.Day + 1;
				Leap = false;
			}
			else if (jd.Day <= GetIdes(jd.Month))
			{
				Year = jd.Year;
				Month = jd.Month;
				Event = EventType.Ides;
				Count = GetIdes(month) - jd.Day + 1;
				Leap = false;
			}
			else if (jd.Month != 2 || !JulianDate.IsLeapYear(jd.Year))
			{
                int monthPrime = DateUtils.Amod(jd.Month + 1, 12);
				int yearPrime;
				if (monthPrime != 1)
				{
					yearPrime = jd.Year;
				}
				else if (monthPrime == 1 && jd.Year != -1)
				{
					yearPrime = jd.Year + 1;
				}
				else
				{
					yearPrime = 1;
				}

				RomanDate rd = new RomanDate(yearPrime, monthPrime, EventType.Kalends,
					1, false);
				int kalends1 = rd.ToFixed();

				Year = yearPrime;
				Month = monthPrime;
				Event = EventType.Kalends;
				Count = kalends1 - fd + 1;
				Leap = false;
			}
			else if (jd.Day < 25)
			{
				Year = jd.Year;
				Month = 3;
				Event = EventType.Kalends;
				Count = 30 - jd.Day;
				Leap = false;
			}
			else
			{
				Year = jd.Year;
				Month = 3;
				Event = EventType.Kalends;
				Count = 31 - jd.Day;
				Leap = (jd.Day == 25) ? true : false;
			}
		}


		public override string ToString()
		{
			JulianDate jd = new JulianDate(ToFixed());

			String yearString;

			if (jd.Month == 12 && Event == EventType.Kalends)
			{
				int nextYear = (Year == -1) ? 1 : (Year + 1);
                if (Year < 0)
                {
                    nextYear = (Year == -1) ? 1 : (Year + 1);
                }
                else
                {
                    nextYear = Year;
                }
				yearString = String.Format("{0} {1}", Math.Abs(nextYear),
					((nextYear < 0) ? "B.C.E." : "C.E."));
			}
			else
			{
				yearString = String.Format("{0} {1}", Math.Abs(Year),
					((Year < 0) ? "B.C.E." : "C.E."));
			}

			if (jd.Month == 2 && Event == EventType.Kalends && Leap)
			{
				return String.Format("ante diem bis {0} {1} {2}, {3}",
					GetRomanNumeral((uint)Count),
					EventName, MonthName, yearString);
			}
			else
			{
				if (count == 1)
				{
					return String.Format("{0} {1}, {2}",
						EventName, MonthName, yearString);
				}
				else if (count == 2)
				{
					return String.Format("pridie {0} {1}, {2}",
						EventName, MonthName, yearString);
				}
				else
				{
					return String.Format("ante diem {0} {1} {2}, {3}",
						GetRomanNumeral((uint)Count),
						EventName, MonthName, yearString);
				}
			}
		}



		public static int GetIdes(int month)
		{
			if (month < 1 || month > 12)
			{
				throw new ArgumentOutOfRangeException();
			}

			if (month == 3 || month == 5 || month == 7 || month == 10)
			{
				return 15;
			}
			else
			{
				return 13;
			}
		}


		public static int GetNones(int month)
		{
			return GetIdes(month) - 8;
		}


		public static string GetRomanNumeral(uint number)
		{
			if (number == 0)
			{
				return "N";
			}

			uint[] values = new uint[] { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };
			string[] numerals = new string[] { "M", "CM", "D", "CD", "C", 
				"XC", "L", "XL", "X", "IX", "V", "IV", "I" };

			StringBuilder result = new StringBuilder();

			for (int i = 0; i < 13; i++)
			{
				while (number >= values[i])
				{
					number -= values[i];
					result.Append(numerals[i]);
				}
			}

			return result.ToString();
		}

	}

}


﻿
using System;
using System.Collections.Generic;
using System.Text;


namespace calendarnet
{
    public delegate int GergorianYearDelete(int gyear);


	public class GregorianDate : FixedDateBase, IFixedDate
	{
		private int month, day, year;
		public const int Epoch = 1;

		public readonly string[] MonthNames = { "January", "February", "March",
												"April", "May", "June", 
												"July", "August", "September",
												"October", "November", "December" };

		public int Year
		{
			set { year = value; }
			get { return year; }
		}

		public int Month
		{
			set
			{
				if (value <= 0 || value > 12)
				{
					throw new ArgumentOutOfRangeException("month",
						"Month must be between 1 and 12.");
				}
				month = value;
			}

			get { return month; }
		}

		public string MonthName
		{
			get
			{
				switch (month)
				{
				case 1: return "January";
				case 2: return "February";
				case 3: return "March";
				case 4: return "April";
				case 5: return "May";
				case 6: return "June";
				case 7: return "July";
				case 8: return "August";
				case 9: return "September";
				case 10: return "October";
				case 11: return "November";
				case 12: return "December";
				default:
					// this shouldn't happen
					throw new ArgumentOutOfRangeException();
				}
			}
		}

		public int Day
		{
			set
			{
				if (value <= 0)
				{
					throw new ArgumentOutOfRangeException("day",
						"Day must be greater than zero.");
				}

				int maxday = 0;

				switch (month)
				{
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					maxday = 31;
					break;

				case 9:
				case 4:
				case 6:
				case 11:
					maxday = 30;
					break;

				case 2:
					maxday = (IsLeapYear(Year) ? 29 : 28);
					break;
				}

				if (value > maxday)
				{
					throw new ArgumentOutOfRangeException("day",
						String.Format("Day must be less than {0} for month {1}.",
						maxday, month));
				}

				day = value;
			}

			get { return day; }
		}


		public GregorianDate()
			: this(Epoch)
		{

		}


		public GregorianDate(int fd)
		{
			FromFixed(fd);
		}


		public GregorianDate(int year, int month, int day)
		{
			Year = year;
			Month = month;
			Day = day;
		}


		public GregorianDate(IFixedDate fd)
		{
			FromFixed(fd.ToFixed());
		}


		public int ToFixed()
		{
			int returnme = Epoch - 1;
			returnme += 365 * (year - 1);
			returnme += DateUtils.Floor(year - 1, 4);
			returnme -= DateUtils.Floor(year - 1, 100);
			returnme += DateUtils.Floor(year - 1, 400);
			returnme += DateUtils.Floor(367 * month - 362, 12);

			if (month <= 2)
			{
				returnme += 0;
			}
			else if (IsLeapYear(year))
			{
				returnme += -1;
			}
			else
			{
				returnme += -2;
			}

			returnme += day;

			return returnme;
		}


		public void FromFixed(int fd)
		{
			year = GetYearFromFixed(fd);

			GregorianDate jan1 = new GregorianDate(year, 1, 1);

			int priorDays = fd - jan1.ToFixed();

			GregorianDate march1 = new GregorianDate(year, 3, 1);

			int correction;
			if (fd < march1.ToFixed())
			{
				correction = 0;
			}
			else if (IsLeapYear(year))
			{
				correction = 1;
			}
			else
			{
				correction = 2;
			}

            month = DateUtils.Floor(12 * (priorDays + correction) + 373, 367);

			GregorianDate gd = new GregorianDate(year, month, 1);
			day = fd - gd.ToFixed() + 1;
		}


		public override string ToString()
		{
			return String.Format("{0} {1}, {2}", MonthName, Day, Year);
		}


		public static bool IsLeapYear(int gy)
		{
			bool returnme = false;

            if (DateUtils.Mod(gy, 4) == 0)
			{
                switch (DateUtils.Mod(gy, 400))
				{
				case 100:
				case 200:
				case 300:
					returnme = false;
					break;

				default:
					returnme = true;
					break;
				}
			}

			return returnme;
		}


		public static int GetYearFromFixed(int fd)
		{
			int d0 = fd - Epoch;
            int n400 = DateUtils.Floor(d0, 146097);
            int d1 = DateUtils.Mod(d0, 146097);
            int n100 = DateUtils.Floor(d1, 36524);
            int d2 = DateUtils.Mod(d1, 36524);
            int n4 = DateUtils.Floor(d2, 1461);
            int d3 = DateUtils.Mod(d2, 1461);
            int n1 = DateUtils.Floor(d3, 365);
			int year = 400 * n400 + 100 * n100 + 4 * n4 + n1;

			int returnme;
			if (n100 == 4 || n1 == 4)
			{
				returnme = year;
			}
			else
			{
				returnme = year + 1;
			}

			return returnme;
		}


		public int GetOrdinalDayOfYear()
		{
			int d0 = ToFixed() - Epoch;
            int d1 = DateUtils.Mod(d0, 146097);
            int n100 = DateUtils.Floor(d1, 36524);
            int d2 = DateUtils.Mod(d1, 36524);
            int d3 = DateUtils.Mod(d2, 1461);
            int n1 = DateUtils.Floor(d3, 365);

			int returnme;
			if (n1 != 4 && n100 != 4)
			{
                returnme = DateUtils.Mod(d3, 365) + 1;
			}
			else
			{
				returnme = 366;
			}

			return returnme;
		}


		public static int GetDaysInMonth(int year, int month)
		{
			switch (month)
			{
			case 1:
			case 3:
			case 5:
			case 7:
            case 8:
            case 10:
            case 12:
				return 31;

			case 2:
				return IsLeapYear(year) ? 29 : 28;

			case 4:
			case 6:
			case 9:
			case 11:
				return 30;

			default:
				throw new ArgumentException("Invalid month", "month");
			}
		}


        public GregorianDate NextMonth
        {
            get 
            {
                return new GregorianDate(ToFixed() + GetDaysInMonth(year, month));
            }
        }


        public GregorianDate PreviousMonth
        {
            get
            {
                int m = month - 1;
                int y = year;
                if (m < 1)
                {
                    m = 12;
                    y--;
                }
                return new GregorianDate(ToFixed() - GetDaysInMonth(y, m));
            }
        }

		public static GregorianDate GetUSMemorialDay(int year)
		{
			GregorianDate may31 = new GregorianDate(year, 5, 31);
            return new GregorianDate(DateUtils.FirstWeekday(may31.ToFixed(), DateUtils.Weekday.Mon));
		}


		public static GregorianDate GetUSLaborDay(int year)
		{
			GregorianDate sep1 = new GregorianDate(year, 9, 1);
            return new GregorianDate(DateUtils.FirstWeekday(sep1.ToFixed(), DateUtils.Weekday.Mon));
		}


		public static GregorianDate GetUSElectionDay(int year)
		{
			GregorianDate nov2 = new GregorianDate(year, 11, 2);
            return new GregorianDate(DateUtils.FirstWeekday(nov2.ToFixed(), DateUtils.Weekday.Tues));
		}


		public static GregorianDate GetUSDSTBegin(int year)
		{
			if (year >= 2007)
			{
				GregorianDate mar1 = new GregorianDate(year, 3, 1);
                return new GregorianDate(DateUtils.NthWeekday(mar1.ToFixed(), 2, DateUtils.Weekday.Sun));
			}
			else
			{
				GregorianDate apr1 = new GregorianDate(year, 4, 1);
                return new GregorianDate(DateUtils.FirstWeekday(apr1.ToFixed(), DateUtils.Weekday.Sun));
			}
		}


		public static GregorianDate GetUSDSTEnd(int year)
		{
			if (year >= 2007)
			{
				GregorianDate nov1 = new GregorianDate(year, 11, 1);
                return new GregorianDate(DateUtils.FirstWeekday(nov1.ToFixed(), DateUtils.Weekday.Sun));
			}
			else
			{
				GregorianDate oct31 = new GregorianDate(year, 10, 31);
                return new GregorianDate(DateUtils.LastWeekday(oct31.ToFixed(), DateUtils.Weekday.Sun));
			}
		}


        //# New Year's Day, January 1st.
        //# Birthday of Martin Luther King, third Monday in January.
        //# Inauguration Day, January 20th every four years, starting in 1937.
        //# Washington's Birthday, third Monday in February since 1971; prior to that year, it was celebrated on the traditional date of February 22.
        //# Inauguration Day, March 4th every four years, pre-1937.
        //# Armed Forces Day, third Saturday in May.
        //# Memorial Day, last Monday in May since 1971; from 1868 to 1970 it was celebrated on May 30, and was called Decoration Day for part of that time.
        //# Flag Day, June 14th.
        //# United States of America's Independence Day, July 4.
        //# Labor Day, first Monday in September.
        //# Columbus Day, second Monday in October (federal holiday since 1971).
        //# Election Day, Tuesday on or after November 2.
        //# Veterans Day, November 11th (except from 1971 to 1977, inclusive, when it was celebrated on the fourth Monday in October; formerly known as Armistice). 



        public static GregorianDate GetUSThanksgiving(int year)
        {
            GregorianDate nov1 = new GregorianDate(year, 11, 1);
            return new GregorianDate(DateUtils.NthWeekday(nov1.ToFixed(), 4, DateUtils.Weekday.Thu));
        }




		public static GregorianDate GetEaster(int year)
		{
            int century = DateUtils.Floor(year, 100) + 1;
            int shiftedEpact = DateUtils.Mod(14 + 11 * DateUtils.Mod(year, 19) - DateUtils.Floor(3 * century, 4) +
                DateUtils.Floor(5 + 8 * century, 25), 30);

			int adjustedEpact;
            if (shiftedEpact == 0 || (shiftedEpact == 1 && 10 < DateUtils.Mod(year, 19)))
			{
				adjustedEpact = shiftedEpact + 1;
			}
			else
			{
				adjustedEpact = shiftedEpact;
			}

			GregorianDate apr19 = new GregorianDate(year, 4, 19);
			GregorianDate paschalMoon = new GregorianDate(apr19.ToFixed() - adjustedEpact);

            return new GregorianDate(DateUtils.WeekdayAfter(paschalMoon.ToFixed(), DateUtils.Weekday.Sun));
		}


		public static GregorianDate GetSeptuagesimaSunday(int gyear)
		{
			return new GregorianDate(GetEaster(gyear).ToFixed() - 63);
		}


		public static GregorianDate GetSexagesimaSunday(int gyear)
		{
			return new GregorianDate(GetEaster(gyear).ToFixed() - 56);
		}


		public static GregorianDate GetShroveSunday(int gyear)
		{
			return new GregorianDate(GetEaster(gyear).ToFixed() - 49);
		}


		public static GregorianDate GetShroveMonday(int gyear)
		{
			return new GregorianDate(GetEaster(gyear).ToFixed() - 48);
		}


		public static GregorianDate GetMardiGras(int gyear)
		{
			return new GregorianDate(GetEaster(gyear).ToFixed() - 47);
		}


		public static GregorianDate GetAshWednesday(int gyear)
		{
			return new GregorianDate(GetEaster(gyear).ToFixed() - 46);
		}


		public static GregorianDate GetPassionSunday(int gyear)
		{
			return new GregorianDate(GetEaster(gyear).ToFixed() - 14);
		}


		public static GregorianDate GetPalmSunday(int gyear)
		{
			return new GregorianDate(GetEaster(gyear).ToFixed() - 7);
		}


		public static GregorianDate GetHolyThursday(int gyear)
		{
			return new GregorianDate(GetEaster(gyear).ToFixed() - 3);
		}


		public static GregorianDate GetGoodFriday(int gyear)
		{
			return new GregorianDate(GetEaster(gyear).ToFixed() - 2);
		}


		public static GregorianDate GetRogationSunday(int gyear)
		{
			return new GregorianDate(GetEaster(gyear).ToFixed() + 35);
		}


		public static GregorianDate GetAscensionDay(int gyear)
		{
			return new GregorianDate(GetEaster(gyear).ToFixed() + 39);
		}


		public static GregorianDate GetPentecost(int gyear)
		{
			return new GregorianDate(GetEaster(gyear).ToFixed() + 49);
		}


		public static GregorianDate GetWhitmundy(int gyear)
		{
			return new GregorianDate(GetEaster(gyear).ToFixed() + 50);
		}


		public static GregorianDate GetTrinitySunday(int gyear)
		{
			return new GregorianDate(GetEaster(gyear).ToFixed() + 56);
		}


		public static GregorianDate GetCorpusChristi(int gyear)
		{
			return new GregorianDate(GetEaster(gyear).ToFixed() + 60);
		}


		public static GregorianDate GetCorpusChristiUSA(int gyear)
		{
			return new GregorianDate(GetEaster(gyear).ToFixed() + 63);
		}


        public IEnumerable<string> GetEvents(IEnumerable<Event> events)
        {
            List<string> returnme = new List<string>();

            int fd = ToFixed();

            foreach (Event e in events)
            {
                if (e.FixedDate == fd)
                {
                    returnme.Add(e.EventName);
                }
            }

            return returnme;
        }


        public static IEnumerable<Event> GetChristianEvents(int gyear)
        {
            List<Event> events = new List<Event>();
            events.Add(new Event(GetEaster(gyear).ToFixed(), "Easter") );
            events.Add(new Event(GetSeptuagesimaSunday(gyear).ToFixed(), "Septuagesima Sunday"));
            events.Add(new Event(GetSexagesimaSunday(gyear).ToFixed(), "Sexagesima Sunday"));
            events.Add(new Event(GetShroveSunday(gyear).ToFixed(), "Shrove Sunday"));
            events.Add(new Event(GetShroveMonday(gyear).ToFixed(), "Shrove Monday"));
            events.Add(new Event(GetAshWednesday(gyear).ToFixed(), "Ash Wednesday"));
            events.Add(new Event(GetPassionSunday(gyear).ToFixed(), "Passion Sunday"));
            events.Add(new Event(GetHolyThursday(gyear).ToFixed(), "Holy Thursday"));
            events.Add(new Event(GetGoodFriday(gyear).ToFixed(), "Good Friday"));
            events.Add(new Event(GetRogationSunday(gyear).ToFixed(), "Rogation Sunday"));
            events.Add(new Event(GetAscensionDay(gyear).ToFixed(), "Ascension Day"));
            events.Add(new Event(GetPentecost(gyear).ToFixed(), "Pentcost"));
            events.Add(new Event(GetWhitmundy(gyear).ToFixed(), "Whitmundy"));
            events.Add(new Event(GetTrinitySunday(gyear).ToFixed(), "Trinity Sunday"));
            events.Add(new Event(GetCorpusChristi(gyear).ToFixed(), "Corpus Christi (non-USA)"));
            events.Add(new Event(GetCorpusChristiUSA(gyear).ToFixed(), "Corpus Christi (USA)"));
            return events;
        }


        public static IEnumerable<Event> GetUSEvents(int year)
        {
            List<Event> events = new List<Event>();
            events.Add(new Event(GetUSMemorialDay(year), "Memorial Day"));
            events.Add(new Event(GetUSLaborDay(year), "Labor Day"));
            events.Add(new Event(GetUSElectionDay(year), "Election Day"));
            events.Add(new Event(GetUSDSTBegin(year), "Daylight Saving Time Begins"));
            events.Add(new Event(GetUSDSTEnd(year), "Daylight Saving Time Ends"));
            return events;
        }

	}

}


﻿
using System;
using System.Text;

namespace calendarnet
{
	public class ArmenianDate : EgyptianDate
	{
		public new const int Epoch = 201443;

		public readonly string[] Months = { "Nawasardi", "Hoȓi", "Sahmi", "Tre",
			"K'aloch", "Arach", "Mehekani", "Areg", "Ahekani", "Mareri", 
			"Margach", "Hrotich" };


		public ArmenianDate() : this(Epoch)
		{
			
		}


		public ArmenianDate(int fd)
		{
			FromFixed(fd);
		}


        public ArmenianDate(IFixedDate fd) : this(fd.ToFixed())
        {
            
        }


		public ArmenianDate(int year, int month, int day) : base(year, month, day)
		{

		}


		public new void FromFixed(int fd)
		{
			EgyptianDate ed = new EgyptianDate(fd + EgyptianDate.Epoch - Epoch);
			Year = ed.Year;
			Month = ed.Month;
			Day = ed.Day;
		}


		public new int ToFixed()
		{
			EgyptianDate ed = new EgyptianDate(year, month, day);
			return Epoch + ed.ToFixed() - EgyptianDate.Epoch;
		}

	}


}


﻿

using System;



namespace calendarnet
{

public class MayanLongCountDate : FixedDateBase, IFixedDate
{
    public const int MayanEpoch = -1137142;

    public int Baktun 
    { 
        get; 
        protected set; 
    }

    public int Katun
    {
        get;
        protected set;
    }

    public int Tun
    {
        get;
        protected set;
    }

    public int Uinal
    {
        get;
        protected set;
    }

    public int Kin
    {
        get;
        protected set;
    }


    public MayanLongCountDate()
        : this(MayanEpoch)
    {

    }

    public MayanLongCountDate(int fd) 
    {
        FromFixed(fd);
    }


    public MayanLongCountDate(int baktun, int katun, int tun, int uinal, int kin)
    {
        Baktun = baktun;
        Katun = katun;
        Tun = tun;
        Uinal = uinal;
        Kin = kin;
    }


    public int ToFixed()
    {
        return MayanEpoch + Baktun * 144000 + Katun * 7200 + Tun * 360 + Uinal * 20 + Kin;
    }

    public void FromFixed(int fd)
    {
        int longCount = fd - MayanEpoch;
        Baktun = DateUtils.Floor(longCount, 144000);

        int dayOfBaktun = DateUtils.Mod(longCount, 144000);
        Katun = DateUtils.Floor(dayOfBaktun, 7200);

        int dayOfKatun = DateUtils.Mod(dayOfBaktun, 7200);
        Tun = DateUtils.Floor(dayOfKatun, 360);

        int dayOfTun = DateUtils.Mod(dayOfKatun, 360);
        Uinal = DateUtils.Floor(dayOfTun, 20);

        Kin = DateUtils.Mod(dayOfTun, 20);
    }

    public override string ToString()
    {
        return String.Format("Baktun={0}, Katun={1}, Tun={2}, Uinal={3}, Kin={4}", Baktun, Katun, Tun, Uinal, Kin);
    }
}


public class MayanHaabDate : FixedDateBase, IFixedDate
{

    public static readonly string[] Months = { "Pop", "Uo", "Zip", "Zotz", "Tzec", "Xul", "Yaxkin",
                                               "Mol", "Chen", "Yax", "Zac", "Ceh", "Mac", "Kankin",
                                               "Muan", "Pax", "Kayab", "Cumku", "Uayeb" };
    public static readonly int MayanHaabEpoch = MayanLongCountDate.MayanEpoch - GetMayanHaabOrdinal(18, 8);

    public int Month
    {
        get;
        protected set;
    }

    public int Day
    {
        get;
        protected set;
    }

    public MayanHaabDate() : this(MayanHaabEpoch)
    {

    }

    public MayanHaabDate(int fd)
    {
        FromFixed(fd);
    }

    public MayanHaabDate(IFixedDate fd)
        : this(fd.ToFixed())
    {

    }

    public MayanHaabDate(int month, int day)
    {
        Month = month; 
        Day = day;
    }

    #region IFixedDate Members

    public int ToFixed()
    {
        throw new InvalidOperationException();
    }

    public void FromFixed(int fd)
    {
        int count = DateUtils.Mod(fd - MayanHaabEpoch, 365);
        Day = DateUtils.Mod(count, 20);
        Month = DateUtils.Floor(count, 20) + 1;
    }

    #endregion


    public override string ToString()
    {
        return String.Format("{0} ({1}), {2}", Months[Month - 1], Month, Day);
    }


    public static int GetMayanHaabOrdinal(int month, int day)
    {
        return (month - 1) * 20 + day;
    }


    public static MayanHaabDate GetMayanHaabOnOrBefore(MayanHaabDate mhd, int fd)
    {
        return new MayanHaabDate(fd - DateUtils.Mod(fd - MayanHaabEpoch - GetMayanHaabOrdinal(mhd.Month, mhd.Day), 365));
    }
}



public class MayanTzolkinDate : IFixedDate
{
    public static readonly string[] Months = { "Imix", "Ik", "Akbal", "Kan", "Chicchan", "Cimi", "Manik",
                                               "Lamat", "Muluc", "Oc", "Chuen", "Eb", "Ben", "Ix",
                                               "Men", "Cib", "Caban", "Etzneb", "Cauac", "Ahau" };
    public static readonly int MayanTzolkinEpoch = MayanLongCountDate.MayanEpoch - GetMayanTzolkinOrdinal(4, 20);

    public int Number { get; protected set; }
    public int Name { get; protected set; }


    public MayanTzolkinDate() : this(MayanTzolkinEpoch)
    {

    }

    public MayanTzolkinDate(int fd)
    {
        FromFixed(fd);
    }

    public MayanTzolkinDate(IFixedDate fd) : this(fd.ToFixed())
    {

    }

    public MayanTzolkinDate(int number, int name)
    {
        Number = number;
        Name = name;
    }

    public int ToFixed()
    {
        throw new InvalidOperationException();
    }

    public void FromFixed(int fd)
    {
        int count = fd - MayanTzolkinEpoch + 1;
        Number = DateUtils.Amod(count, 13);
        Name = DateUtils.Amod(count, 20);
    }


    public override string ToString()
    {
        return String.Format("{0} {1} ({2})", Number, Months[Name-1], Name);
    }

    public static int GetMayanTzolkinOrdinal(int number, int name)
    {
        return DateUtils.Mod(number - 1 + 39 * (number - name), 260);
    }

    public static int GetMayanTzolkinOnOrBefore(MayanTzolkinDate mtd, int fd)
    {
        return fd - DateUtils.Mod(fd - MayanTzolkinEpoch - GetMayanTzolkinOrdinal(mtd.Number, mtd.Name), 260);
    }
}


public static class MayanCalendar
{
    public static int GetMayanCalendarRoundOnOrBefore(MayanHaabDate mhd, MayanTzolkinDate mtd, int fd)
    {
        int haabCount = MayanHaabDate.GetMayanHaabOrdinal(mhd.Month, mhd.Day) + MayanHaabDate.MayanHaabEpoch;
        int tzolkinCount = MayanTzolkinDate.GetMayanTzolkinOrdinal(mtd.Number, mtd.Name) + MayanTzolkinDate.MayanTzolkinEpoch;
        int diff = tzolkinCount - haabCount;

        if (DateUtils.Mod(diff, 5) == 0)
        {
            return fd - DateUtils.Mod(fd - haabCount - 365 * diff, 18980);
        }
        else
        {
            throw new ArgumentException();
        }
    }

}
    

}
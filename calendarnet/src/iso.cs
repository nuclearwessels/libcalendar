﻿
using System;
using System.Collections.Generic;
using System.Text;


namespace calendarnet
{
	public class ISODate : FixedDateBase
	{
		public const int Epoch = GregorianDate.Epoch;
		private int year, week, day;

		public int Year
		{
			set { year = value; }
			get { return year; }
		}


		public int Week
		{
			set
			{
				if (value < 1 || value > 53)
				{
					throw new ArgumentOutOfRangeException();
				}
				week = value;
			}

			get { return week; }
		}


		public int Day
		{
			set
			{
				if (value < 1 || value > 7)
				{
					throw new ArgumentOutOfRangeException();
				}
				day = value;
			}

			get { return day; }
		}


		public ISODate()
		{
			FromFixed(Epoch);
		}


		public ISODate(int fd)
		{
			FromFixed(fd);
		}


		public ISODate(int year, int week, int day)
		{
			Year = year;
			Week = week;
			Day = day;
		}


		public void FromFixed(int fd)
		{
			int approx = GregorianDate.GetYearFromFixed(fd - 3);

			ISODate iso1 = new ISODate(approx + 1, 1, 1);
			Year = (fd >= iso1.ToFixed()) ? (approx + 1) : approx;

			ISODate iso2 = new ISODate(year, 1, 1);
			week = DateUtils.Floor(fd - iso2.ToFixed(), 7) + 1;
			day = DateUtils.Amod(fd, 7);
		}


		public int ToFixed()
		{
			GregorianDate gd = new GregorianDate(year - 1, 12, 28);
			return DateUtils.NthWeekday(gd.ToFixed(), week, DateUtils.Weekday.Sun) + day;
		}


		public override string ToString()
		{
            return String.Format("week {0}, Day {1}, Year {2}", Week, Day, Year);
		}

	}

}


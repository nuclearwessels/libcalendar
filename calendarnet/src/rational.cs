﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;


namespace calendarnet
{
	public class Rational
	{
		private long whole = 0, num = 0, denom = 1;

		public long Num
		{
			set { num = value; }
			get { return num; }
		}


		public long Denom
		{
			set
			{
				if (value == 0)
				{
					throw new ArgumentOutOfRangeException();
				}

				denom = value;
			}

			get { return denom; }
		}

		public long Whole
		{
			set { whole = value; }
			get { return whole; }
		}


		public Rational()
			: this(0, 0, 1)
		{
		}


		public Rational(long whole) : this(whole, 0, 1)
		{

		}


		public Rational(long n, long d)
			: this(0, n, d)
		{
		}


		public Rational(long whole, long num, long denom)
		{
			Whole = whole;
			Num = num;
			Denom = denom;
		}


		public void MakeProper()
		{
			// TODO make this efficient 

			long remainder;
			long quotient = DivRem(num, denom, out remainder);

			whole += quotient;
			num -= quotient * denom;

			if (num < 0)
			{
				num += denom;
				whole--;
			}

			//while (num >= denom)
			//{
			//    num -= denom;
			//    whole++;
			//}
		}


		public void MakeImproper()
		{
            checked
            {
                num += whole * denom;
                whole = 0;
            }
		}


		public void Reduce()
		{
			List<long> commonFactors = GetCommonFactors(num, denom);
            if (commonFactors.Count == 0)
            {
                return;
            }

            long divisor = commonFactors.Max(a => a);

            //long divisor = commonFactors[commonFactors.Count - 1];            

			num /= divisor;
			denom /= divisor;
		}

		private static Dictionary<long, List<long>> factorsDict = new Dictionary<long, List<long>>();

		public static List<long> GetFactors(long a)
		{
			if (a < 1)
			{
				return new List<long>();
			}

			if (factorsDict.ContainsKey(a))
			{
				List<long> value;
				factorsDict.TryGetValue(a, out value);
				return value;
			}
			else
			{
				long remainder;

				List<long> returnme = new List<long>();

				for (long i = 2; i <= a; i++)
				{
					DivRem(a, i, out remainder);
					if (remainder == 0)
					{
						returnme.Add(i);
					}
				}

				factorsDict.Add(a, returnme);
				return returnme;
			}
		}


		public static List<long> GetCommonFactors(long a, long b)
		{
			List<long> returnme = new List<long>();

			List<long> factorsA = GetFactors(a);
			List<long> factorsB = GetFactors(b);

			foreach (long i in factorsA)
			{
				if (factorsB.Contains(i))
				{
					returnme.Add(i);
				}
			}

			return returnme;
		}


		public override string ToString()
		{
			return String.Format("{0} {1}/{2}", whole, num, denom);
		}


        public static Rational Floor(long dividend, long divisor)
        {
            return Floor(new Rational(dividend), new Rational(divisor));
        }

        public static Rational Floor(Rational dividend, Rational divisor)
        {
            Rational r = dividend / divisor;
            //r.MakeProper();
            return r.Floor();
        }

         
        public Rational Floor()
        {
            MakeProper();
            if (Whole < 0)
            {
                return new Rational((Num == 0) ? Whole : (Whole - 1));
            }
            else
            {
                return new Rational(Whole);
            }
        }


		public static Rational Mod(Rational dividend, Rational divisor)
		{
			Rational r = dividend / divisor;
			r.MakeProper();

            Rational a = dividend - ( new Rational(r.Whole) * divisor);
            a.MakeProper();

            return a;
		}


		public Rational Mod(Rational divisor)
		{
			return Mod(this, divisor);
		}


		public static Rational operator +(Rational a, Rational b)
		{
			// num       newnum
			// ------- = -------
			// denom     newDenom

            Rational returnme;

            //checked
            //{
                long newDenom = a.Denom * b.Denom;

                long newNumA = a.Num * newDenom / a.Denom;
                long newNumB = b.Num * newDenom / b.Denom;

                returnme = new Rational(a.Whole + b.Whole, newNumA + newNumB,
                    newDenom);
                returnme.MakeProper();
                //returnme.Reduce();
            //}

			return returnme;
		}


		public static Rational operator +(Rational a, long b)
		{
			return new Rational(a.Whole + b, a.Num, a.Denom);
		}

		public static Rational operator -(Rational a, Rational b)
		{
			long newDenom = a.Denom * b.Denom;

			long newNumA = a.Num * newDenom / a.Denom;
			long newNumB = b.Num * newDenom / b.Denom;

			Rational returnme = new Rational(a.Whole - b.Whole, newNumA - newNumB,
				newDenom);
			returnme.MakeProper();
            //returnme.Reduce();

			return returnme;
		}

		public static Rational operator -(Rational a, long b)
		{
			return new Rational(a.Whole - b, a.Num, a.Denom);
		}


		public static Rational operator *(Rational a, Rational b)
		{
			//Rational returnme = new Rational(a.Whole * b.Whole, a.Num * b.Num,
			//	a.Denom * b.Denom);

            Rational a1 = new Rational(a.Whole, a.Num, a.Denom);
            Rational b1 = new Rational(b.Whole, b.Num, b.Denom);

			a1.MakeImproper();
			b1.MakeImproper();

			Rational c = new Rational(0, a1.Num * b1.Num, a1.Denom * b1.Denom);
			c.MakeProper();
            //c.Reduce();
			return c;
		}


		public static Rational operator *(Rational a, long b)
		{
			Rational c = new Rational(b, 0, 1);
			return (a * c);
		}


		public static Rational operator /(Rational a, Rational b)
		{
			long newNumA = a.Whole * a.Denom + a.Num;
			long newNumB = b.Whole * b.Denom + b.Num;

			Rational newA = new Rational(a.Whole * a.Denom + a.Num, a.Denom);
			Rational newB = new Rational(b.Denom, b.Whole * b.Denom + b.Num);

			return newA * newB;
		}

		public static Rational operator /(Rational a, long b)
		{
			Rational c = new Rational(b, 0, 1);
			return a / c;
		}


		public static implicit operator double(Rational a)
		{
			double returnme = a.Whole;
			returnme += (double)a.Num / (double)a.Denom;
			return returnme;
		}


        public static long DivRem(long num, long denom, out long remainder)
        {
            //long quotient = Math.DivRem(num, denom, out remainder);
            long quotient = num / denom;
            remainder = num - (quotient * denom);
            return quotient;
        }


	}

}


﻿
using System;
using System.Collections.Generic;
using System.Text;


namespace calendarnet
{
	public class HebrewDate : FixedDateBase, IFixedDate
	{
        public static readonly string[] HebrewMonthNames = { "נִיסָן",   // Nisan
                                                      "אייר",   // Iyar
                                                      "סיוון",  // Sivan
                                                      "תַּמּוּז",   // Tammuz
                                                      "אָב",     // Av
                                                      "אֱלוּל",   // Elul
                                                      "תִּשׁרִי",   // Tishri
                                                      "מַרְחֶשְׁוָן", // Marheshvan
                                                      "כִּסְלֵו",   // Kislev
                                                      "טֵבֵת",    // Tevet
                                                      "שְׁבָט",    // Shevat
                                                      "אֲדָר",    // Adar
                                                      "אֲדָר א׳", // Adar I
                                                      "אֲדָר ב׳", // Adar II
                                                    };

        public static readonly string[] MonthNames = { "Nisan", "Iyyar", "Sivan", "Tammuz", "Av", "Elul",
                                                "Tishri", "Marheshvan", "Kislev", "Tevet", "Shevat",
                                                "Adar", "Adar I", "Adar II" };

        public const int Epoch = -1373428;
		public const int Tishri = 7;
		public const int Nisan = 1;

		private int year, month, day;

		public int Year
		{
			set { year = value; }
			get { return year; }
		}


		public int Month
		{
			set
			{
				if (value < 1 || value > GetLastMonthOfYear(year))
				{
					throw new ArgumentOutOfRangeException();
				}
                month = value;
			}

			get { return month; }
		}


        public string MonthName
        {
            get { return GetMonthName(Month, Year); }
        }

        public string HebrewMonthName
        {
            get { return GetHebrewMonthName(Month, Year); }
        }
        

		public int Day
		{
			set
			{
				if (value < 1 || value > GetLastDayOfMonth(month, year))
				{
					throw new ArgumentOutOfRangeException();
				}
				day = value;
			}

			get { return day; }
		}


        public static string GetMonthName(int month, int year)
        {
            if (month <= 11)
            {
                return MonthNames[month - 1];
            }
            else
            {
                if (IsLeapYear(year))
                {
                    return MonthNames[month];
                }
                else
                {
                    return MonthNames[month - 1];
                }
            }
        }


        public static string GetHebrewMonthName(int month, int year)
        {
            if (month <= 11)
            {
                return HebrewMonthNames[month - 1];
            }
            else
            {
                if (IsLeapYear(year))
                {
                    return HebrewMonthNames[month];
                }
                else
                {
                    return HebrewMonthNames[month - 1];
                }
            }
        }

		public HebrewDate()
		{
			FromFixed(Epoch);
		}

		
        public HebrewDate(int fd)
		{
			FromFixed(fd);
		}


        public HebrewDate(IFixedDate fd)
            : this(fd.ToFixed())
        {

        }


		public HebrewDate(int year, int month, int day)
		{
			Year = year;
			Month = month;
			Day = day;
		}


		public void FromFixed(int fd)
		{
			int approx = (int)( new Rational(fd - Epoch) / new Rational(35975351, 98496) ).Floor() + 1;

            int y = approx - 1;
			bool done = false;
			while (!done)
			{
				if ( (double) GetNewYear(y) <= (double) fd)
				{
					year = y;
					y++;
				}
				else
				{
					done = true;
				}
			}

			HebrewDate hd = new HebrewDate(year, Nisan, 1);
			int start = (fd < hd.ToFixed()) ? Tishri : Nisan;

			month = GetLastMonthOfYear(year);

			for (int m = GetLastMonthOfYear(year); m >= start; m--)
			{
				hd = new HebrewDate(year, m, GetLastDayOfMonth(m, year));
				if (fd <= hd.ToFixed())
				{
					Month = hd.Month;
				}
			}

			hd = new HebrewDate(year, month, 1);
			day = fd - hd.ToFixed() + 1;
		}


		public int ToFixed()
		{
			int returnme = GetNewYear(year) + day /*- 1*/;

            //int ny = GetNewYear(year);
            //Rational nyr = GetNewYearRational(year);
            //nyr.MakeProper();

			if (month < Tishri)
			{
				for (int m = Tishri; m <= GetLastMonthOfYear(year); m++)
				{
					returnme += GetLastDayOfMonth(m, year);
				}

				for (int m = Nisan; m < month; m++)
				{
					returnme += GetLastDayOfMonth(m, year);
				}
			}
			else
			{
				for (int m = Tishri; m < month; m++)
				{
					returnme += GetLastDayOfMonth(m, year);
				}
			}

			return returnme;
		}


		public override string ToString()
		{
			return String.Format("{0} {1}, {2}", GetMonthName(Month, Year), Day, Year);
		}


		public static bool IsLeapYear(int year)
		{
            return (DateUtils.Mod(7 * year + 1, 19) < 7) ? true : false;
		}


		public static int GetLastMonthOfYear(int year)
		{
			return (IsLeapYear(year) ? 13 : 12);
		}


		public static double GetMolad(int hebrewMonth, int hebrewYear)
		{
			int y = (hebrewMonth < Tishri) ? (hebrewYear + 1) : hebrewYear;
            int monthsElapsed = hebrewMonth - Tishri + DateUtils.Floor(235 * y - 234, 19);

			double molad = (double)Epoch - 876.0f / 25920.0f +
				(double)monthsElapsed * ( (double) 29.0f + 0.5f + 793.0f / 25920.0f);

			return molad;
		}


        public static Rational GetMoladRational(int hebrewMonth, int hebrewYear)
        {
            int y = (hebrewMonth < Tishri) ? (hebrewYear + 1) : hebrewYear;
            Rational monthsElapsed = new Rational(hebrewMonth) - new Rational(Tishri) + 
                new Rational(235 * y - 234, 19).Floor();

            Rational molad = new Rational(Epoch) - new Rational(876, 25920) +
                monthsElapsed * (new Rational(29, 1, 2) + new Rational(793, 25920));

            return molad;
        }


		public static int GetElapsedDays(int hebrewYear)
		{
            int monthsElapsed = DateUtils.Floor(235 * hebrewYear - 234, 19);
			long partsElapsed = 12084L + 13753L * (long)monthsElapsed;
            int day = 29 * monthsElapsed + (int)DateUtils.Floor(partsElapsed, 25920L);
            return (DateUtils.Mod(3 * (day + 1), 7) < 3) ? (day + 1) : day;
		}


        public static Rational GetElapsedDaysRational(int hebrewYear)
        {
            Rational r = GetMoladRational(Tishri, hebrewYear) - new Rational(Epoch) + new Rational(1, 2);
            return r.Floor();
        }


		public static int GetNewYearDelay(int hebrewYear)
		{
			int returnme;

			int ny0 = GetElapsedDays(hebrewYear - 1);
			int ny1 = GetElapsedDays(hebrewYear);
			int ny2 = GetElapsedDays(hebrewYear + 1);

			if ((ny2 - ny1) == 356)
			{
				returnme = 2;
			}
			else if ((ny1 - ny0) == 382)
			{
				returnme = 1;
			}
			else
			{
				returnme = 0;
			}

			return returnme;
		}


		public static int GetNewYear(int hebrewYear)
		{
			return Epoch + GetElapsedDays(hebrewYear) + GetNewYearDelay(hebrewYear);
		}


        public static Rational GetNewYearRational(int hebrewYear)
        {
            return new Rational(Epoch) + GetElapsedDaysRational(hebrewYear) + GetNewYearDelay(hebrewYear);
        }

         
		public static int GetLastDayOfMonth(int hebrewMonth, int hebrewYear)
		{
			switch (hebrewMonth)
			{
			case 2:
			case 4:
			case 6:
			case 10:
			case 13:
				return 29;

			case 12:
				return IsLeapYear(hebrewYear) ? 30 : 29;

			case 8:
				return IsLongMarheshvan(hebrewYear) ? 30 : 29;

			case 9:
				return IsShortKislev(hebrewYear) ? 29 : 30;

			default:
				return 30;
			}
		}


		public static bool IsLongMarheshvan(int hebrewYear)
		{
			switch (GetDaysInYear(hebrewYear))
			{
			case 355:
			case 385:
				return true;

			default:
				return false;
			}
		}


		public static bool IsShortKislev(int hebrewYear)
		{
			switch (GetDaysInYear(hebrewYear))
			{
			case 353:
			case 383:
				return true;

			default:
				return false;
			}

		}


		public static int GetDaysInYear(int hebrewYear)
		{
			return GetNewYear(hebrewYear + 1) - GetNewYear(hebrewYear);
		}


		public static HebrewDate GetYomKippur(int gregorianYear)
		{
			int hebrewYear = gregorianYear -
				GregorianDate.GetYearFromFixed(Epoch) + 1;

			return new HebrewDate(hebrewYear, Tishri, 10);
		}


		public static HebrewDate GetRoshHaShanah(int gregorianYear)
		{
			int hebrewYear = gregorianYear -
				GregorianDate.GetYearFromFixed(Epoch) + 1;

			return new HebrewDate(hebrewYear, Tishri, 1);
		}


		public static HebrewDate GetSukkot(int gregorianYear)
		{
			int hebrewYear = gregorianYear -
				GregorianDate.GetYearFromFixed(Epoch) + 1;

			return new HebrewDate(hebrewYear, Tishri, 15);
		}


		public static HebrewDate GetHoshanaRabba(int gregorianYear)
		{
			int hebrewYear = gregorianYear -
				GregorianDate.GetYearFromFixed(Epoch) + 1;

			return new HebrewDate(hebrewYear, Tishri, 21);
		}


		public static HebrewDate GetSheminiAzeret(int gregorianYear)
		{
			int hebrewYear = gregorianYear -
				GregorianDate.GetYearFromFixed(Epoch) + 1;

			return new HebrewDate(hebrewYear, Tishri, 22);
		}


		public static HebrewDate GetSimhatTorah(int gregorianYear)
		{
			int hebrewYear = gregorianYear -
				GregorianDate.GetYearFromFixed(Epoch) + 1;

			return new HebrewDate(hebrewYear, Tishri, 23);
		}


		public static HebrewDate GetPassover(int gregorianYear)
		{
			int hebrewYear = gregorianYear -
				GregorianDate.GetYearFromFixed(Epoch);

			return new HebrewDate(hebrewYear, Nisan, 15);
		}


		public static HebrewDate GetShavuot(int gregorianYear)
		{
			int hebrewYear = gregorianYear -
				GregorianDate.GetYearFromFixed(Epoch);

			return new HebrewDate(hebrewYear, 3, 21);
		}


		public struct OmerType
		{
			public bool valid;
			public int completedWeeks;
			public int excessDays;
		}


		public static OmerType GetOmer(int fd)
		{
			OmerType returnme;

			int c = fd - GetPassover(GregorianDate.GetYearFromFixed(fd)).ToFixed();

			if (c >= 1 && c <= 49)
			{
				returnme.valid = true;
                returnme.completedWeeks = DateUtils.Floor(c, 7);
                returnme.excessDays = DateUtils.Mod(c, 7);
			}
			else
			{
				returnme.valid = false;
				returnme.completedWeeks = 0;
				returnme.excessDays = 0;
			}

			return returnme;
		}


		public static HebrewDate GetPurim(int gregorianYear)
		{
			int hebrewYear = gregorianYear -
				GregorianDate.GetYearFromFixed(Epoch);

			int lastMonth = GetLastMonthOfYear(hebrewYear);

			return new HebrewDate(hebrewYear, lastMonth, 14);
		}


		public static HebrewDate GetTaAnitEsther(int gregorianYear)
		{
			HebrewDate purim = new HebrewDate(GetPurim(gregorianYear));

			HebrewDate returnme;

            if (DateUtils.GetDayOfWeek(purim.ToFixed()) == DateUtils.Weekday.Sun)
			{
				returnme = new HebrewDate(purim.ToFixed() - 3);
			}
			else
			{
				returnme = new HebrewDate(purim.ToFixed() - 1);
			}

			return returnme;
		}


		public static HebrewDate GetTishahBeAv(int gregorianYear)
		{
			int hebrewYear = gregorianYear -
				GregorianDate.GetYearFromFixed(Epoch);

			HebrewDate av9 = new HebrewDate(hebrewYear, 5, 9);

			HebrewDate returnme;

            if (DateUtils.GetDayOfWeek(av9.ToFixed()) == DateUtils.Weekday.Sat)
			{
				returnme = new HebrewDate(av9.ToFixed() + 1);
			}
			else
			{
				returnme = new HebrewDate(av9);
			}

			return returnme;
		}


		public static HebrewDate GetYomHaZikkaron(int gregorianYear)
		{
			int hebrewYear = gregorianYear -
				GregorianDate.GetYearFromFixed(Epoch);

			HebrewDate iyyar4 = new HebrewDate(hebrewYear, 2, 4);

			HebrewDate returnme;

            if (DateUtils.Weekday.Wed < DateUtils.GetDayOfWeek(iyyar4.ToFixed()))
			{
                returnme = new HebrewDate(DateUtils.WeekdayBefore(iyyar4.ToFixed(), DateUtils.Weekday.Wed));
			}
			else
			{
				returnme = iyyar4;
			}

			return returnme;
		}


		public static HebrewDate GetShEla(int gregorianYear)
		{
			return new HebrewDate(new CopticDate(gregorianYear, 3, 26).ToFixed());
		}


		public static List<HebrewDate> GetBirkathHaHama(int gregorianYear)
		{
			List<HebrewDate> returnme = new List<HebrewDate>();

			List<CopticDate> dates = CopticDate.GetCopticInGregorian(gregorianYear, 7, 30);

			if (dates.Count != 0)
			{
				if (DateUtils.Mod(dates[0].Year, 28) == 17)
				{
                    foreach (CopticDate cd in dates)
                    {
                        returnme.Add( new HebrewDate(cd.ToFixed()));
                    }
				}
			}

			return returnme;
		}


        public static IEnumerable<Event> GetHebrewEvents(int gregorianYear)
        {
            List<Event> events = new List<Event>();

            events.Add(new Event(GetYomKippur(gregorianYear), "Yom Kippur"));
            events.Add(new Event(GetRoshHaShanah(gregorianYear), "Rosh Ha Shanah") );
            events.Add(new Event(GetSukkot(gregorianYear), "Sukkot"));
            events.Add(new Event(GetHoshanaRabba(gregorianYear), "Hoshana Rabba"));
            events.Add(new Event(GetSheminiAzeret(gregorianYear), "Shemini Azeret"));
            events.Add(new Event(GetSimhatTorah(gregorianYear), "Simhat Torah"));
            events.Add(new Event(GetPassover(gregorianYear), "Passover"));
            events.Add(new Event(GetShavuot(gregorianYear), "Shavuot"));

            HebrewDate passover = GetPassover(gregorianYear);
            for (int fd = passover.ToFixed(); fd <= passover.ToFixed() + 50; fd++)
            {
                var omer = GetOmer(fd);
                if (omer.valid)
                {
                    events.Add(new Event(fd, String.Format("Omer {0} ({1}w{2}d)", fd - passover.ToFixed(), omer.completedWeeks, omer.excessDays)));
                }
            }

            events.Add(new Event(GetPurim(gregorianYear), "Purim"));
            events.Add(new Event(GetTaAnitEsther(gregorianYear), "Ta Anit Esther"));
            events.Add(new Event(GetTishahBeAv(gregorianYear), "Tishah Be Av"));
            events.Add(new Event(GetYomHaZikkaron(gregorianYear), "Yom Ha Zikkaron"));
            events.Add(new Event(GetShEla(gregorianYear), "Sh Ela"));
            
            foreach (var fd in GetBirkathHaHama(gregorianYear))
            {
                events.Add(new Event(fd, "Birkath Ha Hama"));
            }

            return events;
        }


		public static HebrewDate GetHebrewBirthday(HebrewDate birthdate, int hebrewYear)
		{
			HebrewDate returnme;

			if (birthdate.Month == GetLastMonthOfYear(birthdate.Year))
			{
				returnme = new HebrewDate(hebrewYear, GetLastMonthOfYear(hebrewYear),
					birthdate.Day);
			}
			else
			{
				HebrewDate temp = new HebrewDate(hebrewYear, birthdate.Month, 1);
				returnme = new HebrewDate(temp.ToFixed() + birthdate.Day - 1);
			}

			return returnme;
		}


		public static List<HebrewDate> GetHebrewBirthdayInGregorian(HebrewDate birthdate, int gregorianYear)
		{
			List<HebrewDate> returnme = new List<HebrewDate>();

			GregorianDate jan1 = new GregorianDate(gregorianYear, 1, 1);
			GregorianDate dec31 = new GregorianDate(gregorianYear, 12, 31);

			HebrewDate hd = new HebrewDate(jan1.ToFixed());

			for (int i = hd.Year; i <= hd.Year + 1; i++)
			{
				HebrewDate date = GetHebrewBirthday(birthdate, i);

				if (date.ToFixed() >= jan1.ToFixed() && date.ToFixed() <= dec31.ToFixed())
				{
					returnme.Add(date);
				}
			}

			return returnme;
		}


		public static HebrewDate GetYahrzeit(HebrewDate deathdate, int hebrewYear)
		{
			HebrewDate returnme;

			if (deathdate.Month == 8 && deathdate.Day == 30 &&
				!IsLongMarheshvan(deathdate.Year + 1))
			{
				HebrewDate temp = new HebrewDate(hebrewYear, 9, 1);
				returnme = new HebrewDate(temp.ToFixed() - 1);
			}
			else if (deathdate.Month == 9 && deathdate.Day == 30 &&
				IsShortKislev(deathdate.Year + 1))
			{
				HebrewDate temp = new HebrewDate(hebrewYear, 10, 1);
				returnme = new HebrewDate(temp.ToFixed() - 1);
			}
			else if (deathdate.Month == 13)
			{
				returnme = new HebrewDate(hebrewYear, GetLastMonthOfYear(hebrewYear),
					deathdate.Day);
			}
			else if (deathdate.Day == 30 && deathdate.Month == 12 &&
				!IsLeapYear(hebrewYear))
			{
				returnme = new HebrewDate(hebrewYear, 11, 30);
			}
			else
			{
				HebrewDate temp = new HebrewDate(hebrewYear, deathdate.Month, 1);
				returnme = new HebrewDate(temp.ToFixed() + deathdate.Day - 1);
			}

			return returnme;
		}


		public static List<HebrewDate> GetYahrzeitInGregorian(HebrewDate deathdate, int gregorianYear)
		{
			List<HebrewDate> returnme = new List<HebrewDate>();

			GregorianDate jan1 = new GregorianDate(gregorianYear, 1, 1);
			GregorianDate dec31 = new GregorianDate(gregorianYear, 12, 31);

			HebrewDate hd = new HebrewDate(jan1);

			for (int i = hd.Year; i <= hd.Year + 1; i++)
			{
				HebrewDate date = GetYahrzeit(deathdate, i);

				if (date.ToFixed() >= jan1.ToFixed() && date.ToFixed() <= dec31.ToFixed())
				{
					returnme.Add(date);
				}
			}

			return returnme;
		}

	}

}

﻿
using System;
using System.Collections.Generic;
using System.Text;


namespace calendarnet
{
	public class IslamicDate : FixedDateBase, IFixedDate
	{
		public const int Epoch = 227015;
		private int year, month, day;
		public readonly string[] MonthNames = { "Muharram", "Safar", "Rabi' I",
		    "Rabi' II", "Jumada I", "Jumada II", "Rajab", "Sha'ban",
		    "Ramadan", "Shawwal", "Dhu al-Qa'da", "Dhu al-Hijja" };
        public readonly string[] WeekDayNames = { "yaum al-ahad", "yaum al-ithnayna",
					"yaum ath-thalatha", "yaum al-arba'a'", "yaum al-hamis", 
					"yaum al-jum'a", "yaum as-sabt" };

		public int Year
		{
			set { year = value; }
			get { return year; }
		}


		public int Month
		{
			set
			{
				if (value < 1 || value > 12)
				{
					throw new ArgumentOutOfRangeException();
				}
				month = value;
			}

			get { return month; }
		}

		public string MonthName
		{
			get
			{

				return MonthNames[month - 1];
			}
		}


		public int Day
		{
			set
			{
				int maxday = 0;

				switch (month)
				{
				case 1:
				case 3:
				case 5:
				case 7:
				case 9:
				case 11:
					maxday = 30;
					break;

				case 2:
				case 4:
				case 6:
				case 8:
				case 10:
					maxday = 29;
					break;

				case 12:
					maxday = IsLeapYear(year) ? 30 : 29;
					break;

				default:
					// this shouldn't happen
					throw new ArgumentOutOfRangeException();
				}

				if (value < 1 || value > maxday)
				{
					throw new ArgumentOutOfRangeException();
				}

				day = value;
			}

			get { return day; }
		}


		public string WeekdayName
		{
			get
			{
                return WeekDayNames[(int) DateUtils.GetDayOfWeek(ToFixed())];
			}
		}


		public IslamicDate() : this(Epoch)
		{
			
		}


		public IslamicDate(int fd)
		{
			FromFixed(fd);
		}


		public IslamicDate(int year, int month, int day)
		{
			Year = year;
			Month = month;
			Day = day;
		}


		public void FromFixed(int fd)
		{
            Year = DateUtils.Floor(30 * (fd - Epoch) + 10646, 10631);

			IslamicDate id1 = new IslamicDate(year, 1, 1);
			int priorDays = fd - id1.ToFixed();
            Month = DateUtils.Floor(11 * priorDays + 330, 325);

			IslamicDate id2 = new IslamicDate(year, month, 1);
			Day = fd - id2.ToFixed() + 1;
		}


		public int ToFixed()
		{
            return day + 29 * (month - 1) + DateUtils.Floor(6 * month - 1, 11)
                + (year - 1) * 354 + DateUtils.Floor(3 + 11 * year, 30) + Epoch - 1;
		}


		public override string ToString()
		{
			return String.Format("{0}({1}) {2}, {3}", MonthName, Month, Day, Year);
		}


		public static bool IsLeapYear(int year)
		{
            return (DateUtils.Mod(14 + 11 * year, 30) < 11);
		}


		List<IslamicDate> GetIslamicInGregorian(int gregorianYear, int islamicMonth, int islamicDay)
		{
			List<IslamicDate> returnme = new List<IslamicDate>();

			GregorianDate jan1 = new GregorianDate(gregorianYear, 1, 1);
			GregorianDate dec31 = new GregorianDate(gregorianYear, 12, 31);

			IslamicDate id = new IslamicDate(jan1.ToFixed());
			int y = id.Year;

			for (int i = y; i <= y + 2; i++)
			{
				IslamicDate date = new IslamicDate(i, islamicMonth, islamicDay);

				if (date.ToFixed() >= jan1.ToFixed() && date.ToFixed() <= dec31.ToFixed())
				{
					returnme.Add(date);
				}
			}

			return returnme;
		}

	}
}

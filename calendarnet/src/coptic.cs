﻿
using System;
using System.Collections.Generic;
using System.Text;


namespace calendarnet
{
	public class CopticDate : FixedDateBase, IFixedDate
	{
		private int month, day, year;
		public const int Epoch = 103605;
		public readonly string[] MonthNames = { "Thoout", "Paope", "Athor", "Koiak",
			"Tobe", "Meshir", "Paremotep", "Parmoute", "Pashons", "Paone", "Epep",
			"Mesore", "Epagomene" };
		public readonly string[] DaysOfWeekNames = { "Tkyriake", "Pesnau", "Pshoment",
					"Peftoou", "Ptiou", "Psoou", "Psabbaton" };

		public int Year
		{
			set { year = value; }
			get { return year; }
		}


		public int Month
		{
			set
			{
				if (value < 1 || value > 13)
				{
					throw new ArgumentOutOfRangeException();
				}
				month = value;
			}

			get { return month; }
		}


		public string MonthName
		{
			get
			{

				return MonthNames[month - 1];
			}
		}


		public int Day
		{
			set
			{
				if (value >= 0 && value <= ((Month < 13) ? 30 : (IsLeapYear(Year)) ? 6 : 5))
				{
					day = value;
				}
				else
				{
					throw new ArgumentOutOfRangeException();
				}
			}

			get { return day; }
		}


		public string WeekdayName
		{
			get
			{
                return DaysOfWeekNames[(int) DateUtils.GetDayOfWeek(ToFixed())];
			}
		}


		public CopticDate() : this(Epoch)
		{
			
		}


		public CopticDate(int fd)
		{
			FromFixed(fd);
		}


		public CopticDate(int year, int month, int day)
		{
			Year = year;
			Month = month;
			Day = day;
		}


		public void FromFixed(int fd)
		{
			year = DateUtils.Floor(4 * (fd - Epoch) + 1463, 1461);

			CopticDate cd = new CopticDate(year, 1, 1);
			month = DateUtils.Floor(fd - cd.ToFixed(), 30) + 1;

			cd = new CopticDate(year, month, 1);
			day = fd + 1 - cd.ToFixed();
		}


		public int ToFixed()
		{
			int returnme = Epoch - 1;
			returnme += 365 * (year - 1);
			returnme += DateUtils.Floor(year, 4);
			returnme += 30 * (month - 1);
			returnme += day;
			return returnme;
		}


		public override string ToString()
		{
			return String.Format("{0}({1}) {2}, {3} A.M.",
				MonthName, Month, Day, Year);
		}


		public static List<CopticDate> GetCopticInGregorian(int gregorianYear,
			int copticMonth, int copticDay)
		{
			List<CopticDate> returnme = new List<CopticDate>();

			GregorianDate jan1gd = new GregorianDate(gregorianYear, 1, 1);
			int jan1 = jan1gd.ToFixed();

			GregorianDate dec31gd = new GregorianDate(gregorianYear, 12, 31);
			int dec31 = dec31gd.ToFixed();

			CopticDate cd = new CopticDate(jan1);
			int y = cd.Year;

			CopticDate cd1 = new CopticDate(y, copticMonth, copticDay);
			int date1 = cd1.ToFixed();

			CopticDate cd2 = new CopticDate(y + 1, copticMonth, copticDay);
			int date2 = cd2.ToFixed();

			if (date1 >= jan1 && date1 <= dec31)
			{
				returnme.Add(new CopticDate(date1));
			}

			if (date2 >= jan1 && date2 <= dec31)
			{
				returnme.Add(new CopticDate(date2));
			}

			return returnme;
		}


		public static List<CopticDate> GetCopticChristmas(int gregorianYear)
		{
			return GetCopticInGregorian(gregorianYear, 4, 29);
		}


		public static bool IsLeapYear(int copticYear)
		{
			return DateUtils.Mod(copticYear, 4) == 3;
		}

	}

}


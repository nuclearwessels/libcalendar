﻿
using System;
using System.Collections.Generic;
using System.Text;


namespace calendarnet
{
	public class EgyptianDate : FixedDateBase, IFixedDate
	{
		public const int Epoch = -272787;

		protected int year, month, day;

		public int Year
		{
			set { year = value; }
			get { return year; }
		}


		public int Month
		{
			set
			{
				if (value > 13 || value < 1)
				{
					throw new ArgumentOutOfRangeException("Invalid month.");
				}

				month = value;
			}

			get { return month; }
		}


		public int Day
		{
			set
			{
				if (value < 1)
				{
					throw new ArgumentOutOfRangeException();
				}

				if ((month < 13 && value > 30) || (month == 13 && value > 5))
				{
					throw new ArgumentOutOfRangeException();
				}

				day = value;
			}

			get { return day; }
		}

		public EgyptianDate()
			: this(Epoch)
		{

		}

		public EgyptianDate(int fd)
		{
			FromFixed(fd);
		}


        public EgyptianDate(IFixedDate fd)
            : this(fd.ToFixed())
        {

        }


		public EgyptianDate(int year, int month, int day)
		{
			Year = year;
			Month = month;
			Day = day;
		}


		public void FromFixed(int fd)
		{
			int days = fd - Epoch;
			Year = DateUtils.Floor(days, 365) + 1;
			Month = DateUtils.Floor(DateUtils.Mod(days, 365), 30) + 1;
			Day = days - 365 * (year - 1) - 30 * (month - 1) + 1;
		}

		public int ToFixed()
		{
			return Epoch + 365 * (year - 1) + 30 * (month - 1) + day - 1;
		}


		public override string ToString()
		{
			return String.Format("year {0} month {1} day {2}", year, month, day);
		}
	}
}


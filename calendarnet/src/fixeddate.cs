﻿
using System;
using System.Collections.Generic;
using System.Text;


namespace calendarnet
{
    public class Event : IComparable<Event>
    {
        private int fd;
        private string eventName;

        public int FixedDate
        {
            set { fd = value; }
            get { return fd; }
        }


        public string EventName
        {
            set { eventName = value; }
            get { return eventName; }
        }


        public Event(int fd, string eventName)
        {
            this.fd = fd;
            this.eventName = eventName;
        }


        public Event(IFixedDate fd, string eventName)
            : this(fd.ToFixed(), eventName)
        {

        }


        #region IComparable<Event> Members

        public int CompareTo(Event other)
        {
            if (FixedDate < other.FixedDate)
            {
                return -1;
            }
            else if (FixedDate > other.FixedDate)
            {
                return 1;
            }
            else
            {
                return EventName.CompareTo(other.EventName);
            }
        }

        #endregion


        


    }


    public interface IFixedDate
    {
        int ToFixed();
        void FromFixed(int fd);
    }


    public abstract class FixedDateBase
    {

        //public static int operator +(IFixedDate fd, int days)
        //{
        //    return fd.ToFixed() + days;
        //}


        //public static int operator -(IFixedDate fd, int days)
        //{
        //    return fd.ToFixed() - days;
        //}


        //public static implicit operator int(IFixedDate fd)
        //{
        //    return fd.ToFixed();
        //}

    }



	public static class DateUtils
	{
		public enum Weekday { Sun = 0, Mon, Tues, Wed, Thu, Fri, Sat };

        public static readonly string[] WeekdayNames = { "Sunday", "Monday", "Tuesday", "Wednesday",
                                                         "Thursday", "Friday", "Saturday" };


		public static Weekday GetDayOfWeek(int fixedDate)
		{
			return (Weekday)(Mod(fixedDate, 7));
		}


		public static int WeekdayOnOrBefore(int fd, Weekday weekday)
		{
			int date = fd - (int)weekday;
			return fd - (int)GetDayOfWeek(date);
		}


		public static int WeekdayOnOrAfter(int fd, Weekday weekday)
		{
			int date = fd + 6;
			return WeekdayOnOrBefore(date, weekday);
		}


		public static int WeekdayNearest(int fd, Weekday weekday)
		{
			int date = fd + 3;
			return WeekdayOnOrBefore(date, weekday);
		}


		public static int WeekdayBefore(int fd, Weekday weekday)
		{
			int date = fd - 1;
			return WeekdayOnOrBefore(date, weekday);
		}


		public static int WeekdayAfter(int fd, Weekday weekday)
		{
			int date = fd + 7;
			return WeekdayOnOrBefore(date, weekday);
		}


        public static int NthWeekday(int fd, int n, Weekday weekday)
		{
			if (n > 0)
			{
				return 7 * n + WeekdayBefore(fd, weekday);
			}
			else
			{
				return 7 * n + WeekdayAfter(fd, weekday);
			}
		}


		public static int FirstWeekday(int fd, Weekday weekday)
		{
			return NthWeekday(fd, 1, weekday);
		}


		public static int LastWeekday(int fd, Weekday weekday)
		{
			return NthWeekday(fd, -1, weekday);
		}


		public static int Floor(int a, int b)
		{
			int remainder;
			int quotient = DivRem(a, b, out remainder);
			if (a < 0)
			{
				return (remainder != 0) ? (quotient - 1) : quotient;
			}
			else
			{
				return quotient;
			}
		}


		public static long Floor(long a, long b)
		{
			long remainder;
			long quotient = DivRem(a, b, out remainder);
			if (a < 0)
			{
				return (remainder != 0) ? (quotient - 1) : quotient;
			}
			else
			{
				return quotient;
			}
		}


		public static double Mod(double x, double y)
		{
			return x - y * Math.Floor(x / y);
		}


		public static int Mod(int x, int y)
		{
			return x - y * Floor(x, y);
		}


		public static int Amod(int x, int y)
		{
			int r = Mod(x, y);
			return (r == 0) ? y : r;
		}


		public static int Quotient(double x, double y)
		{
			return (int)Math.Floor(x / y);
		}


        public static int DivRem(int dividend, int divisor, out int remainder)
        {
            int quotient = dividend / divisor;
            remainder = dividend - divisor * quotient;
            return quotient;
        }

        
        public static long DivRem(long dividend, long divisor, out long remainder)
        {
            long quotient = dividend / divisor;
            remainder = dividend - divisor * quotient;
            return quotient; 
        }

	}

}


﻿using System;
using System.Collections.Generic;
using System.Text;


namespace calendarnet
{
	public abstract class OldHinduBase : FixedDateBase
	{
		public const int Epoch = -1132959;
		public static readonly Rational AryaSolarYear = new Rational(365, 149, 576);
		public static readonly Rational AryaSolarMonth = AryaSolarYear / 12;
		public static readonly Rational AryaJovianPeriod = new Rational(1577917500, 364224);

		public static readonly string[] JovianCycles = { 
			"Vijaya", "Jaya", "Manmatha", "Durmukha", "Hemalamba",
			"Vilamba", "Vikarin", "Sarvari", "Plava", "Subhakrit",
			"Sobhana", "Krodhin", "Visvavasu", "Parabhava", "Plavanga",
			"Kilaka", "Saumya", "Sadharana", "Virodhakrit", "Paridhavin", 
			"Pramadin", "Ananda", "Rakshasa", "Anala", "Rakshasa",
			"Anala", "Pingala", "Kalayukta", "Siddharthin", "Raudra", "Durmati", "Dundubhi",
			"Rudhirodgarin", "Raktaksha", "Krodhana", "Kshaya", "Prabhava",
			"Vibhava", "Sukla", "Pramoda", "Prajapati", "Angiras", 
			"Srumukha", "Bhava", "Yuvan", "Dhatri", "Isvara",
			"Bahudhanya", "Pramathin", "Vikrama", "Vrisha", "Chitrabhanu",
			"Subhanu", "Tarana", "Parthiva", "Vyaya", "Sarvajit", 
			"Sarvadharin", "Virodhin", "Vikrita", "Khara", "Nandana" };

		public static int GetHinduDayCount(int fd)
		{
			return fd - Epoch;
		}


		public static int GetJovianYear(int fd)
		{
			Rational a = new Rational(GetHinduDayCount(fd), 0, 1);

			Rational a2 = a / (AryaJovianPeriod / 12);

			long b = a2.Whole;
			return DateUtils.Mod((int)b, 60) + 1;
		}


	}


	public class OldHinduSolarDate : OldHinduBase, IFixedDate
	{
		private int year, month, day;

		public static readonly string[] MonthNames = { "Mesha", "Vrishabha",
			"Mithuna", "Karka", "Simha", "Kanya", "Tula", "Vrischika",
			"Dhanu", "Makara", "Kumbha", "Mina" };

		public int Year
		{
			set { year = value; }
			get { return year; }
		}

		public int Month
		{
			set
			{
				if (value < 1 || value > 12)
				{
					throw new ArgumentOutOfRangeException();
				}
				month = value;
			}

			get { return month; }
		}


		public int Day
		{
			set
			{
				if (value < 1 || value > 31)
				{
					throw new ArgumentOutOfRangeException();
				}
				day = value;
			}
			get { return day; }
		}


		public OldHinduSolarDate() : this(Epoch)
		{
			
		}


		public OldHinduSolarDate(int year, int month, int day)
		{
			Year = year;
			Month = month;
			Day = day;
		}


		public OldHinduSolarDate(int fd)
		{
			FromFixed(fd);
		}


		public void FromFixed(int fd)
		{
			Rational sun = new Rational(GetHinduDayCount(fd), 0, 1) +
				new Rational(1, 4);

			Year = (int)Math.Floor(sun / AryaSolarYear);
			Month = (int) DateUtils.Mod(Math.Floor(sun / AryaSolarMonth), 12.0f) + 1;
			Day = (int) Math.Floor(DateUtils.Mod(sun, AryaSolarMonth)) + 1;
		}


		public int ToFixed()
		{
			Rational s = new Rational(Epoch, 0, 1);
			s += AryaSolarYear * year;
			s += AryaSolarMonth * (month - 1);
			s += day;
			s -= new Rational(5, 4);

			return (int)Math.Ceiling(s);
		}


		public override string ToString()
		{
			return String.Format("{0}({1}) {2}, {3}", MonthNames[month-1], month, day, year);
		}

	}


	public class OldHinduLunarDate : OldHinduBase
	{
		public static readonly Rational AryaLunarMonth = new Rational(1577917500, 5343336);
		public static readonly Rational AryaLunarDay = AryaLunarMonth / 30;

		private int year, month, day;
		private bool leap;

		public int Year
		{
			set { year = value; }
			get { return year; }
		}


		public int Month
		{
			set 
			{
				if (value < 1 || value > 12)
				{
					throw new ArgumentOutOfRangeException();
				}
				month = value; 
			}
			get { return month; }
		}


		public int Day
		{
			set
			{
				if (value < 1 || value > 30)
				{
					throw new ArgumentOutOfRangeException();
				}
				day = value;
			}

			get { return day; }
		}


		public bool Leap
		{
			set { leap = value; }
			get { return leap; }
		}


		public OldHinduLunarDate() : this(Epoch)
		{
			
		}


		public OldHinduLunarDate(int fd)
		{
			FromFixed(fd);
		}


		public OldHinduLunarDate(int year, int month, bool leap, int day)
		{
			Year = year;
			Month = month;
			Leap = leap;
			Day = day;
		}
		

		public int ToFixed()
		{
			return 0;
		}


        private static readonly Rational OneQuarter = new Rational(0, 1, 4);

		public void FromFixed(int fd)
		{
            int hinduDayCount = GetHinduDayCount(fd);
			Rational sun = new Rational(GetHinduDayCount(fd)) + OneQuarter;
			Rational newMoon = sun - Rational.Mod(sun, AryaLunarMonth);
            //newMoon.Reduce();
            newMoon.MakeProper();

            Rational b = Rational.Mod(newMoon, AryaSolarMonth);
            //b.Reduce();
            Rational c = AryaSolarMonth - AryaLunarMonth;
            c.MakeProper();
            //c.Reduce();

            leap  = ((double) c >= (double) b) && ((double) b > 0);

            month = DateUtils.Mod((int)Math.Ceiling(newMoon / AryaSolarMonth), 12) + 1;

            day = (int)DateUtils.Mod(Rational.Floor(sun, AryaLunarDay), 30) + 1;

            Rational a1 = newMoon + AryaSolarMonth;
            Rational a =  a1 / AryaSolarYear;
            a.MakeProper();

            year = (int) Math.Ceiling( (newMoon + AryaSolarMonth) / AryaSolarYear) + 1;
		}


        private static readonly Rational LeapYearRational = new Rational(23902504679, 1282400064);

		public static bool IsLeapYear(int lunarYear)
		{
			return Rational.Mod(new Rational(lunarYear, 0, 1) * AryaSolarYear - AryaSolarMonth,
				AryaLunarMonth) >= LeapYearRational;
		}

	}


    public class OldHinduLunarFPDate : OldHinduBase
    {
        public new const double AryaJovianPeriod = 1577917500D / 364224D;

        public new const double AryaSolarYear = 365D + 149D / 576D;
        public new const double AryaSolarMonth = AryaSolarYear / 12D;

        public const double AryaLunarMonth = 1577917500D / 5343336D;
        public const double AryaLunarDay = AryaLunarMonth / 30D;

        private int year, month, day;
        private bool leap;

        public int Year
        {
            set { year = value; }
            get { return year; }
        }


        public int Month
        {
            set
            {
                if (value < 1 || value > 12)
                {
                    throw new ArgumentOutOfRangeException();
                }
                month = value;
            }
            get { return month; }
        }


        public int Day
        {
            set
            {
                if (value < 1 || value > 30)
                {
                    throw new ArgumentOutOfRangeException();
                }
                day = value;
            }

            get { return day; }
        }


        public bool Leap
        {
            set { leap = value; }
            get { return leap; }
        }


        public OldHinduLunarFPDate()
            : this(Epoch)
        {

        }


        public OldHinduLunarFPDate(int fd)
        {
            FromFixed(fd);
        }


        public OldHinduLunarFPDate(int year, int month, bool leap, int day)
        {
            Year = year;
            Month = month;
            Leap = leap;
            Day = day;
        }


        public int ToFixed()
        {
            return 0;
        }


        public void FromFixed(int fd)
        {
            int hinduDayCount = GetHinduDayCount(fd);
            double sun = (double) GetHinduDayCount(fd) + 0.25D;
            double newMoon = sun - DateUtils.Mod(sun, AryaLunarMonth);

            double b = DateUtils.Mod(newMoon, AryaSolarMonth);
            double c = AryaSolarMonth - AryaLunarMonth;

            leap = ( c >= b) && (b > 0D);

            double x = newMoon / AryaSolarMonth;
            double monthCeiling = Math.Ceiling(newMoon / AryaSolarMonth);

            month = DateUtils.Mod((int)monthCeiling, 12) + 1;

            day = (int)DateUtils.Mod(Math.Floor(sun / AryaLunarDay), 30D) + 1;

            double a1 = newMoon + AryaSolarMonth;
            double a = a1 / AryaSolarYear;

            year = (int)Math.Ceiling((newMoon + AryaSolarMonth) / AryaSolarYear) + 1;
        }


        private static readonly Rational LeapYearRational = new Rational(23902504679, 1282400064);

        public static bool IsLeapYear(int lunarYear)
        {
            return DateUtils.Mod(lunarYear * AryaSolarYear - AryaSolarMonth, AryaLunarMonth) >= LeapYearRational;
        }

    }



}


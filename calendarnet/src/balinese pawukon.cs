﻿

using System;
using System.Collections.Generic;


namespace calendarnet
{

public class BalinesePawukonDate : IFixedDate
{
    public static readonly int BaliEpoch = (new JulianDate() { JulianDay = 146 }).ToFixed();

    public bool BaliLuang { get; protected set; }
    public int BaliDwiwara { get; protected set; }
    public int BaliTriwara { get; protected set; }
    public int BaliCaturwara { get; protected set; }
    public int BaliPancawara { get; protected set; }
    public int BaliSadwara { get; protected set; }
    public int BaliSaptawara { get; protected set; }
    public int BaliAsatawara { get; protected set; }
    public int BaliSangawara { get; protected set; }
    public int BaliDasawara { get; protected set; }

    public BalinesePawukonDate() : this(BaliEpoch)
    {

    }

    public BalinesePawukonDate(int fd)
    {
        FromFixed(fd);
    }

    public BalinesePawukonDate(IFixedDate fd)
        : this(fd.ToFixed())
    {

    }

    public BalinesePawukonDate(bool luang, int dwiwara, int triwara, int caturwara, int pancawara,
                               int sadwara, int saptawara, int asatawara, int sangawara, int dasawara)
    {
        BaliLuang = luang;
        BaliDwiwara = dwiwara;
        BaliTriwara = triwara;
        BaliCaturwara = caturwara;
        BaliPancawara = pancawara;
        BaliSadwara = sadwara;
        BaliSaptawara = saptawara;
        BaliAsatawara = asatawara;
        BaliSangawara = sangawara;
        BaliDasawara = dasawara;
    }

    public int ToFixed()
    {
        throw new InvalidOperationException();
    }

    public void FromFixed(int fd)
    {
        BaliLuang = GetBaliLuangFromFixed(fd);
        BaliDwiwara = GetBaliDwiwaraFromFixed(fd);
        BaliTriwara = GetBaliTriwaraFromFixed(fd);
        BaliCaturwara = GetBaliCaturawaFromFixed(fd);
        BaliPancawara = GetBaliPancawaraFromFixed(fd);
        BaliSadwara = GetBaliSadwaraFromFixed(fd);
        BaliSaptawara = GetBaliSaptawaraFromFixed(fd);
        BaliAsatawara = GetBaliAsatawaraFromFixed(fd);
        BaliSangawara = GetBaliSangawaraFromFixed(fd);
        BaliDasawara = GetBaliDasawaraFromFixed(fd);
    }

    public override string ToString()
    {
        return String.Format("Luang={0}, Dwiwara={1}, Triwara={2}, Caturwara={3}, Pancawara={4}, Sadwara={5}, " +
                             "Saptawara={6}, Asatawara={7}, Sangawara={8}, Dasawara={9}",
                             BaliLuang,
                             BaliDwiwara,
                             BaliTriwara,
                             BaliCaturwara,
                             BaliPancawara,
                             BaliSadwara,
                             BaliSaptawara,
                             BaliAsatawara,
                             BaliSangawara,
                             BaliDasawara);
    }

    public static int GetBaliDayFromFixed(int fd)
    {
        return DateUtils.Mod(fd - BaliEpoch, 210);
    }
    
    public static int GetBaliTriwaraFromFixed(int fd)
    {
        return DateUtils.Mod(GetBaliDayFromFixed(fd), 3) + 1;
    }

    public static int GetBaliSadwaraFromFixed(int fd)
    {
        return DateUtils.Mod(GetBaliDayFromFixed(fd), 6) + 1;
    }

    public static int GetBaliSaptawaraFromFixed(int fd)
    {
        return DateUtils.Mod(GetBaliDayFromFixed(fd), 7) + 1;
    }

    public static int GetBaliPancawaraFromFixed(int fd)
    {
        return DateUtils.Mod(GetBaliDayFromFixed(fd) + 1, 5) + 1;
    }

    public static int GetBaliWeekFromFixed(int fd)
    {
        return DateUtils.Floor(GetBaliDayFromFixed(fd), 7) + 1;
    }

    protected static readonly int[] fiveDayUrips = new int[] { 5, 9, 7, 4, 8 };
    protected static readonly int[] sevenDayUrips = new int[] { 5, 4, 3, 7, 8, 6, 9 };

    public static int GetBaliDasawaraFromFixed(int fd)
    {
        int i = GetBaliPancawaraFromFixed(fd) - 1;
        int j = GetBaliSaptawaraFromFixed(fd) - 1;
        return DateUtils.Mod(fiveDayUrips[i] + sevenDayUrips[j] + 1, 10);
    }

    public static int GetBaliDwiwaraFromFixed(int fd)
    {
        return DateUtils.Amod(GetBaliDasawaraFromFixed(fd), 2);
    }

    public static bool GetBaliLuangFromFixed(int fd)
    {
        return DateUtils.Mod(GetBaliDasawaraFromFixed(fd), 2) == 0;
    }

    public static int GetBaliSangawaraFromFixed(int fd)
    {
        return DateUtils.Mod(Math.Max(0, GetBaliDayFromFixed(fd) - 3), 9) + 1;
    }

    public static int GetBaliAsatawaraFromFixed(int fd)
    {
        return DateUtils.Mod( Math.Max(6, 4 + DateUtils.Mod(fd-70, 210)), 8) + 1;
    }

    public static int GetBaliCaturawaFromFixed(int fd)
    {
        return DateUtils.Amod(GetBaliAsatawaraFromFixed(fd), 4);
    }

    public static int GetBaliOnOrBefore(BalinesePawukonDate bpd, int fd)
    {
        int a5 = bpd.BaliPancawara - 1;
        int a6 = bpd.BaliSadwara - 1;
        int b7 = bpd.BaliSaptawara - 1;
        int b35 = DateUtils.Mod(a5 + 14 + 15 * (b7 - a5), 35);
        int days = a6 + 36 * (b35 - a6);
        int delta = GetBaliDayFromFixed(0);

        return fd - DateUtils.Mod(fd + delta - days, 210);
    }


    protected static List<int> GetPositionsInInterval(int n, int c, int delta, int start, int end)
    {
        int pos = start + DateUtils.Mod(n - start - delta - 1, c);
        List<int> returnme = new List<int>();

        if (pos > end)
        {
            return returnme;
        }
        else
        {
            returnme.Add(pos);
            List<int> list2 = GetPositionsInInterval(n, c, delta, pos + 1, end);
            returnme.AddRange(list2);
        }

        return returnme;
    }


    public static List<int> GetKajengKeliwonInGregorian(int gregorianYear)
    {
        GregorianDate jan1 = new GregorianDate(gregorianYear, 1, 1);
        GregorianDate dec31 = new GregorianDate(gregorianYear, 12, 31);
        int delta = GetBaliDayFromFixed(0);

        return GetPositionsInInterval(9, 15, delta, jan1.ToFixed(), dec31.ToFixed());
    }


    public static List<int> GetTumpekInGregorian(int gregorianYear)
    {
        GregorianDate jan1 = new GregorianDate(gregorianYear, 1, 1);
        GregorianDate dec31 = new GregorianDate(gregorianYear, 12, 31);
        int delta = GetBaliDayFromFixed(0);

        return GetPositionsInInterval(14, 35, delta, jan1.ToFixed(), dec31.ToFixed());
    }

}


}



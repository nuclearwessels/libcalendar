﻿
using System;
using System.Collections.Generic;
using System.Text;


namespace calendarnet
{
	public class JulianDate : FixedDateBase, IFixedDate
	{
		private int month, day, year;
		public const int Epoch = -1;

		private readonly string[] months = { 
			"January", "February", "March", "April", "May", "June",
			"July", "August", "September", "October", "November", "December" };

		public const double JulianDayEpoch = -1721424.5f;
		public const double ModifiedJulianDayEpoch = 678576.0f;


		public int Year
		{
			set { year = value; }
			get { return year; }
		}

		public int Month
		{
			set
			{
				if (value < 1 || value > 12)
				{
					throw new ArgumentOutOfRangeException("Month",
						"Month must be between 1 and 12.");
				}
				month = value;
			}

			get { return month; }
		}


		public string MonthName
		{
			get { return months[month - 1]; }
		}


		public int Day
		{
			set
			{
				if (value <= 0)
				{
					throw new ArgumentOutOfRangeException("day", "Day must be greater than zero.");
				}

				int maxday = 0;

				switch (month)
				{
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					maxday = 31;
					break;

				case 9:
				case 4:
				case 6:
				case 11:
					maxday = 30;
					break;

				case 2:
					maxday = (IsLeapYear(Year) ? 29 : 28);
					break;
				}

				if (value > maxday)
				{
					throw new ArgumentOutOfRangeException("day",
						String.Format("Day must be less than {0} for month {1}.", maxday, month));
				}

				day = value;
			}

			get { return day; }
		}


        public JulianDate NextMonth
        {
            get
            {
                return new JulianDate(ToFixed() + GetDaysInMonth(year, month));
            }
        }


        public JulianDate PreviousMonth
        {
            get
            {
                int m = month - 1;
                int y = year;
                if (m < 1)
                {
                    m = 12;
                    y--;
                }
                return new JulianDate(ToFixed() - GetDaysInMonth(y, m));
            }
        }


		public double JulianDay
		{
			set
			{
				FromFixed( (int) (value + JulianDayEpoch) );
			}

			get
			{
				return (double)ToFixed() - JulianDayEpoch;
			}
		}


		public double ModifiedJulianDay
		{
			set
			{
				FromFixed((int)(value + ModifiedJulianDayEpoch));
			}

			get
			{
				return (double)ToFixed() - ModifiedJulianDayEpoch;
			}
		}


		public JulianDate()
		{
			FromFixed(Epoch);
		}


		public JulianDate(int fd)
		{
			FromFixed(fd);
		}


        public JulianDate(IFixedDate fd)
            : this(fd.ToFixed())
        {

        }


		public JulianDate(int year, int month, int day)
		{
			Year = year;
			Month = month;
			Day = day;
		} 


		public int ToFixed()
		{
			int y = (year < 0) ? (year + 1) : year;

			int returnme = Epoch - 1;
			returnme += 365 * (y - 1);
            returnme += DateUtils.Quotient(y - 1, 4);
            returnme += DateUtils.Quotient(367 * month - 362, 12);

			if (month <= 2)
			{
				returnme += 0;
			}
			else if (IsLeapYear(year))
			{
				returnme += -1;
			}
			else
			{
				returnme += -2;
			}

			returnme += day;

			return returnme;
		}


		public void FromFixed(int fd)
		{
            int approx = DateUtils.Quotient(4 * (fd - Epoch) + 1464, 1461);
			Year = (approx <= 0) ? (approx - 1) : approx;

			JulianDate jan1 = new JulianDate(year, 1, 1);
			int priorDays = fd - jan1.ToFixed();

			JulianDate mar1 = new JulianDate(year, 3, 1);
			int correction;
			if (fd < mar1.ToFixed())
			{
				correction = 0;
			}
			else if (IsLeapYear(year))
			{
				correction = 1;
			}
			else
			{
				correction = 2;
			}

            Month = DateUtils.Quotient(12 * (priorDays + correction) + 373, 367);
			JulianDate yearMonth1 = new JulianDate(year, month, 1);
			Day = fd - yearMonth1.ToFixed() + 1;
		}


		public override string ToString()
		{
			return String.Format("{0} {1}, {2} {3}", MonthName, Day, Year,
				(Year < 0) ? "B.C.E." : "C.E.");
		}


		public static List<JulianDate> GetJulianInGregorian(int gregorianYear, int julianMonth, int julianDay)
		{
			List<JulianDate> returnme = new List<JulianDate>();

			GregorianDate jan1 = new GregorianDate(gregorianYear, 1, 1);
			GregorianDate dec31 = new GregorianDate(gregorianYear, 12, 31);

			int y = jan1.Year;
			int yprime = (y == -1) ? 1 : (y + 1);

			JulianDate date1 = new JulianDate(y, julianMonth, julianDay);
			JulianDate date2 = new JulianDate(yprime, julianMonth, julianDay);

			if (date1.ToFixed() >= jan1.ToFixed() && date1.ToFixed() <= dec31.ToFixed())
			{
				returnme.Add(date1);
			}

			if (date2.ToFixed() >= jan1.ToFixed() && date2.ToFixed() <= dec31.ToFixed())
			{
				returnme.Add(date2);
			}

			return returnme;
		}


		public static List<JulianDate> GetEasternOrthodoxChristmas(int gy)
		{
			return GetJulianInGregorian(gy, 12, 25);
		}


		GregorianDate GetOrthodoxEaster(int gregorianYear)
		{
			int shiftedEpact = DateUtils.Mod(14 + 11 * DateUtils.Mod(gregorianYear, 19), 30);
			int jyear = (gregorianYear > 0) ? gregorianYear : (gregorianYear - 1);
			JulianDate apr19 = new JulianDate(jyear, 4, 19);
			JulianDate paschalMoon = new JulianDate(apr19.ToFixed() - shiftedEpact);
            return new GregorianDate(DateUtils.WeekdayAfter(paschalMoon.ToFixed(), DateUtils.Weekday.Sun));
		}


		public static bool IsLeapYear(int year)
		{
			return (DateUtils.Mod(year, 4) == ((year > 0) ? 0 : 3));
		}


        public static int GetDaysInMonth(int year, int month)
        {
            switch (month)
            {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 31;

            case 2:
                return IsLeapYear(year) ? 29 : 28;

            case 4:
            case 6:
            case 9:
            case 11:
                return 30;

            default:
                throw new ArgumentException("Invalid month", "month");
            }
        }
	}
}


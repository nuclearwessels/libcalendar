using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

using libcalendarman;


namespace calendarwww.Controllers
{
    public class DayController : Controller
    {
        //
        // GET: /Day/

        public ActionResult Index()
        {
            DateTime now = DateTime.Now;

            GregorianDate gd = new GregorianDate(now.Year, now.Month, now.Day);

            CalendarInfo ci = new CalendarInfo( (int) gd);


            return View("Day", ci );
        }


        public ActionResult Detail(int fd)
        {
            return View("Day", new CalendarInfo(fd));            
        }

    }
}

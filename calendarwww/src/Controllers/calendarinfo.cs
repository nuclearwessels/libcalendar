﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

using libcalendarman;


namespace calendarwww.Controllers
{
    public class CalendarInfo
    {
        public int Date
        {
            get;
            set;
        }

        public List<Event> Events
        {
            get;
            set;
        }

        public SettingsInfo Settings
        {
            set;
            get;
        }

        public CalendarInfo(int date)
        {
            Date = date;
            Events = new List<Event>();
        }


        //public CalendarInfo(FixedDate date)
        //    : this(date.ToFixed())
        //{

        //}


        public CalendarInfo(int date, IEnumerable<Event> events)
        {
            Date = date;
            Events.AddRange(events);
        }


        public CalendarInfo(FixedDate date, IEnumerable<Event> events)
            : this(date.ToFixed(), events)
        {

        }


        public IEnumerable<Event> GetEvents(int date)
        {
            return Events.Where(c => c.FixedDate == date);
        }
    }


}


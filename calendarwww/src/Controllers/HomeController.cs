﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using libcalendarman;


namespace calendarwww.Controllers
{
	[HandleError]
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			//ViewData["Message"] = "Jay's Calendrical Odyssey!";

            DateTime now = DateTime.Now;
            GregorianDate gd = new GregorianDate(now.Year, now.Month, now.Day);

            return RedirectToAction("Detail", "Day", new { fd = gd.ToFixed() });
            //return View( new CalendarInfo(gd) );
		}


		public ActionResult About()
		{
			return View();
		}
	}
}

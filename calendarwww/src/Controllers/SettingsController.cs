﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace calendarwww.Controllers
{

    public class SettingsInfo
    {
        private bool hebrewHolidays, hebrewDates, hebrewNames;
        public static readonly string HebrewHolidaysString = "Hebrew Holidays";
        public static readonly string HebrewDatesString = "Hebrew Dates";
        public static readonly string HebrewNamesString = "Hebrew Names";

        private bool gregorianDates;
        public static readonly string GregorianDatesString = "Gregorian Dates";

        private bool christianHolidays, usHolidays;
        public static readonly string ChristianHolidaysString = "Christian Holidays";
        public static readonly string USHolidaysString = "U.S. Holidays";

        public bool HebrewHolidays { get { return hebrewHolidays; } }
        public bool HebrewDates { get { return hebrewDates; } }
        public bool HebrewNames { get { return hebrewNames; } }

        public bool GregorianDates { get { return gregorianDates; } }
        public bool ChristianHolidays { get { return christianHolidays; } }
        public bool USHolidays { get { return usHolidays; } }


        public SettingsInfo()
        {

        }


        public SettingsInfo(HttpCookieCollection cookies)
        {
            CheckCookie(cookies, HebrewHolidaysString, ref hebrewHolidays);
            CheckCookie(cookies, HebrewDatesString, ref hebrewDates);
            CheckCookie(cookies, HebrewNamesString, ref hebrewNames);
            CheckCookie(cookies, GregorianDatesString, ref gregorianDates);
            CheckCookie(cookies, ChristianHolidaysString, ref christianHolidays);
            CheckCookie(cookies, USHolidaysString, ref usHolidays);
        }


        public SettingsInfo(FormCollection fc)
        {
            CheckForm(fc, HebrewHolidaysString, ref hebrewHolidays);
            CheckForm(fc, HebrewDatesString, ref hebrewDates);
            CheckForm(fc, HebrewNamesString, ref hebrewNames);
            CheckForm(fc, GregorianDatesString, ref gregorianDates);
            CheckForm(fc, ChristianHolidaysString, ref christianHolidays);
            CheckForm(fc, USHolidaysString, ref usHolidays);
        }


        private static void CheckCookie(HttpCookieCollection hcc, string name, ref bool value)
        {
            HttpCookie cookie = hcc.Get(name);
            if (cookie == null)
            {
                value = false;
            }
            else if (cookie.Value == "true")
            {
                value = true;
            }
            else
            {
                value = false;
            }
            
        }


        private static void CheckForm(FormCollection fc, string name, ref bool value)
        {
            string s = fc.Get(name);
            if (s == null)
            {
                value = false;
            }
            else
            {
                value = s.Contains("true");
            }
        }


        public void SetCookies(HttpResponseBase response)
        {
            SetCookie(response, HebrewDatesString, hebrewDates);
            SetCookie(response, HebrewHolidaysString, hebrewHolidays);
            SetCookie(response, HebrewNamesString, hebrewNames);
            SetCookie(response, GregorianDatesString, gregorianDates);
            SetCookie(response, ChristianHolidaysString, christianHolidays);
            SetCookie(response, USHolidaysString, usHolidays);
        }


        private static void SetCookie(HttpResponseBase response, string name, bool value)
        {
            HttpCookie hc = new HttpCookie(name, value ? "true" : "false");
            hc.Expires = DateTime.Now.AddDays(365);
            response.AppendCookie(hc);
        }

    }



    public class SettingsController : Controller
    {

        public ActionResult Index(int fd)
        {
            CalendarInfo ci = new CalendarInfo(fd);
            ci.Settings = new SettingsInfo(Request.Cookies);

            return View("Index", ci);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Index(FormCollection fc, int fd)
        {
            CalendarInfo ci = new CalendarInfo(fd);

            SettingsInfo si = new SettingsInfo(fc);
            si.SetCookies(Response);

            return RedirectToAction("Index", "Day", new { fd = fd });
        }


    }
}


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

using libcalendarman;


namespace calendarwww.Controllers
{

    public class GregorianController : Controller
    {
        //
        // GET: /Gregorian/

        public ActionResult Index()
        {
            DateTime now = DateTime.Now;
            GregorianDate gd = new GregorianDate(now.Year, now.Month, now.Day);
            return RedirectToAction("Month", new { fd = gd.ToFixed() } );
        }


        public ActionResult Month(int fd)
        {
            int gregorianYear = GregorianDate.GetYearFromFixed(fd);

            List<Event> events = new List<Event>();

            SettingsInfo si = new SettingsInfo(Request.Cookies);
            // TODO Add hebrew events
            //if (si.HebrewHolidays)
            //{
            //    events.AddRange(HebrewDate.GetHebrewEvents(gregorianYear));
            //}

            if (si.ChristianHolidays)
            {
                events.AddRange(GregorianDate.GetChristianEvents(gregorianYear));
            }

            if (si.USHolidays)
            {
                events.AddRange(GregorianDate.GetUSEvents(gregorianYear));
            }

            CalendarInfo ci = new CalendarInfo(fd, events);
            ci.Settings = si;
            

            return View("Month", ci);
        }


        public ActionResult Year(int fd)
        {
            CalendarInfo ci = new CalendarInfo(fd);

            return View("Year", ci);
        }

    }
}

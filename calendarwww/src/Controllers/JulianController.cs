using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

using libcalendarman;


namespace calendarwww.Controllers
{

    public class JulianController : Controller
    {
        //
        // GET: /Gregorian/

        public ActionResult Index()
        {
			DateTime now = DateTime.Now;

            List<Event> events = new List<Event>();

            

            return View("Month", new CalendarInfo(new GregorianDate(now.Year, now.Month, now.Day), events));
        }


        public ActionResult Month(int fd)
        {
            List<Event> events = new List<Event>();

            return View("Month", new CalendarInfo(new JulianDate(fd), events));
        }

    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

using libcalendarman;


namespace calendarwww.Controllers
{
    public class EventController : Controller
    {
        static List<EventType> AllEventTypes;


        static EventController()
        {
            AllEventTypes = new List<EventType>();
            AllEventTypes.AddRange(GregorianDate.ChristianEventTypes);
            AllEventTypes.AddRange(GregorianDate.USEventTypes);
            AllEventTypes.AddRange(JulianDate.OrthodoxEventTypes);
        }


        private static List<Event> GetEvents(int gyear)
        {
            List<Event> l = new List<Event>();
            foreach (EventType et in AllEventTypes)
            {
                et.AddEvent(l, gyear);
            }
            return l;
        }


        public static List<Event> GetEvents(SettingsInfo si, int gregorianYear)
        {
            List<Event> events = new List<Event>();

            if (si.HebrewHolidays)
            {
                //events.AddRange(HebrewDate.GetHebrewEvents(gregorianYear));
            }

            if (si.ChristianHolidays)
            {
                events.AddRange(GregorianDate.GetChristianEvents(gregorianYear));
            }

            if (si.USHolidays)
            {
                events.AddRange(GregorianDate.GetUSEvents(gregorianYear));
            }

            return events;
        }


        public ActionResult Index()
        {
            return View();
        }


        //
        // GET: /Event/
        public ActionResult Day(int fd)
        {
            int gyear = GregorianDate.GetYearFromFixed(fd);

            
            CalendarInfo ci = new CalendarInfo(fd, GetEvents(gyear).Where( a => a.FixedDate == fd));
            
            return View(ci);
        }


        public ActionResult List(string evt, int fd)
        {
            int i = 0;

            return View();
        }

    }
}

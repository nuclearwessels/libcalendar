﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<calendarwww.Controllers.CalendarInfo>" %>
<%@ Import Namespace="calendarnet" %>
<%@ Import Namespace="calendarwww.Controllers" %>

<% 
    GregorianDate refDate = new GregorianDate(Model.Date);
    GregorianDate firstOfMonth = new GregorianDate(refDate.Year, refDate.Month, 1);
    GregorianDate firstSunday = new GregorianDate(
        firstOfMonth.WeekdayOnOrBefore(FixedDate.Weekday.Sun));
    GregorianDate lastOfMonth = new GregorianDate(refDate.Year, refDate.Month,
        GregorianDate.GetDaysInMonth(refDate.Year, refDate.Month));
    GregorianDate lastSaturday = new GregorianDate(
        lastOfMonth.WeekdayOnOrAfter(FixedDate.Weekday.Sat));
%>

<div id="month">
    <table>
        <tr>
            <th colspan="7"> <%= Html.Encode( refDate.MonthName ) %> </th>
         </tr>
        <% 
        int fd = firstSunday;
        int weeks = (lastSaturday - firstSunday + 1) / 7;

        for (int week = 0; week < weeks; week++)
        { 
        %>
            <tr>
            <%
            for (int dow = 0; dow < 7; dow++, fd++)
            {
                GregorianDate today = new GregorianDate(fd);

            %>
                <td>
                <%
                if (today.Month == refDate.Month)
                {
                %>
                    <%= Html.ActionLink(String.Format("{0}", today.Day ), "Detail", "Day",
                        new { fd = today.ToFixed() }, null ) %>
                <%  
                }                
                %>
                </td>
            <%
            } 
            %>
                               
            </tr>
        <% } %>
    </table>
</div>
<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<calendarwww.Controllers.CalendarInfo>" %>
<%@ Import Namespace="calendarnet" %>
<%@ Import Namespace="calendarwww.Controllers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%
    GregorianDate gd = new GregorianDate();
    %>
	Gregorian Calendar: <%= Html.Encode(gd.MonthName) %> <%= Html.Encode(gd.Year) %>
</asp:Content>


<asp:Content ContentPlaceHolderID="LeftContent" runat="server">
    <% Html.RenderPartial("LeftContent", Model); %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%
        GregorianDate refDate = new GregorianDate(Model.Date);
        
        GregorianDate firstOfMonth = new GregorianDate(refDate.Year, refDate.Month, 1);
        GregorianDate firstSunday = new GregorianDate(
            firstOfMonth.WeekdayOnOrBefore(FixedDate.Weekday.Sun));
        GregorianDate lastOfMonth = new GregorianDate(refDate.Year, refDate.Month,
            GregorianDate.GetDaysInMonth(refDate.Year, refDate.Month) );
        GregorianDate lastSaturday = new GregorianDate(
            lastOfMonth.WeekdayOnOrAfter(FixedDate.Weekday.Sat));

        HebrewDate hebrewDate;
    %>

    <div id="month">
        <h2>Gregorian Calendar: <%= Html.Encode(refDate.MonthName) %> <%= Html.Encode(refDate.Year) %></h2>

        <table>
           
            <tr>
            <%  
            foreach (string dow in FixedDate.WeekdayNames)
            { 
            %>
                <th><%= Html.Encode(dow) %></th>
            <%
            } 
            %>
            </tr>
            
        
            <% 
            int fd = firstSunday;
            int weeks = (lastSaturday - firstSunday + 1) / 7;

            for (int week = 0; week < weeks; week++)
            { 
            %>
                <tr>
                <%
                for (int dow = 0; dow < 7; dow++, fd++)
                {
                    GregorianDate today = new GregorianDate(fd);
                %>
                    <td>
                    <%
                    if (today.Month == refDate.Month)
                    {
                    %>
                        <%= Html.ActionLink(String.Format("{0}", today.Day ), "Detail", "Day",
                            new { fd = today.ToFixed() }, null ) %>
                    <%  
                        foreach (Event e in Model.GetEvents(fd))
                        {
                        %>
                            <p><%= Html.Encode(e.EventName) %></p>
                            <%--<div><%= Html.ActionLink( e.EventName, "Detail", "Event", new { evt = e.EventName}, null ) %></div>--%>
                        <%    
                        }
                                        
                    }

                    %>
                    </td>
                <%
                } 
                %>
                               
               </tr>
            <% } %>
        </table>
    </div>

    <p/>

    <%--Now print out the months for the entire year.--%>
    <div id="minimonthcontainer">
        <table>
            <% for (int j = 1; j <= 12; j += 4)
               {
             %>
                <tr>
                    <% for (int i = 0; i < 4; i++)
                       { 
                    %>
                    <td>
                        <% Html.RenderPartial("MiniMonth", new CalendarInfo(new GregorianDate(refDate.Year, j + i, 1).ToFixed())); %>
                    </td>
                    <% } %>
                </tr>
            <% } %>
        </table>
    </div>
</asp:Content>

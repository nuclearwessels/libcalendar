﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<calendarwww.Controllers.CalendarInfo>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Index</h2>

    <% Html.BeginForm("Index", "Settings", new { fd = Model.Date }, FormMethod.Post); %>
        <%= Html.CheckBox("Hebrew Holidays", Model.Settings.HebrewHolidays) %> Hebrew Holidays<br />
        <%= Html.CheckBox("Hebrew Dates", Model.Settings.HebrewDates) %> Hebrew Dates<br />
        
        <input type="submit" value="Submit" />
    <% Html.EndForm(); %>
    

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="LeftContent" runat="server">
    <% Html.RenderPartial("LeftContent", Model); %>
</asp:Content>
